
#ifndef LIBXDMA_H
#define LIBXDMA_H

#include <linux/types.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/delay.h>
#include <linux/fb.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/ioport.h>
#include <linux/io.h>
#include <linux/jiffies.h>
#include <linux/kernel.h>
#include <linux/mm.h>
#include <linux/mm_types.h>
#include <linux/poll.h>
#include <linux/pci.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <linux/workqueue.h>
#include <linux/aio.h>
#include <linux/splice.h>
#include <linux/version.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,15,0)
	#include <linux/dma-mapping.h>
#endif
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,18,0)
		#include <linux/dma-direct.h>
#endif
#include <linux/uio.h>
#include <linux/ioctl.h>
#include <linux/errno.h>
#include <linux/aer.h>
#include <linux/string.h>
#include <linux/kdev_t.h>
#include <linux/device.h>
#include <linux/fcntl.h>
#include <linux/msi.h>
#include <linux/mman.h>
#include <linux/kthread.h>
#include <linux/wait.h>

#ifdef DEBUG_V
#define dbg_v(...)	 pr_err("APEC: "); pr_err(__VA_ARGS__)
#define DEBUG
#else
/* disable debugging */
#define dbg_v(...)
#endif

#ifdef DEBUG
#define dbg(...)	pr_err("APEC: "); pr_err(__VA_ARGS__)
#else
/* disable debugging */
#define dbg(...)
#endif

#define DRV_MODULE_NAME		"XDMA"
#define DRV_MODULE_DESC		"CERN XDMA Base Driver"
#define DRV_MODULE_RELDATE	"March. 2019"


// TARGET,OFFSET,MASK, configuration attribute
/*configuration attributes:
RV: reserved: 			0x00
RW: read/write: 		0x01
RC: clear on write :	0x02
W1C: write 1 to clear : 0x04
w1s: write 1 to set : 	0x10
RO : read only	: 		0x20
WO : write only	: 		0x40
CH : channel 	:		0x100 used to check if correct function is used
*/
#define RV (0x01)
#define RW (0x02)
#define RC (0x04)
#define W1C (0x08)
#define W1S (0x10)
#define RO (0x20)
#define WO (0x40)
#define CH (0x80)

//XILINX CONSTANTS
#define XDMA_BAR_NUM (6)
#define XDMA_CHANNEL_NUM_MAX (4)
#define DMA_SUBSYSTEM_FOR_PCIE_IDENTIFIER 0x1FC
/* maximum amount of register space to map */
#define XDMA_BAR_SIZE (0x8000UL)
//ONLY needed during init to find config bar
#define XDMA_OFS_INT_CTRL	(0x2000UL)
//Only needed during init to find config bar
#define XDMA_OFS_CONFIG		(0x3000UL)
#define DESC_MAGIC 0xAD4B0000UL

/* obtain the 32 most significant (high) bits of a 32-bit or 64-bit address */
#define PCI_DMA_H(addr) ((addr >> 16) >> 16)
/* obtain the 32 least significant (low) bits of a 32-bit or 64-bit address */
#define PCI_DMA_L(addr) (addr & 0xffffffffUL)


struct XDMA{
	struct list_head list_head;// keep list of devices
    struct list_head rcu_node;//keep list of devices
    int idx;		/* dev index,guaranted to be uniqe for each device */
	struct pci_dev *pdev;//actual device
	void *__iomem bar[XDMA_BAR_NUM];	/* addresses for mapped BARs */
	int user_bar_idx;	/* BAR index of user logic */
	int config_bar_idx;	/* BAR index of XDMA config logic */
	int bypass_bar_idx;	/* BAR index of XDMA bypass logic */
	int regions_in_use;	/* flag if dev was in use during probe() */
	int got_regions;	/* flag if probe() obtained the regions */
	int bit64_dma_enabled;
	int c2h_number_channels;
	int h2c_number_channels;
	int c2h_channels[XDMA_CHANNEL_NUM_MAX];
	int h2c_channels[XDMA_CHANNEL_NUM_MAX];
	struct msix_entry entry[32];
};
typedef struct XDMA XDMA;

struct XDMA_REGISTER{
	u8 target;
	u8 offset;
	u32 mask;
	u8 attr;

};

/**
 * Descriptor for a single contiguous memory block transfer.
 *
 * Multiple descriptors are linked by means of the next pointer. An additional
 * extra adjacent number gives the amount of extra contiguous descriptors.
 *
 * The descriptors are in root complex memory, and the bytes in the 32-bit
 * words must be in little-endian byte ordering.
 */
struct XDMA_DESCRIPTOR {
	u32 control;
	u32 bytes;		/* transfer length in bytes */
	u32 src_addr_lo;	/* source address (low 32-bit) */
	u32 src_addr_hi;	/* source address (high 32-bit) */
	u32 dst_addr_lo;	/* destination address (low 32-bit) */
	u32 dst_addr_hi;	/* destination address (high 32-bit) */
	/*
	 * next descriptor in the single-linked list of descriptors;
	 * this is the PCIe (bus) address of the next descriptor in the
	 * root complex memory
	 */
	u32 next_lo;		/* next desc address (low 32-bit) */
	u32 next_hi;		/* next desc address (high 32-bit) */
} __packed;

struct XDMA_WRITEBACK {
	u32 control;
	u32 bytes;		/* transfer length in bytes */
} __packed;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,15,0)
struct sched_param {
    int sched_priority;
};
#endif


typedef struct XDMA_REGISTER XDMA_REGISTER;

//H2C channel
extern const XDMA_REGISTER H2C_CHANNEL_IDENTIFIER ;
extern const XDMA_REGISTER H2C_CHANNEL_TARGET;
extern const XDMA_REGISTER H2C_CHANNEL_STREAM;
extern const XDMA_REGISTER H2C_CHANNEL_RESERVED;
extern const XDMA_REGISTER H2C_CHANNEL_CHANNELID;
extern const XDMA_REGISTER H2C_CHANNEL_VERSION;

extern const XDMA_REGISTER H2C_CHANNEL_CONTROL_RESERVED0 ;
extern const XDMA_REGISTER H2C_CHANNEL_CONTROL_WRITEBACK ;
extern const XDMA_REGISTER H2C_CHANNEL_CONTROL_POLLMODE_WB_ENABLE ;
extern const XDMA_REGISTER H2C_CHANNEL_CONTROL_NON_INC_MODE ;
extern const XDMA_REGISTER H2C_CHANNEL_CONTROL_IE_DESC_ERROR ;
extern const XDMA_REGISTER H2C_CHANNEL_CONTROL_IE_WRITE_ERROR ;
extern const XDMA_REGISTER H2C_CHANNEL_CONTROL_IE_READ_ERROR ;
extern const XDMA_REGISTER H2C_CHANNEL_CONTROL_RESERVED1 ;
extern const XDMA_REGISTER H2C_CHANNEL_CONTROL_IE_IDLE_STOP ;
extern const XDMA_REGISTER H2C_CHANNEL_CONTROL_IE_INVALID_LENGTH ;
extern const XDMA_REGISTER H2C_CHANNEL_CONTROL_IE_MAGIC_STOPPED ;
extern const XDMA_REGISTER H2C_CHANNEL_CONTROL_IE_ALIGN_MISMATCH ;
extern const XDMA_REGISTER H2C_CHANNEL_CONTROL_IE_DESCRIPTOR_COMPLETE ;
extern const XDMA_REGISTER H2C_CHANNEL_CONTROL_IE_DESCRIPTOR_STOPPED ;
extern const XDMA_REGISTER H2C_CHANNEL_CONTROL_RUN ;

extern const XDMA_REGISTER H2C_CHANNEL_STATUS_DESCR_ERROR ;
extern const XDMA_REGISTER H2C_CHANNEL_STATUS_WRITE_ERROR ;
extern const XDMA_REGISTER H2C_CHANNEL_STATUS_READ_ERROR ;
extern const XDMA_REGISTER H2C_CHANNEL_STATUS_IDLE_STOPPED ;
extern const XDMA_REGISTER H2C_CHANNEL_STATUS_INVALID_LENGTH ;
extern const XDMA_REGISTER H2C_CHANNEL_STATUS_MAGIC_STOPPED ;
extern const XDMA_REGISTER H2C_CHANNEL_STATUS_ALIGN_MISMATCH ;
extern const XDMA_REGISTER H2C_CHANNEL_STATUS_DESCRIPTOR_COMPLETE ;
extern const XDMA_REGISTER H2C_CHANNEL_STATUS_DESCRIPTOR_STOPPED ;
extern const XDMA_REGISTER H2C_CHANNEL_STATUS_BUSY ;

extern const XDMA_REGISTER H2C_CHANNEL_COMPLETED_DESCRIPTOR_COUNT ;

extern const XDMA_REGISTER H2C_CHANNEL_ADDR_ALIGNMENT ;
extern const XDMA_REGISTER H2C_CHANNEL_LEN_GRANULARITY ;
extern const XDMA_REGISTER H2C_CHANNEL_ADDRESS_BITS ;

extern const XDMA_REGISTER H2C_CHANNEL_POLLMODE_LO_WB_ADDR ;

extern const XDMA_REGISTER H2C_CHANNEL_POLLMODE_HI_WB_ADDR ;

extern const XDMA_REGISTER H2C_CHANNEL_IM_DESC_ERROR ;
extern const XDMA_REGISTER H2C_CHANNEL_IM_WRITE_ERROR ;
extern const XDMA_REGISTER H2C_CHANNEL_IM_READ_ERROR ;
extern const XDMA_REGISTER H2C_CHANNEL_IM_IDLE_STOPPED ;
extern const XDMA_REGISTER H2C_CHANNEL_IM_INVALID_LENGTH ;
extern const XDMA_REGISTER H2C_CHANNEL_IM_MAGIC_STOPPED ;
extern const XDMA_REGISTER H2C_CHANNEL_IM_ALIGN_MISMATCH ;
extern const XDMA_REGISTER H2C_CHANNEL_IM_DESCRIPTOR_COMPLETED ;
extern const XDMA_REGISTER H2C_CHANNEL_IM_DESCRIPTOR_STOPPED ;

extern const XDMA_REGISTER H2C_CHANNEL_PERFORMANCE_MONITOR_CONTROL_RUN ;
extern const XDMA_REGISTER H2C_CHANNEL_PERFORMANCE_MONITOR_CONTROL_CLEAR ;
extern const XDMA_REGISTER H2C_CHANNEL_PERFORMANCE_MONITOR_CONTROL_AUTO ;

extern const XDMA_REGISTER H2C_CHANNEL_PERFORMANCE_CYCLE_COUNT_MAXED ;
extern const XDMA_REGISTER H2C_CHANNEL_PERFORMANCE_CYCLE_COUNT_HI ;
extern const XDMA_REGISTER H2C_CHANNEL_PERFORMANCE_CYCLE_COUNT_LO ;

extern const XDMA_REGISTER H2C_CHANNEL_PERFORMANCE_DATA_COUNT_MAXED ;
extern const XDMA_REGISTER H2C_CHANNEL_PERFORMANCE_DATA_COUNT_HI ;
extern const XDMA_REGISTER H2C_CHANNEL_PERFORMANCE_DATA_COUNT_LO ;

//C2H channel
extern const XDMA_REGISTER C2H_CHANNEL_IDENTIFIER ;
extern const XDMA_REGISTER C2H_CHANNEL_TARGET ;
extern const XDMA_REGISTER C2H_CHANNEL_STREAM ;
extern const XDMA_REGISTER C2H_CHANNEL_RESERVED ;
extern const XDMA_REGISTER C2H_CHANNEL_CHANNELID ;
extern const XDMA_REGISTER C2H_CHANNEL_VERSION ;

extern const XDMA_REGISTER C2H_CHANNEL_CONTROL_RESERVED0 ;
extern const XDMA_REGISTER C2H_CHANNEL_CONTROL_WRITEBACK ;
extern const XDMA_REGISTER C2H_CHANNEL_CONTROL_POLLMODE_WB_ENABLE ;
extern const XDMA_REGISTER C2H_CHANNEL_CONTROL_NON_INC_MODE ;
extern const XDMA_REGISTER C2H_CHANNEL_CONTROL_IE_DESC_ERROR ;
extern const XDMA_REGISTER C2H_CHANNEL_CONTROL_IE_READ_ERROR ;
extern const XDMA_REGISTER C2H_CHANNEL_CONTROL_IE_IDLE_STOP ;
extern const XDMA_REGISTER C2H_CHANNEL_CONTROL_IE_INVALID_LENGTH ;
extern const XDMA_REGISTER C2H_CHANNEL_CONTROL_IE_MAGIC_STOPPED ;
extern const XDMA_REGISTER C2H_CHANNEL_CONTROL_IE_ALIGN_MISMATCH ;
extern const XDMA_REGISTER C2H_CHANNEL_CONTROL_IE_DESCRIPTOR_COMPLETE ;
extern const XDMA_REGISTER C2H_CHANNEL_CONTROL_IE_DESCRIPTOR_STOPPED ;
extern const XDMA_REGISTER C2H_CHANNEL_CONTROL_RUN ;

extern const XDMA_REGISTER C2H_CHANNEL_STATUS_DESCR_ERROR ;
extern const XDMA_REGISTER C2H_CHANNEL_STATUS_READ_ERROR ;
extern const XDMA_REGISTER C2H_CHANNEL_STATUS_IDLE_STOPPED ;
extern const XDMA_REGISTER C2H_CHANNEL_STATUS_INVALID_LENGTH ;
extern const XDMA_REGISTER C2H_CHANNEL_STATUS_MAGIC_STOPPED ;
extern const XDMA_REGISTER C2H_CHANNEL_STATUS_ALIGN_MISMATCH ;
extern const XDMA_REGISTER C2H_CHANNEL_STATUS_DESCRIPTOR_COMPLETE ;
extern const XDMA_REGISTER C2H_CHANNEL_STATUS_DESCRIPTOR_STOPPED ;
extern const XDMA_REGISTER C2H_CHANNEL_STATUS_BUSY ;


extern const XDMA_REGISTER C2H_CHANNEL_COMPLETED_DESCRIPTOR_COUNT ;


extern const XDMA_REGISTER C2H_CHANNEL_ADDR_ALIGNMENT ;
extern const XDMA_REGISTER C2H_CHANNEL_LEN_GRANULARITY ;
extern const XDMA_REGISTER C2H_CHANNEL_ADDRESS_BITS ;


extern const XDMA_REGISTER C2H_CHANNEL_POLLMODE_LO_WB_ADDR ;

extern const XDMA_REGISTER C2H_CHANNEL_POLLMODE_HI_WB_ADDR ;


extern const XDMA_REGISTER C2H_CHANNEL_IM_DESC_ERROR ;
extern const XDMA_REGISTER C2H_CHANNEL_IM_READ_ERROR ;
extern const XDMA_REGISTER C2H_CHANNEL_IM_IDLE_STOPPED ;
extern const XDMA_REGISTER C2H_CHANNEL_IM_INVALID_LENGTH ;
extern const XDMA_REGISTER C2H_CHANNEL_IM_MAGIC_STOPPED ;
extern const XDMA_REGISTER C2H_CHANNEL_IM_DESCRIPTOR_COMPLETED ;
extern const XDMA_REGISTER C2H_CHANNEL_IM_DESCRIPTOR_STOPPED ;

extern const XDMA_REGISTER C2H_CHANNEL_PERFORMANCE_MONITOR_CONTROL_RUN ;
extern const XDMA_REGISTER C2H_CHANNEL_PERFORMANCE_MONITOR_CONTROL_CLEAR ;
extern const XDMA_REGISTER C2H_CHANNEL_PERFORMANCE_MONITOR_CONTROL_AUTO ;

extern const XDMA_REGISTER C2H_CHANNEL_PERFORMANCE_CYCLE_COUNT_LO ;
extern const XDMA_REGISTER C2H_CHANNEL_PERFORMANCE_CYCLE_COUNT_MAXED ;
extern const XDMA_REGISTER C2H_CHANNEL_PERFORMANCE_CYCLE_COUNT_HI ;

extern const XDMA_REGISTER C2H_CHANNEL_PERFORMANCE_DATA_COUNT_LO ;
extern const XDMA_REGISTER C2H_CHANNEL_PERFORMANCE_DATA_COUNT_MAXED ;
extern const XDMA_REGISTER C2H_CHANNEL_PERFORMANCE_DATA_COUNT_HI ;


//IRQ BLOCK REGISTERS
extern const XDMA_REGISTER IRQ_BLOCK_IDENTIFIER;
extern const XDMA_REGISTER IRQ_IDENTIFIER;
extern const XDMA_REGISTER IRQ_RESERVED;
extern const XDMA_REGISTER IRQ_VERSION;

extern const XDMA_REGISTER IRQ_USER_INT_ENMASK;

extern const XDMA_REGISTER IRQ_CHANNEL_INT_ENMASK;

extern const XDMA_REGISTER IRQ_USER_INT_REQ;

extern const XDMA_REGISTER IRQ_ENGINE_INT_REQ;

extern const XDMA_REGISTER IRQ_USER_INT_PEND;

extern const XDMA_REGISTER IRQ_ENGINE_INT_PEND;

extern const XDMA_REGISTER IRQ_VECTOR_3;
extern const XDMA_REGISTER IRQ_VECTOR_2;
extern const XDMA_REGISTER IRQ_VECTOR_1;
extern const XDMA_REGISTER IRQ_VECTOR_0;

extern const XDMA_REGISTER IRQ_VECTOR_7;
extern const XDMA_REGISTER IRQ_VECTOR_6;
extern const XDMA_REGISTER IRQ_VECTOR_5;
extern const XDMA_REGISTER IRQ_VECTOR_4;

extern const XDMA_REGISTER IRQ_VECTOR_11;
extern const XDMA_REGISTER IRQ_VECTOR_10;
extern const XDMA_REGISTER IRQ_VECTOR_9;
extern const XDMA_REGISTER IRQ_VECTOR_8;

extern const XDMA_REGISTER IRQ_VECTOR_15;
extern const XDMA_REGISTER IRQ_VECTOR_14;
extern const XDMA_REGISTER IRQ_VECTOR_13;
extern const XDMA_REGISTER IRQ_VECTOR_12;

extern const XDMA_REGISTER IRQ_VECTOR3;
extern const XDMA_REGISTER IRQ_VECTOR2;
extern const XDMA_REGISTER IRQ_VECTOR1;
extern const XDMA_REGISTER IRQ_VECTOR0;

extern const XDMA_REGISTER IRQ_VECTOR7;
extern const XDMA_REGISTER IRQ_VECTOR6;
extern const XDMA_REGISTER IRQ_VECTOR5;
extern const XDMA_REGISTER IRQ_VECTOR4;

//CONFIG BLOCK REGISTERS
extern const XDMA_REGISTER CONFIG_XDMA_IDENTIFIER;
extern const XDMA_REGISTER CONFIG_IDENTIFIER;
extern const XDMA_REGISTER CONFIG_RESERVED;
extern const XDMA_REGISTER CONFIG_VERSION;
extern const XDMA_REGISTER CONFIG_BUS_DEV;
extern const XDMA_REGISTER CONFIG_PCIE_MAX_PAYLOAD;
extern const XDMA_REGISTER CONFIG_PCIE_MAX_READ;
extern const XDMA_REGISTER CONFIG_SYSTEM_ID;
extern const XDMA_REGISTER CONFIG_MSI_EN;
extern const XDMA_REGISTER CONFIG_MSIX_EN;
extern const XDMA_REGISTER CONFIG_PCIE_WIDTH;
extern const XDMA_REGISTER CONFIG_RELAXED_ORDERING;
extern const XDMA_REGISTER CONFIG_USER_EFF_PAYLOAD;
extern const XDMA_REGISTER CONFIG_USER_PRG_PAYLOAD;
extern const XDMA_REGISTER CONFIG_USER_EFF_READ;
extern const XDMA_REGISTER CONFIG_USER_PRG_READ;
extern const XDMA_REGISTER CONFIG_WRITE_FLUSH_TIMEOUT;

//H2C SGDMA REGISTERS
extern const XDMA_REGISTER H2C_SGDMA_XDMA_IDENTIFIER;
extern const XDMA_REGISTER H2C_SGDMA_TARGET;
extern const XDMA_REGISTER H2C_SGDMA_STREAM;
extern const XDMA_REGISTER H2C_SGDMA_RESERVED;
extern const XDMA_REGISTER H2C_SGDMA_CHANNEL_ID;
extern const XDMA_REGISTER H2C_SGDMA_VERSION;
extern const XDMA_REGISTER H2C_SGDMA_DSC_ADR_LOW;
extern const XDMA_REGISTER H2C_SGDMA_DSC_ADR_HI;
extern const XDMA_REGISTER H2C_SGDMA_DSC_ADJ;
extern const XDMA_REGISTER H2C_SGDMA_DSC_CREDIT;


//C2H SGDMA REGISTERS
extern const XDMA_REGISTER C2H_SGDMA_XDMA_IDENTIFIER;
extern const XDMA_REGISTER C2H_SGDMA_TARGET;
extern const XDMA_REGISTER C2H_SGDMA_STREAM;
extern const XDMA_REGISTER C2H_SGDMA_RESERVED;
extern const XDMA_REGISTER C2H_SGDMA_CHANNEL_ID;
extern const XDMA_REGISTER C2H_SGDMA_VERSION;
extern const XDMA_REGISTER C2H_SGDMA_DSC_ADR_LOW;
extern const XDMA_REGISTER C2H_SGDMA_DSC_ADR_HI;
extern const XDMA_REGISTER C2H_SGDMA_DSC_ADJ;
extern const XDMA_REGISTER C2H_SGDMA_DSC_CREDIT;


//SGDMA COMMON REGISTERS
extern const XDMA_REGISTER C2H_SGDMA_COMMON_XDMA_IDENTIFIER;
extern const XDMA_REGISTER C2H_SGDMA_COMMON_TARGET;
extern const XDMA_REGISTER C2H_SGDMA_COMMON_RESERVED;
extern const XDMA_REGISTER C2H_SGDMA_COMMON_VERSION;
extern const XDMA_REGISTER C2H_SGDMA_COMMON_C2H_DSC_HALT;
extern const XDMA_REGISTER C2H_SGDMA_COMMON_H2C_DSC_HALT;
extern const XDMA_REGISTER C2H_SGDMA_COMMON_H2C_DSC_CREDIT_ENABLE;
extern const XDMA_REGISTER C2H_SGDMA_COMMON_C2H_DSC_CREDIT_ENABLE;

//MSI-X Vector Table and PBA
extern const XDMA_REGISTER MSIX_VECTOR0_ADRRESS_LOW;
extern const XDMA_REGISTER MSIX_VECTOR0_ADRRESS_HI;
extern const XDMA_REGISTER MSIX_VECTOR0_DATA;
extern const XDMA_REGISTER MSIX_VECTOR0_CONTROL_RESERVED;
extern const XDMA_REGISTER MSIX_VECTOR0_CONTROL_MESSAGE_DISABLE;

void remove_XDMA(XDMA* xdev);
XDMA* init_XDMA(struct pci_dev *pdev);

u32 xdmaReadChannel(struct XDMA* dev,const XDMA_REGISTER item,u8 channel );
u32 xdmaWriteChannel(struct XDMA* dev,const XDMA_REGISTER item, u32 value,u8 channel);
u32 xdmaRead(struct XDMA* dev,const XDMA_REGISTER item);
u32 xdmaWrite(struct XDMA* dev,const XDMA_REGISTER item,u32 value) ;
u32 __xdmaRead32(struct XDMA* dev,u8 target, u8 channel, u8 offset);
void __xdmaWrite32(struct XDMA* dev,u8 target, u8 channel, u8 offset,u32 value);

int check_user_bar(XDMA* dev);
u32 read_user_bar(XDMA* dev,u32 address);
void write_user_bar(XDMA*,u32 address,u32 value);

int check_bypass_bar(XDMA* dev);
u32 read_bypass_bar(XDMA* dev,u32 address);
void write_bypass_bar(XDMA*,u32 address,u32 value);

#endif
