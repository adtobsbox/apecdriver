
#include "libXDMA.h"


//H2C CHANNEL BLOCK REGISTERS

//DMA Subsystem for PCIe identifier
const XDMA_REGISTER H2C_CHANNEL_IDENTIFIER={
.target=0x00,
.offset=0x00,
.mask=0xFFF00000,
.attr=RO|CH
};
//H2C Channel Target
 const XDMA_REGISTER H2C_CHANNEL_TARGET={
.target=0x00,
.offset=0x00,
.mask=0x000F0000,
.attr=RO|CH
};
//Stream
//1: AXI4-Stream Interface
//0: Memory Mapped
//AXI4 Interface
 const XDMA_REGISTER H2C_CHANNEL_STREAM={
 .target=0x00,
 .offset=0x00,
 .mask=0x00008000,
 .attr=RO|CH};
//reserved
 const XDMA_REGISTER H2C_CHANNEL_RESERVED={
 .target=0x00,
 .offset=0x00,
 .mask=0x00007000,
 .attr=RO|CH};
//channel ID target
 const XDMA_REGISTER H2C_CHANNEL_CHANNELID={
 .target=0x00,
 .offset=0x00,
 .mask=0x00000F00,
 .attr=RO|CH};
//Version
//8'h01: 2015.3 and 2015.4
//8'h02: 2016.1
//8'h03: 2016.2
//8'h04: 2016.3
//8'h05: 2016.4
//8'h06: 2017.1, 2017.2 and 2017.
const XDMA_REGISTER H2C_CHANNEL_VERSION={
.target=0x00,
.offset=0x00,
.mask=0x000000FF,
.attr=RO|CH};
//reserved
 const XDMA_REGISTER H2C_CHANNEL_CONTROL_RESERVED0={
 .target=0x00,
 .offset=0x04,
 .mask=0xF0000000,
 .attr=RW|CH};
//When set write back information for C2H in AXI-Stream mode is disabled, default write back is enabled.
 const XDMA_REGISTER H2C_CHANNEL_CONTROL_WRITEBACK={
 .target=0x00,
 .offset=0x04,
 .mask=0x08000000,
 .attr=RW|CH};
//pollmode_wb_enable
//Poll mode writeback enable.
//When this bit is set the DMA writes back the completed descriptor count
//when a descriptor with the Completed bit set, is completed.
 const XDMA_REGISTER H2C_CHANNEL_CONTROL_POLLMODE_WB_ENABLE={
 .target=0x00,
 .offset=0x04,
 .mask=0x04000000,
 .attr=RW|CH};
//non_inc_mode
//Non-incrementing address mode.Applies to m_axi_araddr interface only.
 const XDMA_REGISTER H2C_CHANNEL_CONTROL_NON_INC_MODE={
 .target=0x00,
 .offset=0x04,
 .mask=0x02000000,
 .attr=RW|CH};
//ie_desc_error
//Set to all 1s (0x1F) to enable logging of Status.Desc_error and to stop the engine if the error is detected
 const XDMA_REGISTER H2C_CHANNEL_CONTROL_IE_DESC_ERROR={
 .target=0x00,
 .offset=0x04,
 .mask=0x00F80000,
 .attr=RW|CH};
//ie_write_error
//Set to all 1s (0x1F) to enable logging of Status.Write_error and to stop the engine if the error is detected.
 const XDMA_REGISTER H2C_CHANNEL_CONTROL_IE_WRITE_ERROR={
 .target=0x00,
 .offset=0x04,
 .mask=0x0007C000,
 .attr=RW|CH};
//ie_read_error
//Set to all 1s (0x1F) to enable logging of Status.Read_error and to stop the engine if the error is detected.
 const XDMA_REGISTER H2C_CHANNEL_CONTROL_IE_READ_ERROR={
 .target=0x00,
 .offset=0x04,
 .mask=0x00003E00,
 .attr=RW|CH};
//reserved
 const XDMA_REGISTER H2C_CHANNEL_CONTROL_RESERVED1={
 .target=0x00,
 .offset=0x04,
 .mask=0x00000180,
 .attr=RW|CH};
//ie_idle_stopped
//Set to 1 to enable logging of Status.Idle_stopped
 const XDMA_REGISTER H2C_CHANNEL_CONTROL_IE_IDLE_STOP={
 .target=0x00,
 .offset=0x04,
 .mask=0x00000040,
 .attr=RW|CH};
//ie_invalid_length
//Set to 1 to enable logging of Status.Invalid_length
 const XDMA_REGISTER H2C_CHANNEL_CONTROL_IE_INVALID_LENGTH={
 .target=0x00,
 .offset=0x04,
 .mask=0x00000020,
 .attr=RW|CH};
//ie_magic_stopped
//Set to 1 to enable logging of Status.Magic_stopped
 const XDMA_REGISTER H2C_CHANNEL_CONTROL_IE_MAGIC_STOPPED={
 .target=0x00,
 .offset=0x04,
 .mask=0x00000010,
 .attr=RW|CH};
//ie_align_mismatch
//Set to 1 to enable logging of Status.Align_mismatch
 const XDMA_REGISTER H2C_CHANNEL_CONTROL_IE_ALIGN_MISMATCH={
 .target=0x00,
 .offset=0x04,
 .mask=0x00000008,
 .attr=RW|CH};
//ie_descriptor_completed
//Set to 1 to enable logging of Status.Descriptor_completed
 const XDMA_REGISTER H2C_CHANNEL_CONTROL_IE_DESCRIPTOR_COMPLETE={
 .target=0x00,
 .offset=0x04,
 .mask=0x00000004,
 .attr=RW|CH};
//ie_descriptor_stopped
//Set to 1 to enable logging of Status.Descriptor_stopped
 const XDMA_REGISTER H2C_CHANNEL_CONTROL_IE_DESCRIPTOR_STOPPED={
 .target=0x00,
 .offset=0x04,
 .mask=0x00000002,
 .attr=RW|CH};
//Run
//Set to 1 to start the SGDMA engine. Reset to 0 to stop transfer; if the engine is busy it completes the current descriptor
 const XDMA_REGISTER H2C_CHANNEL_CONTROL_RUN={
 .target=0x00,
 .offset=0x04,
 .mask=0x00000001,
 .attr=RW|CH};
//descr_error[4:0]
//Reset (0) on setting the Control register Run bit.
//4: Unexpected completion
//3: Header EP
//2: Parity error
//1: Completer abort
//0: Unsupported request
 const XDMA_REGISTER H2C_CHANNEL_STATUS_DESCR_ERROR={
 .target=0x00,
 .offset=0x40,
 .mask=0x00F80000,
 .attr=RW|CH};
//write_error[4:0]
//Reset (0) on setting the Control register Run bit.
//Bit position:
//4-2: Reserved/
//1: Slave error
//0: Decode error
 const XDMA_REGISTER H2C_CHANNEL_STATUS_WRITE_ERROR={
 .target=0x00,
 .offset=0x40,
 .mask=0x0007C000,
 .attr=RW|CH};
//read_error[4:0]
//Reset (0) on setting the Control register Run bit.
//Bit position
//4: Unexpected completion
//3: Header EP
//2: Parity error
//1: Completer abort
//0: Unsupported request
 const XDMA_REGISTER H2C_CHANNEL_STATUS_READ_ERROR={
 .target=0x00,
 .offset=0x40,
 .mask=0x00003E00,
 .attr=RW|CH};
//idle_stopped
//Reset (0) on setting the Control register Run bit. Set when the engine is idle
//after resetting the Control register Run bit if the Control register ie_idle_stopped bit is set.
 const XDMA_REGISTER H2C_CHANNEL_STATUS_IDLE_STOPPED={
 .target=0x00,
 .offset=0x40,
 .mask=0x00000040,
 .attr=RW|CH};
//invalid_length
//Reset on setting the Control register Run bit. Set when the descriptor length
//is not a multiple of the data width of an AXI4-Stream channel and the Control register ie_invalid_length bit is set
 const XDMA_REGISTER H2C_CHANNEL_STATUS_INVALID_LENGTH={
 .target=0x00,
 .offset=0x40,
 .mask=0x00000020,
 .attr=RW|CH};
//magic_stopped
//Reset on setting the Control register Run bit. Set when the engine encounters a descriptor with invalid
// magic and stopped if the Control register ie_magic_stopped bit is set
 const XDMA_REGISTER H2C_CHANNEL_STATUS_MAGIC_STOPPED={
 .target=0x00,
 .offset=0x40,
 .mask=0x00000010,
 .attr=RW|CH};
//align_mismatch
//Source and destination address on descriptor are not properly aligned to each other.
 const XDMA_REGISTER H2C_CHANNEL_STATUS_ALIGN_MISMATCH={
 .target=0x00,
 .offset=0x40,
 .mask=0x00000008,
 .attr=RW|CH};
//descriptor_completed
//Reset on setting the Control register Run bit. Set after the engine has completed a descriptor with the COMPLE
//TE bit set if the Control register ie_descriptor_stopped bit is set.
 const XDMA_REGISTER H2C_CHANNEL_STATUS_DESCRIPTOR_COMPLETE={
 .target=0x00,
 .offset=0x40,
 .mask=0x00000004,
 .attr=RW|CH};
//descriptor_stopped
//Reset on setting Control register Run bit. Set after the engine completed a
//descriptor with the STOP bit set if the Control register ie_descriptor_stopped bit is set.
 const XDMA_REGISTER H2C_CHANNEL_STATUS_DESCRIPTOR_STOPPED={
 .target=0x00,
 .offset=0x40,
 .mask=0x00000002,
 .attr=RW|CH};
//Busy
//Set if the SGDMA engine is busy. Zero when it is idle
 const XDMA_REGISTER H2C_CHANNEL_STATUS_BUSY={
 .target=0x00,
 .offset=0x40,
 .mask=0x00000001,
 .attr=RO|CH};
//compl_descriptor_count
//The number of competed descriptors update by the engine after completing
//each descriptor in the list.Reset to 0 on rising edge of Control register Run bit (Table 2-40)
 const XDMA_REGISTER H2C_CHANNEL_COMPLETED_DESCRIPTOR_COUNT={
 .target=0x00,
 .offset=0x48,
 .mask=0xFFFFFFFF,
 .attr=RO|CH};
//addr_alignment
//The byte alignment that the source and destination addresses must align to. This value is dependent on configuration parameters.
 const XDMA_REGISTER H2C_CHANNEL_ADDR_ALIGNMENT={
 .target=0x00,
 .offset=0x4C,
 .mask=0x00FF0000,
 .attr=RO|CH};
//len_granularity
//The minimum granularity of DMA transfers in bytes.
 const XDMA_REGISTER H2C_CHANNEL_LEN_GRANULARITY={
 .target=0x00,
 .offset=0x4C,
 .mask=0x0000FF00,
 .attr=RO|CH};
//address_bits
//The number of address bits configured.
 const XDMA_REGISTER H2C_CHANNEL_ADDRESS_BITS={
 .target=0x00,
 .offset=0x4C,
 .mask=0x000000FF,
 .attr=RO|CH};
//Pollmode_lo_wb_addr[31:0]
//Lower 32 bits of the poll mode writeback address.
 const XDMA_REGISTER H2C_CHANNEL_POLLMODE_LO_WB_ADDR={
 .target=0x00,
 .offset=0x88,
 .mask=0xFFFFFFFF,
 .attr=RW|CH};
//Pollmode_hi_wb_addr[63:32]
//Upper 32 bits of the poll mode writeback address
 const XDMA_REGISTER H2C_CHANNEL_POLLMODE_HI_WB_ADDR={
 .target=0x00,
 .offset=0x8C,
 .mask=0xFFFFFFFF,
 .attr=RW|CH};
//im_desc_error[4:0]
//Set to 1 to interrupt when corresponding status register read_error bit is logged
 const XDMA_REGISTER H2C_CHANNEL_IM_DESC_ERROR={
 .target=0x00,
 .offset=0x90,
 .mask=0x00F80000,
 .attr=RW|CH};
//im_write_error[4:0]
//set to 1 to interrupt when corresponding status register write_error bit is logged.
 const XDMA_REGISTER H2C_CHANNEL_IM_WRITE_ERROR={
 .target=0x00,
 .offset=0x90,
 .mask=0x0007C000,
 .attr=RW|CH};
//im_read_error[4:0]
//set to 1 to interrupt when corresponding status register read_error bit is logged
 const XDMA_REGISTER H2C_CHANNEL_IM_READ_ERROR={
 .target=0x00,
 .offset=0x90,
 .mask=0x00003E00,
 .attr=RW|CH};
//im_idle_stopped
//Set to 1 to interrupt when the status register idle_stopped bit is logged
 const XDMA_REGISTER H2C_CHANNEL_IM_IDLE_STOPPED={
 .target=0x00,
 .offset=0x90,
 .mask=0x00000040,
 .attr=RW|CH};
//im_invalid_length
//Set to 1 to interrupt when status register invalid_length bit is logged.
 const XDMA_REGISTER H2C_CHANNEL_IM_INVALID_LENGTH={
 .target=0x00,
 .offset=0x90,
 .mask=0x00000020,
 .attr=RW|CH};
//im_magic_stopped
//set to 1 to interrupt when status register magic_stopped bit is logged
 const XDMA_REGISTER H2C_CHANNEL_IM_MAGIC_STOPPED={
 .target=0x00,
 .offset=0x90,
 .mask=0x00000010,
 .attr=RW|CH};
//im_align_mismatch
//set to 1 to interrupt when status register align_mismatch bit is logged.
 const XDMA_REGISTER H2C_CHANNEL_IM_ALIGN_MISMATCH={
 .target=0x00,
 .offset=0x90,
 .mask=0x00000008,
 .attr=RW|CH};
//im_descriptor_completd
//set to 1 to interrupt when status register descriptor_completed bit is logged
 const XDMA_REGISTER H2C_CHANNEL_IM_DESCRIPTOR_COMPLETED={
 .target=0x00,
 .offset=0x90,
 .mask=0x00000004,
 .attr=RW|CH};
//im_descriptor_stopped
//set to 1 to interrupt when status register descriptor_stopped bit is logged.
 const XDMA_REGISTER H2C_CHANNEL_IM_DESCRIPTOR_STOPPED={
 .target=0x00,
 .offset=0x90,
 .mask=0x00000002,
 .attr=RW|CH};
//Run
//Set to 1 to arm performance counters. Counter starts after the Control
//register Run bit is set.Set to 0 to halt performance counters.
 const XDMA_REGISTER H2C_CHANNEL_PERFORMANCE_MONITOR_CONTROL_RUN={
 .target=0x00,
 .offset=0xC0,
 .mask=0x00000004,
 .attr=RW|CH};
//Clear
//Write 1 to clear performance counters.
 const XDMA_REGISTER H2C_CHANNEL_PERFORMANCE_MONITOR_CONTROL_CLEAR={
 .target=0x00,
 .offset=0xC0,
 .mask=0x00000002,
 .attr=WO|CH};
//Auto
//Automatically stop performance counters when a descriptor with the stop
//bit is completed. Automatically clear performance counters when the Control register Run bit is set. Writin
//g 1 to the Performance Monitor Control register Run bit is still required to start the counters
 const XDMA_REGISTER H2C_CHANNEL_PERFORMANCE_MONITOR_CONTROL_AUTO={
 .target=0x00,
 .offset=0xC0,
 .mask=0x00000001,
 RW|CH};
//pmon_cyc_count_maxed
//Cycle count maximum was hit.
 const XDMA_REGISTER H2C_CHANNEL_PERFORMANCE_CYCLE_COUNT_MAXED={
 .target=0x00,
 .offset=0xC8,
 .mask=0x00010000,
 .attr=RO|CH};
//pmon_cyc_count [41:32]
//Increments once per clock while running. See the Performance Monitor
//Control register (0xC0) bits Clear and Auto for clearing
 const XDMA_REGISTER H2C_CHANNEL_PERFORMANCE_CYCLE_COUNT_HI={
 .target=0x00,
 .offset=0xC8,
 .mask=0x000003FF,
 .attr=RO|CH};
//pmon_cyc_count[31:0]
//Increments once per clock while running. See the Performance Monitor
//Control register (0xC0) bits Clear and Auto for clearing
 const XDMA_REGISTER H2C_CHANNEL_PERFORMANCE_CYCLE_COUNT_LO={
 .target=0x00,
 .offset=0xC4,
 .mask=0xFFFFFFFF,
 .attr=RO|CH};
//pmon_dat_count_maxed
//Data count maximum was hit
 const XDMA_REGISTER H2C_CHANNEL_PERFORMANCE_DATA_COUNT_MAXED={
 .target=0x00,
 .offset=0xD0,
 .mask=0x00010000,
 .attr=RO|CH};
//pmon_dat_count [41:32]
//Increments for each valid read data beat while running. See the Performance Monitor Control register (0xC0) bits Clear and Auto for clearing
const XDMA_REGISTER H2C_CHANNEL_PERFORMANCE_DATA_COUNT_HI={
.target=0x00,
.offset=0xD0,
.mask=0x000003FF,
.attr=RO|CH};
//pmon_dat_count[31:0]
//Increments for each valid read data beat while running. See the Performance
//Monitor Control register (0xC0) bits Clear and Auto for clearing.
 const XDMA_REGISTER H2C_CHANNEL_PERFORMANCE_DATA_COUNT_LO={
 .target=0x00,
 .offset=0xCC,
 .mask=0xFFFFFFFF,
 .attr=RO|CH};



//C2H CHANNEL BLOCK REGISTERS
//DMA Subsystem for PCIe identifier
 const XDMA_REGISTER C2H_CHANNEL_IDENTIFIER={
 .target=0x01,
 .offset=0x00,
 .mask=0xFFF00000,
 .attr=RO|CH};
 //C2H Channel Target
 const XDMA_REGISTER C2H_CHANNEL_TARGET={
 .target=0x01,
 .offset=0x00,
 .mask=0x000F0000,
 .attr=RO|CH};
 //Stream
//1: AXI4-Stream Interface
//0: Memory Mapped AXI4 Interface
 const XDMA_REGISTER C2H_CHANNEL_STREAM={
 .target=0x01,
 .offset=0x00,
 .mask=0x00008000,
 .attr=RO|CH};
//reserved
const XDMA_REGISTER C2H_CHANNEL_RESERVED={
.target=0x01,
.offset=0x00,
.mask=0x00007000,
.attr=RO|CH};
 //Channel ID Target [3:0]
 const XDMA_REGISTER C2H_CHANNEL_CHANNELID={
 .target=0x01,
 .offset=0x00,
 .mask=0x00000F00,
 .attr=RO|CH};
//Version
//8'h01: 2015.3 and 2015.4
//8'h02: 2016.1
//8'h03: 2016.2
//8'h04: 2016.3
//8'h05: 2016.4
//8'h06: 2017.1, 2017.2 and 2017.3
const XDMA_REGISTER C2H_CHANNEL_VERSION={
.target=0x01,
.offset=0x00,
.mask=0x000000FF,
.attr=RW|CH};
//reserved
const XDMA_REGISTER C2H_CHANNEL_CONTROL_RESERVED0={
.target=0x01,
.offset=0x04,
.mask=0xF0000000,
.attr=RW|CH};
//Disables the metadata writeback for C2H AXI4-Stream. No effect if the
//channel is configured to use AXI Memory Mapped
const XDMA_REGISTER C2H_CHANNEL_CONTROL_WRITEBACK={
.target=0x01,
.offset=0x04,
.mask=0x08000000,
.attr=RW|CH};
//pollmode_wb_enable
//Poll mode writeback enable.When this bit is set, the DMA writes back the completed descriptor count
//when a descriptor with the Completed bit set, is completed
const XDMA_REGISTER C2H_CHANNEL_CONTROL_POLLMODE_WB_ENABLE={
.target=0x01,
.offset=0x04,
.mask=0x04000000,
.attr=RW|CH};
//non_inc_mode
//Non-incrementing address mode. Applies to m_axi_araddr interface only
const XDMA_REGISTER C2H_CHANNEL_CONTROL_NON_INC_MODE={
.target=0x01,
.offset=0x04,
.mask=0x02000000,
.attr=RW|CH};
//ie_desc_error
//Set to all 1s (0x1F) to enable logging
 //of Status.Desc_error and to stop the engine if the error is detected
const XDMA_REGISTER C2H_CHANNEL_CONTROL_IE_DESC_ERROR={
.target=0x01,
.offset=0x04,
.mask=0x07F80000,
.attr=RW|CH};
//ie_read_error
//Set to all 1s (0x1F) to enable loggingof Status.Read_error and to stop the
//engine if the error is detected
const XDMA_REGISTER C2H_CHANNEL_CONTROL_IE_READ_ERROR={
.target=0x01,
.offset=0x04,
.mask=0x00003E00,
.attr=RW|CH};
//ie_idle_stopped
//Set to 1 to enable logging of Status.Idle_stopped
const XDMA_REGISTER C2H_CHANNEL_CONTROL_IE_IDLE_STOP={
.target=0x01,
.offset=0x04,
.mask=0x00000040,
.attr=RW|CH};
//ie_invalid_length
//Set to 1 to enable logging of Status.Invalid_length
const XDMA_REGISTER C2H_CHANNEL_CONTROL_IE_INVALID_LENGTH={
.target=0x01,
.offset=0x04,
.mask=0x00000020,
.attr=RW|CH};
//ie_magic_stopped
//Set to 1 to enable logging of Status.Magic_stopped
const XDMA_REGISTER C2H_CHANNEL_CONTROL_IE_MAGIC_STOPPED={
.target=0x01,
.offset=0x04,
.mask=0x00000010,
.attr=RW|CH};
//ie_align_mismatch
//Set to 1 to enable logging of Status.Align_mismatc
const XDMA_REGISTER C2H_CHANNEL_CONTROL_IE_ALIGN_MISMATCH={
.target=0x01,
.offset=0x04,
.mask=0x00000008,
.attr=RW|CH};
//ie_descriptor_completed
//Set to 1 to enable logging of Status.Descriptor_complete
const XDMA_REGISTER C2H_CHANNEL_CONTROL_IE_DESCRIPTOR_COMPLETE={
.target=0x01,
.offset=0x04,
.mask=0x00000004,
.attr=RW|CH};
//ie_descriptor_stopped
//Set to 1 to enable logging of Status.Descriptor_stopped
const XDMA_REGISTER C2H_CHANNEL_CONTROL_IE_DESCRIPTOR_STOPPED={
.target=0x01,
.offset=0x04,
.mask=
0x00000002,
.attr=RW|CH};
//Run
//Set to 1 to start the SGDMA engine. Reset to 0 to stop the transfer, if the
//engine is busy it completes the current descriptor.
const XDMA_REGISTER C2H_CHANNEL_CONTROL_RUN={
.target=0x01,
.offset=0x04,
.mask=0x00000001,
.attr=RW|CH};
//descr_error[4:0]
//Reset (0) on setting the Control register Run bit.
//Bit position:
//4:Unexpected completion
//3: Header EP
//2: Parity error
//1: Completer abort
//0: Unsupported request
const XDMA_REGISTER C2H_CHANNEL_STATUS_DESCR_ERROR={
.target=0x01,
.offset=0x40,
.mask=0x00F80000,
.attr=RW|CH};
//read_error[4:0]
//Reset (0) on setting the Control register Run bit.
//Bit position:
//4-2: Reserved
//1: Slave error
//0: Decode error
const XDMA_REGISTER C2H_CHANNEL_STATUS_READ_ERROR={
.target=0x01,
.offset=0x40,
.mask=0x00003E00,
.attr=RW|CH};
//idle_stopped
//Reset (0) on setting the Control register Run bit. Set
//when the engine is idle after resetting the Contro
//l register Run bit if the Control register ie_idle_stopped bit is set
const XDMA_REGISTER C2H_CHANNEL_STATUS_IDLE_STOPPED={
.target=0x01,
.offset=0x40,
.mask=0x00000040,
.attr=RW|CH};
//invalid_length
//Reset on setting the Control register Run bit. Set when the descriptor length
//is not a multiple of the data width of an AXI4-Stream channel and the Control
//register ie_invalid_length bit is set
const XDMA_REGISTER C2H_CHANNEL_STATUS_INVALID_LENGTH={
.target=0x01,
.offset=0x40,
.mask=0x00000020,
.attr=RW|CH};
//magic_stopped
//Reset on setting the Control register Run bit. Set when the engine
//encounters a descriptor with invalid magic and stopped if the Control
//register ie_magic_stopped bit is set.
const XDMA_REGISTER C2H_CHANNEL_STATUS_MAGIC_STOPPED={
.target=0x01,
.offset=0x40,
.mask=0x00000010,
.attr=RW|CH};
//align_mismatch
//Source and destination address on descriptor are not properly aligned to each other.
const XDMA_REGISTER C2H_CHANNEL_STATUS_ALIGN_MISMATCH={
.target=0x01,
.offset=0x40,
.mask=0x00000008,
.attr=RW|CH};
//descriptor_completed
//Reset on setting the Control register Run bit. Set after the engine has
//completed a descriptor with the COMPLETE bit set if the Control register
//ie_descriptor_completed bit is set
const XDMA_REGISTER C2H_CHANNEL_STATUS_DESCRIPTOR_COMPLETE={
.target=0x01,
.offset=0x40,
.mask=0x00000004,
.attr=RW|CH};
//descriptor_stopped
//Reset on setting the Control register Run bit. Set after the engine completed
//a descriptor with the STOP bit set if the Control register ie_magic_stopped bit is set.
const XDMA_REGISTER C2H_CHANNEL_STATUS_DESCRIPTOR_STOPPED={
.target=0x01,
.offset=0x40,
.mask=0x00000002,
.attr=RW|CH};
//Busy
//Set if the SGDMA engine is busy. Zero when it is idle.
const XDMA_REGISTER C2H_CHANNEL_STATUS_BUSY={
.target=0x01,
.offset=0x40,
.mask=0x00000001,
.attr=RO|CH};
//compl_descriptor_count
//The number of competed descriptors update by the engine after completing
//each descriptor in the list.Reset to 0 on rising edge of Control register, run bit (Table 2-59
const XDMA_REGISTER C2H_CHANNEL_COMPLETED_DESCRIPTOR_COUNT={
.target=0x01,
.offset=0x48,
.mask=0xFFFFFFFF,
.attr=RO|CH};
//addr_alignment
//The byte alignment that the source and destination addresses must align
//to. This value is dependent on configuration parameters.
const XDMA_REGISTER C2H_CHANNEL_ADDR_ALIGNMENT={
.target=0x01,
.offset=0x4C,
.mask=0x00FF0000,
.attr=RO|CH};
//len_granularity
//The minimum granularity of DMA transfers in bytes
const XDMA_REGISTER C2H_CHANNEL_LEN_GRANULARITY={
.target=0x01,
.offset=0x4C,
.mask=0x0000FF00,
.attr=RO|CH};
//address_bits
//The number of address bits configured
const XDMA_REGISTER C2H_CHANNEL_ADDRESS_BITS={
.target=0x01,
.offset=0x4C,
.mask=0x000000FF,
.attr=RO|CH};
//Pollmode_lo_wb_addr[31:0]
//Lower 32 bits of the poll mode writeback addres
const XDMA_REGISTER C2H_CHANNEL_POLLMODE_LO_WB_ADDR={
.target=0x01,
.offset=0x88,
.mask=0xFFFFFFFF,
.attr=RW|CH};
//Pollmode_hi_wb_addr[63:32]
//Upper 32 bits of the poll mode writeback address
const XDMA_REGISTER C2H_CHANNEL_POLLMODE_HI_WB_ADDR={
.target=0x01,
.offset=0x8C,
.mask=0xFFFFFFFF,
.attr=RW|CH};
//im_desc_error[4:0]
//set to 1 to interrupt when corresponding Status.Read_Error is logged
const XDMA_REGISTER C2H_CHANNEL_IM_DESC_ERROR={
.target=0x01,
.offset=0x90,
.mask=0x00F80000,
.attr=RW|CH};
//im_read_error[4:0]
//set to 1 to interrupt when corresponding Status.Read_Error is logged
const XDMA_REGISTER C2H_CHANNEL_IM_READ_ERROR={
.target=0x01,
.offset=0x90,
.mask=0x00003E00,
.attr=RW|CH};
//im_idle_stopped
//set to 1 to interrupt when the Status.Idle_stopped is logged
const XDMA_REGISTER C2H_CHANNEL_IM_IDLE_STOPPED={
.target=0x01,
.offset=0x90,
.mask=0x00000040,
.attr=RW|CH};
//im_magic_stopped
//set to 1 to interrupt when Status.Magic_stopped is logged.
const XDMA_REGISTER C2H_CHANNEL_IM_MAGIC_STOPPED={
.target=0x01,
.offset=0x90,
.mask=0x00000010,
.attr=RW|CH};
//im_descriptor_completd
//set to 1 to interrupt when Status.Descriptor_completed is logged
const XDMA_REGISTER C2H_CHANNEL_IM_DESCRIPTOR_COMPLETED={
.target=0x01,
.offset=0x90,
.mask=0x00000004,
.attr=RW|CH};
//im_descriptor_stopped
//set to 1 to interrupt when Status.Descriptor_stopped is logged.
const XDMA_REGISTER C2H_CHANNEL_IM_DESCRIPTOR_STOPPED={
.target=0x01,
.offset=0x90,
.mask=0x00000002,
.attr=RW|CH};
//Run
//Set to 1 to arm performance counters. Counter starts after the Control
//register Run bit is set.Set to 0 to halt performance counters.
const XDMA_REGISTER C2H_CHANNEL_PERFORMANCE_MONITOR_CONTROL_RUN={
.target=0x01,
.offset=0xC0,
.mask=0x00000004,
.attr=RW|CH};
//Clear
//Write 1 to clear performance counters.
const XDMA_REGISTER C2H_CHANNEL_PERFORMANCE_MONITOR_CONTROL_CLEAR={
.target=0x01,
.offset=0xC0,
.mask=0x00000002,
.attr=WO|CH};
//Auto
//Automatically stop performance counters when a descriptor with the stop
//bit is completed. Automatically clear performance counters when the
//Control register Run bit is set. Writing 1 to the Performance Monitor Control
//register Run bit is still required to start the counters
const XDMA_REGISTER C2H_CHANNEL_PERFORMANCE_MONITOR_CONTROL_AUTO={
.target=0x01,
.offset=0xC0,
.mask=0x00000001,
.attr=RW|CH};
//pmon_cyc_count[31:0]
//Increments once per clock while running. See the Performance Monitor
//Control register (0xC0) bits Clear and Auto for clearing.
const XDMA_REGISTER C2H_CHANNEL_PERFORMANCE_CYCLE_COUNT_LO={
.target=0x01,
.offset=0xC4,
.mask=0xFFFFFFFF,
.attr=RO|CH};
//pmon_cyc_count_maxed
//Cycle count maximum was hit.
const XDMA_REGISTER C2H_CHANNEL_PERFORMANCE_CYCLE_COUNT_MAXED={
.target=0x01,
.offset=0xC8,
.mask=0x00010000,
.attr=RO|CH};
//pmon_cyc_count [41:32]
//Increments once per clock while running. See the Performance Monitor
//Control register (0xC0) bits Clear and Auto for clearing.
const XDMA_REGISTER C2H_CHANNEL_PERFORMANCE_CYCLE_COUNT_HI={
.target=0x01,
.offset=0xC8,
.mask=0x000003FF,
.attr=RO|CH};
//pmon_dat_count[31:0]
//Increments for each valid read data beat while running. See the Performance
//Monitor Control register (0xC0) bits Clear and Auto for clearing
const XDMA_REGISTER C2H_CHANNEL_PERFORMANCE_DATA_COUNT_LO={
.target=0x01,
.offset=0xCC,
.mask=0xFFFFFFFF,
.attr=RO|CH};
//pmon_dat_count_maxed
//Data count maximum was hit
const XDMA_REGISTER C2H_CHANNEL_PERFORMANCE_DATA_COUNT_MAXED={
.target=0x01,
.offset=0xD0,
.mask=0x00010000,
.attr=RO|CH};
//pmon_dat_count [41:32]
//Increments for each valid read data beat while running. See the Performance
//Monitor Control register (0xC0) bits Clear and Auto for clearing.
const XDMA_REGISTER C2H_CHANNEL_PERFORMANCE_DATA_COUNT_HI={
.target=0x01,
.offset=0xD0,
.mask=0x000003FF,
.attr=RO|CH};


//IRQ BLOCK REGISTERS

//DMA Subsystem for PCIe identifier
const XDMA_REGISTER IRQ_BLOCK_IDENTIFIER={
.target=0x02,
.offset=0x00,
.mask=0xFFF00000,
.attr=RO};
//IRQ Identifier
const XDMA_REGISTER IRQ_IDENTIFIER={
.target=0x02,
.offset=0x00,
.mask=0x000F0000,
.attr=RO};
//Reserved
const XDMA_REGISTER IRQ_RESERVED={
.target=0x02,
.offset=0x00,
.mask=0x0000FF00,
.attr=RO};
//Version
//8'h01: 2015.3 and 2015.4
//8'h02: 2016.1
//8'h03: 2016.2
//8'h04: 2016.3
//8'h05: 2016.4
//8'h06: 2017.1, 2017.2 and 2017.3
const XDMA_REGISTER IRQ_VERSION={
.target=0x02,
.offset=0x00,
.mask=0x000000FF,
.attr=RO};
//user_int_enmask
//User Interrupt Enable Mask
//0: Prevents an interrupt from being generated when the user interrupt source is asserted.
//1: Generates an interrupt on the rising edge of the user interrupt
//source. If the Enable Mask is set and the source is already set, a user interrupt will be generated also
const XDMA_REGISTER IRQ_USER_INT_ENMASK={
.target=0x02,
.offset=0x04,
.mask=0xFFFFFFFF,
.attr=RW};
//channel_int_enmask
//Engine Interrupt Enable Mask. One bit per read or write engine.
//0: Prevents an interrupt from being generated when interrupt
//source is asserted. The position of the H2C bits always starts at bit
//0. The position of the C2H bits is the index above the last H2C index, and therefore depends on
// the NUM_H2C_CHNL parameter.
//1: Generates an interrupt on the rising edge of the interrupt source.
//If the enmask bit is set and the source is already set, an interrupt is also be generated.
const XDMA_REGISTER IRQ_CHANNEL_INT_ENMASK={
.target=0x02,
.offset=0x10,
.mask=0xFFFFFFFF,
.attr=RW};
//user_int_req
//User Interrupt Request
//This register reflects the interrupt source AND’d with the enable mask register
const XDMA_REGISTER IRQ_USER_INT_REQ={
.target=0x02,
.offset=0x40,
.mask=0xFFFFFFFF,
.attr=RO};
//engine_int_req
//Engine Interrupt Request. One bit per read or write engine. This
//register reflects the interrupt source AND with the enable mask
//register. The position of the H2C bits always starts at bit 0. The position of the C2H bits is the in
//dex above the last H2C index, and therefore depends on the NUM_H2C_CHNL parameter.
//Figure 2-8 shows the packing of H2C and C2H bits
const XDMA_REGISTER IRQ_ENGINE_INT_REQ={
.target=0x02,
.offset=0x44,
.mask=0xFFFFFFFF,
.attr=RO};
//user_int_pend
//User Interrupt Pending.
//This register indicates pending events. The pending events are
//cleared by removing the event cause condition at the source component.
const XDMA_REGISTER IRQ_USER_INT_PEND={
.target=0x02,
.offset=0x48,
.mask=0xFFFFFFFF,
.attr=RO};
//engine_int_pend
//Engine Interrupt Pending.One bit per read or write engine.
//This register indicates pending events. The pending events are cleared by removing the event
//cause condition at the source component. The position of the H2C
//bits always starts at bit 0. The position of the C2H bits is the index
//above the last H2C index, and therefore depends on the NUM_H2C_CHNL parameter.
//Figure 2-8 shows the packing of H2C and C2H bits.
const XDMA_REGISTER IRQ_ENGINE_INT_PEND={
.target=0x02,
.offset=0x4C,
.mask=0xFFFFFFFF,
.attr=RO};

//If MSI is enabled, this register specifies the MSI or MSI-X vector number of the
//MSI. In Legacy interrupts only the two LSBs of each field should be used to map to INTA, B, C, or D

//vector 3
//The vector number that is used when an interrupt is gene
//rated by the user IRQ usr_irq_req[3].
const XDMA_REGISTER IRQ_VECTOR_3={
.target=0x02,
.offset=0x80,
.mask=0x1F000000,
.attr=RW};
//vector 2
//The vector number that is used when an interrupt is gene
//rated by the user IRQ usr_irq_req[2].
const XDMA_REGISTER IRQ_VECTOR_2={
.target=0x02,
.offset=0x80,
.mask=0x001F0000,
.attr=RW};
//vector 1
//The vector number that is used when an interrupt is gene
//rated by the user IRQ usr_irq_req[1].
const XDMA_REGISTER IRQ_VECTOR_1={
.target=0x02,
.offset=0x80,
.mask=0x00001F00,
.attr=RW};
//vector 0
//The vector number that is used when an interrupt is gene
//rated by the user IRQ usr_irq_req[0].
const XDMA_REGISTER IRQ_VECTOR_0={
.target=0x02,
.offset=0x80,
.mask=0x0000001F,
.attr=RW};
//vector 7
//The vector number that is used when an interrupt is gene
//rated by the user IRQ usr_irq_req[7].
const XDMA_REGISTER IRQ_VECTOR_7={
.target=0x02,
.offset=0x84,
.mask=0x1F000000,
.attr=RW};
//vector 6
//The vector number that is used when an interrupt is gene
//rated by the user IRQ usr_irq_req[6].
const XDMA_REGISTER IRQ_VECTOR_6={
.target=0x02,
.offset=0x84,
.mask=0x001F0000,
.attr=RW};
//vector 5
//The vector number that is used when an interrupt is gene
//rated by the user IRQ usr_irq_req[5].
const XDMA_REGISTER IRQ_VECTOR_5={
.target=0x02,
.offset=0x84,
.mask=0x00001F00,
.attr=RW};
//vector 4
//The vector number that is used when an interrupt is gene
//rated by the user IRQ usr_irq_req[4].
const XDMA_REGISTER IRQ_VECTOR_4={
.target=0x02,
.offset=0x84,
.mask=0x0000001F,
.attr=RW};
//vector 11
//The vector number that is used when an interrupt is gene
//rated by the user IRQ usr_irq_req[11].
const XDMA_REGISTER IRQ_VECTOR_11={
.target=0x02,
.offset=0x88,
.mask=0x1F000000,
.attr=RW};
//vector 10
//The vector number that is used when an interrupt is gene
//rated by the user IRQ usr_irq_req[10].
const XDMA_REGISTER IRQ_VECTOR_10={
.target=0x02,
.offset=0x88,
.mask=0x001F0000,
.attr=RW};
//vector 9
//The vector number that is used when an interrupt is gene
//rated by the user IRQ usr_irq_req[9].
const XDMA_REGISTER IRQ_VECTOR_9={
.target=0x02,
.offset=0x88,
.mask=0x00001F00,
.attr=RW};
//vector 8
//The vector number that is used when an interrupt is gene
//rated by the user IRQ usr_irq_req[8]
const XDMA_REGISTER IRQ_VECTOR_8={
.target=0x02,
.offset=0x88,
.mask=0x0000001F,
.attr=RW};

//vector 15
//The vector number that is used when an interrupt is gene
//rated by the user IRQ usr_irq_req[15].
const XDMA_REGISTER IRQ_VECTOR_15={
.target=0x02,
.offset=0x8C,
.mask=0x1F000000,
.attr=RW};
//vector 14
//The vector number that is used when an interrupt is gene
//rated by the user IRQ usr_irq_req[14].
const XDMA_REGISTER IRQ_VECTOR_14={
.target=0x02,
.offset=0x8C,
.mask=0x001F0000,
.attr=RW};
//vector 13
//The vector number that is used when an interrupt is gene
//rated by the user IRQ usr_irq_req[13].
const XDMA_REGISTER IRQ_VECTOR_13={
.target=0x02,
.offset=0x8C,
.mask=0x00001F00,
.attr=RW};
//vector 12
//The vector number that is used when an interrupt is gene
//rated by the user IRQ usr_irq_req[12]
const XDMA_REGISTER IRQ_VECTOR_12={
.target=0x02,
.offset=0x8C,
.mask=0x0000001F,
.attr=RW};

//If MSI is enabled, this register specifies the MSI vector number of the MSI. In Legacy interrupts, only the
//2 LSB of each field should be used to map to INTA, B, C, or D. Similar to the other C2H/H2C bit
// packing clarification, see Figure 2-8. The first C2H vector is after the
//last H2C vector. For example, if NUM_H2C_Channel = 1, then H2C0 vector is at 0xA0, bits [4:0], and C2H
//Channel 0 vector is at 0xA0, bits [12:8].If NUM_H2C_Channel = 4, then H2C3 vector is at 0xA0
// 28:24, and C2H Channel 0 vector is at 0xA4, bits [4:0].

//vector3
//The vector number that is used when an interrupt is generated by channel 3
const XDMA_REGISTER IRQ_VECTOR3={
.target=0x02,
.offset=0xA0,
.mask=0x1F000000,
.attr=RW};
//vector2
//The vector number that is used when an interrupt is generated by channel 2
const XDMA_REGISTER IRQ_VECTOR2={
.target=0x02,
.offset=0xA0,
.mask=0x001F0000,
.attr=RW};
//vector1
//The vector number that is used when an interrupt is generated by channel 1
const XDMA_REGISTER IRQ_VECTOR1={
.target=0x02,
.offset=0xA0,
.mask=0x00001F00,
.attr=RW};
//vector0
//The vector number that is used when an interrupt is generated by channel 0
const XDMA_REGISTER IRQ_VECTOR0={
.target=0x02,
.offset=0xA0,
.mask=0x0000001F,
.attr=RW};
//vector7
//The vector number that is used when an interrupt is generated by channel 7
const XDMA_REGISTER IRQ_VECTOR7={
.target=0x02,
.offset=0xA4,
.mask=0x1F000000,
.attr=RW};
//vector6
//The vector number that is used when an interrupt is generated by channel 6
const XDMA_REGISTER IRQ_VECTOR6={
.target=0x02,
.offset=0xA4,
.mask=0x001F0000,
.attr=RW};
//vector5
//The vector number that is used when an interrupt is generated by channel 5
const XDMA_REGISTER IRQ_VECTOR5={
.target=0x02,
.offset=0xA4,
.mask=0x00001F00,
.attr=RW};
//vector4
//The vector number that is used when an interrupt is generated by channel 4
const XDMA_REGISTER IRQ_VECTOR4={
.target=0x02,
.offset=0xA4,
.mask=0x0000001F,
.attr=RW};


//CONFIG BLOCK REGISTERS

//DMA Subsystem for PCIe identifier
const XDMA_REGISTER CONFIG_XDMA_IDENTIFIER={
.target=0x03,
.offset=0x00,
.mask=0xFFF00000,
.attr=RO};
//Config Identifier
const XDMA_REGISTER CONFIG_IDENTIFIER={
.target=0x03,
.offset=0x00,
.mask=0x000F0000,
.attr=RO};
//Reserved
const XDMA_REGISTER CONFIG_RESERVED={
.target=0x03,
.offset=0x00,
.mask=0x0000FF00,
.attr=RO};
//Version
//8'h01: 2015.3 and 2015.4
//8'h02: 2016.1
//8'h03: 2016.2
//8'h04: 2016.3
//8'h05: 2016.4
//8'h06: 2017.1, 2017.2 and 2017.
const XDMA_REGISTER CONFIG_VERSION={
.target=0x03,
.offset=0x00,
.mask=0x000000FF,
.attr=RO};
//bus_dev
//Bus, device, and function
const XDMA_REGISTER CONFIG_BUS_DEV={
.target=0x03,
.offset=0x04,
.mask=0x0000FFFF,
.attr=RO};
//pcie_max_payload
//Maximum write payload size. This is the lesser of the PCIe IP MPS and DMA
//Subsystem for PCIe parameters.
//3'b000: 128 bytes
//3'b001: 256 bytes
//3'b010: 512 bytes
//3'b011: 1024 bytes
//3'b100: 2048 bytes
//3'b101: 4096 bytes
const XDMA_REGISTER CONFIG_PCIE_MAX_PAYLOAD={
.target=0x03,
.offset=0x08,
.mask=0x00000007,
.attr=RO};
//pcie_max_read
//Maximum read request size. This is the lesser of the PCIe IP MRRS and DMA
//Subsystem for PCIe parameters.
//3'b000: 128 bytes
//3'b001: 256 bytes
//3'b010: 512 bytes
//3'b011: 1024 bytes
//3'b100: 2048 bytes
//3'b101: 4096 bytes
const XDMA_REGISTER CONFIG_PCIE_MAX_READ={
.target=0x03,
.offset=0x0C,
.mask=0x00000007,
.attr=RO};
//system_id
//DMA Subsystem for PCIe system ID
const XDMA_REGISTER CONFIG_SYSTEM_ID={
.target=0x03,
.offset=0x10,
.mask=0x0000FFFF,
.attr=RO};
//MSI_en
//MSI Enable
const XDMA_REGISTER CONFIG_MSI_EN={
.target=0x03,
.offset=0x14,
.mask=0x00000001,
.attr=RO};
//MSI-X Enable
const XDMA_REGISTER CONFIG_MSIX_EN={
.target=0x03,
.offset=0x14,
.mask=0x00000002,
.attr=RO};
//pcie_width
//PCIe AXI4-Stream Width
//0: 64 bits
//1: 128 bits
//2: 256 bits
//3: 512 bits
const XDMA_REGISTER CONFIG_PCIE_WIDTH={
.target=0x03,
.offset=0x18,
.mask=0x00000007,
.attr=RO};
//Relaxed Ordering
//PCIe read request TLPs are generated with the relaxed ordering bit se
const XDMA_REGISTER CONFIG_RELAXED_ORDERING={
.target=0x03,
.offset=0x1C,
.mask=0x00000001,
.attr=RW};
//user_eff_payload
//The actual maximum payload size issued to the user application. This value
//might be lower than user_prg_payload due to IP configuration or datapath width.
//3'b000: 128 bytes
//3'b001: 256 bytes
//3'b010: 512 bytes
//3'b011: 1024 bytes
//3'b100: 2048 bytes
//3'b101: 4096 bytes
const XDMA_REGISTER CONFIG_USER_EFF_PAYLOAD={
.target=0x03,
.offset=0x40,
.mask=0x00000070,
.attr=RO};
//user_prg_payload
//The programmed maximum payload size issued to the user application.
//3'b000: 128 bytes
//3'b001: 256 bytes
//3'b010: 512 bytes
//3'b011: 1024 bytes
//3'b100: 2048 bytes
//3'b101: 4096 bytes
const XDMA_REGISTER CONFIG_USER_PRG_PAYLOAD={
.target=0x03,
.offset=0x40,
.mask=0x00000007,
.attr=RW};
//user_eff_read
//Maximum read request size issued to the user application. This value may be
//lower than user_max_read due to PCIe configuration or datapath width.
//3'b000: 128 bytes
//3'b001: 256 bytes
//3'b010: 512 bytes
//3'b011: 1024 bytes
//3'b100: 2048 bytes
//3'b101: 4096 bytes
const XDMA_REGISTER CONFIG_USER_EFF_READ={
.target=0x03,
.offset=0x44,
.mask=0x00000070,
.attr=RO};
//user_prg_read
//Maximum read request size issued to the user application.
//3'b000: 128 bytes
//3'b001: 256 bytes
//3'b010: 512 bytes
//3'b011: 1024 bytes
//3'b100: 2048 bytes
//3'b101: 4096 bytes
const XDMA_REGISTER CONFIG_USER_PRG_READ={
.target=0x03,
.offset=0x44,
.mask=0x00000007,
.attr=RW};
//Write Flush Timeout
//Applies to AXI4-Stream C2H channels. This register specifies the number of
//clock cycles a channel waits for data before flushing
//the write data it already received from PCIe. This action cl
//oses the descriptor and generates a writeback. A value of 0 disables the timeout. The timeout value in clocks =
//2^value.
const XDMA_REGISTER CONFIG_WRITE_FLUSH_TIMEOUT={
.target=0x03,
.offset=0x60,
.mask=0x0000001F,
.attr=RW};



//H2C SGDMA REGISTERS

//DMA Subsystem for PCIe identifier
const XDMA_REGISTER H2C_SGDMA_XDMA_IDENTIFIER={
.target=0x04,
.offset=0x00,
.mask=0xFFF00000,
.attr=RO|CH};
//H2C DMA Target
const XDMA_REGISTER H2C_SGDMA_TARGET={
.target=0x04,
.offset=0x00,
.mask=0x000F0000,
.attr=RO|CH};
//Stream
//1: AXI4-Stream Interface
//0: Memory Mapped AXI4 Interface
const XDMA_REGISTER H2C_SGDMA_STREAM={
.target=0x04,
.offset=0x00,
.mask=0x00008000,
.attr=RO|CH};
//Reserved
const XDMA_REGISTER H2C_SGDMA_RESERVED={
.target=0x04,
.offset=0x00,
.mask=0x00007000,
.attr=RO|CH};
//Channel ID Target [3:0]
const XDMA_REGISTER H2C_SGDMA_CHANNEL_ID={
.target=0x04,
.offset=0x00,
.mask=0x00000F00,
.attr=RO|CH};
//Version
//8'h01: 2015.3 and 2015.4
//8'h02: 2016.1
//8'h03: 2016.2
//8'h04: 2016.3
//8'h05: 2016.4
//8'h06: 2017.1, 2017.2 and 2017.3
const XDMA_REGISTER H2C_SGDMA_VERSION={
.target=0x04,
.offset=0x00,
.mask=0x000000FF,
.attr=RO|CH};
//dsc_adr[31:0]
//Lower bits of start descriptor address. Dsc_adr[63:0] is the first descriptor
//address that is fetched after the Control register Run bit is set
const XDMA_REGISTER H2C_SGDMA_DSC_ADR_LOW={
.target=0x04,
.offset=0x80,
.mask=0xFFFFFFFF,
.attr=RW|CH};
//dsc_adr[63:32]
//Upper bits of start descriptor address.
//Dsc_adr[63:0] is the first descriptor address that is fetched after the Control
//register Run bit is set
const XDMA_REGISTER H2C_SGDMA_DSC_ADR_HI={
.target=0x04,
.offset=0x84,
.mask=0xFFFFFFFF,
.attr=RW|CH};
//dsc_adj[5:0]
//Number of extra adjacent descriptors after the start descriptor address
const XDMA_REGISTER H2C_SGDMA_DSC_ADJ={
.target=0x04,
.offset=0x88,
.mask=0x0000003F,
.attr=RW|CH};
//h2c_dsc_credit[9:0]
//Writes to this register will add descriptor credits for the channel.  This
//register will only be used if it is enabled via the channel's bits in the
//Descriptor Credit Mode register (Table 2-123).
//Credits are automatically cleared on the falling edge of the channels Control register Run bit or if Descriptor Credit
// Mode is disabled for the channel. The register can be read to determine th
//e number of current remaining credits for the channel.
const XDMA_REGISTER H2C_SGDMA_DSC_CREDIT={
.target=0x04,
.offset=0x8C,
.mask=0x000003FF,
.attr=RW|CH};


//C2H SGDMA REGISTERS

//DMA Subsystem for PCIe identifier
const XDMA_REGISTER C2H_SGDMA_XDMA_IDENTIFIER={
.target=0x05,
.offset=0x00,
.mask=0xFFF00000,
.attr=RO|CH};
//H2C DMA Target
const XDMA_REGISTER C2H_SGDMA_TARGET={
.target=0x05,
.offset=0x00,
.mask=0x000F0000,
.attr=RO|CH};
//Stream
//1: AXI4-Stream Interface
//0: Memory Mapped AXI4 Interface
const XDMA_REGISTER C2H_SGDMA_STREAM={
.target=0x05,
.offset=0x00,
.mask=0x00008000,
.attr=RO|CH};
//Reserved
const XDMA_REGISTER C2H_SGDMA_RESERVED={
.target=0x05,
.offset=0x00,
.mask=0x00007000,
.attr=RO|CH};
//Channel ID Target [3:0]
const XDMA_REGISTER C2H_SGDMA_CHANNEL_ID={
.target=0x05,
.offset=0x00,
.mask=0x00000F00,
.attr=RO|CH};
//Version
//8'h01: 2015.3 and 2015.4
//8'h02: 2016.1
//8'h03: 2016.2
//8'h04: 2016.3
//8'h05: 2016.4
//8'h06: 2017.1, 2017.2 and 2017.3
const XDMA_REGISTER C2H_SGDMA_VERSION={
.target=0x05,
.offset=0x00,
.mask=0x000000FF,
.attr=RO|CH};
//dsc_adr[31:0]
//Lower bits of start descriptor address. Dsc_adr[63:0] is the first descriptor
//address that is fetched after the Control register Run bit is set
const XDMA_REGISTER C2H_SGDMA_DSC_ADR_LOW={
.target=0x05,
.offset=0x80,
.mask=0xFFFFFFFF,
.attr=RW|CH};
//dsc_adr[63:32]
//Upper bits of start descriptor address.
//Dsc_adr[63:0] is the first descriptor address that is fetched after the Control
//register Run bit is set
const XDMA_REGISTER C2H_SGDMA_DSC_ADR_HI={
.target=0x05,
.offset=0x84,
.mask=0xFFFFFFFF,
.attr=RW|CH};
//dsc_adj[5:0]
//Number of extra adjacent descriptors after the start descriptor address
const XDMA_REGISTER C2H_SGDMA_DSC_ADJ={
.target=0x05,
.offset=0x88,
.mask=0x0000003F,
.attr=RW|CH};
//h2c_dsc_credit[9:0]
//Writes to this register will add descriptor credits for the channel.  This
//register will only be used if it is enabled via the channel's bits in the
//Descriptor Credit Mode register (Table 2-123).
//Credits are automatically cleared on the falling edge of the channels Control register Run bit or if Descriptor Credit
// Mode is disabled for the channel. The register can be read to determine th
//e number of current remaining credits for the channel.
const XDMA_REGISTER C2H_SGDMA_DSC_CREDIT={
.target=0x05,
.offset=0x8C,
.mask=0x000003FF,
.attr=RW|CH};


//SGDMA COMMON REGISTERS


//DMA Subsystem for PCIe identifier
const XDMA_REGISTER C2H_SGDMA_COMMON_XDMA_IDENTIFIER={
.target=0x06,
.offset=0x00,
.mask=0xFFF00000,
.attr=RO};
//SGDMA Target
const XDMA_REGISTER C2H_SGDMA_COMMON_TARGET={
.target=0x06,
.offset=0x00,
.mask=0x000F0000,
.attr=RO};
//Reserved
const XDMA_REGISTER C2H_SGDMA_COMMON_RESERVED={
.target=0x06,
.offset=0x00,
.mask=0x0000FF00,
.attr=RO};
//Version
//8'h01: 2015.3 and 2015.4
//8'h02: 2016.1
//8'h03: 2016.2
//8'h04: 2016.3
//8'h05: 2016.4
//8'h06: 2017.1, 2017.2 and 2017.3
const XDMA_REGISTER C2H_SGDMA_COMMON_VERSION={
.target=0x06,
.offset=0x00,
.mask=0x000000FF,
.attr=RO};
//c2h_dsc_halt[3:0]
//One bit per C2H channel. Set to one to halt descriptor fetches for
//corresponding channel.
const XDMA_REGISTER C2H_SGDMA_COMMON_C2H_DSC_HALT={
.target=0x06,
.offset=0x10,
.mask=0x000F0000,
.attr=RW};
//h2c_dsc_halt[3:0]
//One bit per H2C channel. Set to one to halt descriptor fetches for
//corresponding channel.
const XDMA_REGISTER C2H_SGDMA_COMMON_H2C_DSC_HALT={
.target=0x06,
.offset=0x10,
.mask=0x0000000F,
.attr=RW};
//h2c_dsc_credit_enable [3:0]
//One bit per H2C channel. Set to 1 to enable descriptor crediting. For each
//channel, the descriptor fetch engine will limit the descriptors fetched to the
//number of descriptor credits it is given through writes to the channel's
//Descriptor Credit Register.
const XDMA_REGISTER C2H_SGDMA_COMMON_H2C_DSC_CREDIT_ENABLE={
.target=0x06,
.offset=0x20,
.mask=0x0000000F,
.attr=RW};
//c2h_dsc_credit_enable [3:0]
//One bit per C2H channel. Set to 1 to enable descriptor crediting. For each
//channel, the descriptor fetch engine will limit the descriptors fetched to the
//number of descriptor credits it is given through writes to the channel's Descriptor Credit Register.
const XDMA_REGISTER C2H_SGDMA_COMMON_C2H_DSC_CREDIT_ENABLE={
.target=0x06,
.offset=0x20,
.mask=0x000F0000,
.attr=RW};

//MSI-X Vector Table and PBA
//MSIX_Vector0_Address[31:0]
//MSI-X vector0 message lower address.
const XDMA_REGISTER MSIX_VECTOR0_ADRRESS_LOW={
.target=0x08,
.offset=0x00,
.mask=0xFFFFFFFF,
.attr=RW};
//MSIX_Vector0_Address[63:32]
//MSI-X vector0 message upper address.
const XDMA_REGISTER MSIX_VECTOR0_ADRRESS_HI={
.target=0x08,
.offset=0x04,
.mask=0xFFFFFFFF,
.attr=RW};
//MSIX_Vector0_Data[31:0]
//MSI-X vector0 message data.
const XDMA_REGISTER MSIX_VECTOR0_DATA={
.target=0x08,
.offset=0x08,
.mask=0xFFFFFFFF,
.attr=RW};
//MSIX_Vector0_Control[31:0]
//MSI-X vector0 control.
//Bit Position:
//•  31:1: Reserved.
//•  0: Mask. When set to 1, this
//MSI-X vector is not used to
//generate a message
//.  W h e n  r e s e t  t o  0 ,  t h i s  M S I - X  V e c t o r  i s  u s e d
//to generate a message
const XDMA_REGISTER MSIX_VECTOR0_CONTROL_RESERVED={
.target=0x08,
.offset=0x0C,
.mask=0xFFFFFFFE,
.attr=RW};
const XDMA_REGISTER MSIX_VECTOR0_CONTROL_MESSAGE_DISABLE={
.target=0x08,
.offset=0x0C,
.mask=0x00000001,
.attr=RW};







//---------------------------------------------------FUNCTIONS----------------------------------------------------------------------------------------

static LIST_HEAD(xdev_list);
static DEFINE_MUTEX(xdev_mutex);

static LIST_HEAD(xdev_rcu_list);
static DEFINE_SPINLOCK(xdev_rcu_lock);

#ifndef list_last_entry
#define list_last_entry(ptr, type, member) \
		list_entry((ptr)->prev, type, member)
#endif

static inline void xdev_list_add(XDMA *xdev)
{
	mutex_lock(&xdev_mutex);
	if (list_empty(&xdev_list))
		xdev->idx = 0;
	else {
		XDMA *last;

		last = list_last_entry(&xdev_list, XDMA, list_head);
		xdev->idx = last->idx + 1;
	}
	list_add_tail(&xdev->list_head, &xdev_list);
	mutex_unlock(&xdev_mutex);

	dbg_v("dev %s, xdev 0x%p, xdma idx %d.\n",
		dev_name(&xdev->pdev->dev), xdev, xdev->idx);

	spin_lock(&xdev_rcu_lock);
	list_add_tail_rcu(&xdev->rcu_node, &xdev_rcu_list);
	spin_unlock(&xdev_rcu_lock);
}

#undef list_last_entry

static inline void xdev_list_remove(XDMA *xdev)
{
	mutex_lock(&xdev_mutex);
	list_del(&xdev->list_head);
	mutex_unlock(&xdev_mutex);

	spin_lock(&xdev_rcu_lock);
	list_del_rcu(&xdev->rcu_node);
	spin_unlock(&xdev_rcu_lock);
	synchronize_rcu();
}

static inline XDMA *xdev_find_by_pdev(struct pci_dev *pdev)
{
        XDMA *xdev, *tmp;

        mutex_lock(&xdev_mutex);
        list_for_each_entry_safe(xdev, tmp, &xdev_list, list_head) {
                if (xdev->pdev == pdev) {
                        mutex_unlock(&xdev_mutex);
                        return xdev;
                }
        }
        mutex_unlock(&xdev_mutex);
        return NULL;
}


/*
-----------------------------------------------------
|31:16		| 15:12		|11:8		| 	7:0			|
-----------------------------------------------------
|reserved 	| target 	| Channel 	|	byte offset |
-----------------------------------------------------
*/

/*! \fn u32 __calcAddress(const XDMA_REGISTER item,u8 channel)
    \brief Calculates the offset in the config bar from the XDMA struct and the channel
    \param item the XDMA_REGISTER struct that contains the target and the offset
    \param channel which channel we are reading from
    \returns the offset in the config bar for the specific register
*/
u32 __calcAddress(const XDMA_REGISTER item,u8 channel){
	u32 address=0;
	//calculate the offset in the config bar
	address+=(item.target&0xF)<<12;
	address+=(channel&0xF)<<8;
	address+=(item.offset&0xFF);
	return address;
}


/*! \fn u32 __xdmaRead(struct XDMA* dev,const XDMA_REGISTER item,u8 channel)
    \brief low level function for reading a register
    Used by u32 xdmaRead and xdmaReadChannel
    if the register has a mask then the value is shifted so the lsb of the value is in the lsb if the returned value. Can return -1 if fail
    \param dev the XDMA struct that is used to access the config bar
    \param item which register we are reading from (can be a subset of a register)
    \param channel the channel we are reading from, if not a channel register then this should be zero
    \returns the value of the register
*/
u32 __xdmaRead(struct XDMA* dev,const XDMA_REGISTER item,u8 channel){
	u32 address=0;
	u32 return_value=0;
	u32 mask=item.mask;
	int i=0;
	if (item.attr&WO){
		dbg("Write only in xdmaRead, Target:  %d, channel: %d, offset: %d,mask: %d, config attrb: %d \n",item.target,channel,item.offset,item.mask,item.attr);
		return -1;
	}
	else if(item.attr&RV){
		dbg("Reserved in xdmaRead, Target:  %d, channel: %d, offset: %d,mask: %d, config attrb: %d \n",item.target,channel,item.offset,item.mask,item.attr);
		return -1;
	}
	else if(item.attr&CH){
		dbg("Channelitem in xdmaRead, use xdmaReadChannel, Target:  %d, channel: %d, offset: %d,mask: %d, config attrb: %d \n",item.target,channel,item.offset,item.mask,item.attr);
		return -1;
	}
	//calculate the offset in the config bar
	address=__calcAddress(item,channel);
	//read the value from the bar
	return_value= ioread32(dev->bar[dev->config_bar_idx]+address);
	//if we have a mask then we rightshift it until a one in LSB, assume that the mask is continuous
	if (mask){
		return_value=return_value&item.mask;
		for(i=0;i<32;i++){
			if((mask&0x1)==0){
				mask=mask>>1;
				return_value=return_value>>1;
			}
			else{
				break;
			}
		}
		return return_value;

	}
	else{
		return return_value;
	}

}

/*! \fn u32 __xdmaWrite(struct XDMA* dev,const XDMA_REGISTER item,u32 value,u8 channel)
    \brief low level function to write to a register
    if the register has a mask then the value is masked and shifted to the correct position in the register It will also read the whole register and write back the non masked values
    \param dev the XDMA struct that is used to access the config bar
    \param item which register we are writing to (can be a subset of a register)
    \param value the value which we are writing to
    \returns 0 if success and -1 if fail
*/
u32 __xdmaWrite(struct XDMA* dev,const XDMA_REGISTER item,u32 value,u8 channel){
	u32 address=0;
	u32 return_value=0;
	u32 to_write=value;
	u32 mask=item.mask;
	int i=0;
	if (item.attr&RO){
		dbg("Write only in xdmaWrite, Target:  %d, channel: %d, offset: %d,mask: %d, config attrb: %d \n",item.target,channel,item.offset,item.mask,item.attr);
		return -1;
	}
	else if(item.attr&RV){
		dbg("Reserved in xdmaWrite, Target:  %d, channel: %d, offset: %d,mask: %d, config attrb: %d \n",item.target,channel,item.offset,item.mask,item.attr);
		return -1;
	}
	else if(item.attr&CH){
		dbg("Channelitem in xdmaWrite, use xdmaReadChannel, Target:  %d, channel: %d, offset: %d,mask: %d, config attrb: %d \n",item.target,channel,item.offset,item.mask,item.attr);
		return -1;
	}
	else if(item.attr&RC){
		dbg_v("Clearing on write, Target:  %d, channel: %d, offset: %d,mask: %d, config attrb: %d \n",item.target,channel,item.offset,item.mask,item.attr);
	}
	else if(item.attr&W1C){
		dbg_v("Write 1 to clear, Target:  %d, channel: %d, offset: %d,mask: %d, config attrb: %d \n",item.target,channel,item.offset,item.mask,item.attr);
	}
	else if(item.attr&W1C){
		dbg_v("Write 1 to set, Target:  %d, channel: %d, offset: %d,mask: %d, config attrb: %d \n",item.target,channel,item.offset,item.mask,item.attr);
	}
	//calulcate address
	address=__calcAddress(item,channel);
	if(mask){
		//get the register
		return_value= ioread32(dev->bar[dev->config_bar_idx]+address);
		//clear the value
		return_value=return_value&~mask;
		for(i=0;i<32;i++){
			if((mask&0x1)==0){
				mask=mask>>1;
				to_write=to_write<<1;
			}
			else{
				break;
			}
		}
		to_write=to_write|return_value;
		//dbg("Writing : %u to :, Target:  %d, channel: %d, offset: %d,mask: %d, config attrb: %d \n",to_write,item.target,channel,item.offset,item.mask,item.attr);
		iowrite32(to_write, dev->bar[dev->config_bar_idx]+address);

	}
	else{
		//dbg("Writing : %u to :, Target:  %d, channel: %d, offset: %d,mask: %d, config attrb: %d \n",value,item.target,channel,item.offset,item.mask,item.attr);
		iowrite32(value, dev->bar[dev->config_bar_idx]+address);

	}
	return 0;
}



/*! \fn u32 xdmaReadChannel(struct XDMA* dev,u32 channel,const XDMA_REGISTER& item)
    \brief Read a register from a channel register
    if the register has a mask then the value is shifted so the lsb of the value is in the lsb if the returned value. Can return -1 if fail
    \param dev the XDMA struct that is used to access the config bar
    \param item which register we are reading from (can be a subset of a register)
    \param channel which channel we are reading from
    \returns the value of the register
*/
u32 xdmaReadChannel(struct XDMA* dev,const XDMA_REGISTER item,u8 channel){
	const XDMA_REGISTER temp={
		.target=item.target,
		.offset=item.offset,
		.mask=item.mask,
		//remove channel bit so __xdmaRead does not complain
		.attr=item.attr&~CH};
	if ((item.attr&CH)==0){
		dbg("Reading from a non channel register using xdmaReadChannel, Target:  %d, channel: %d, offset: %d,mask: %d, config attrb: %d \n",item.target,channel,item.offset,item.mask,item.attr);
		return -1;
	}
	return __xdmaRead(dev,temp,channel);
}

/*! \fn u32 xdmaWriteChannel(struct XDMA* dev,const XDMA_REGISTER item, u32 value,u8 channel)
    \brief Writes to a channel register
    if the register has a mask then the value is masked and shifted to the correct position in the register It will also read the whole register and write back the non masked values
    \param dev the XDMA struct that is used to access the config bar
    \param item which register we are writing to (can be a subset of a register)
    \param value the value which we are writing to
    \param channel which channel we are writing to
    \returns 0 if success and -1 if fail
*/
u32 xdmaWriteChannel(struct XDMA* dev,const XDMA_REGISTER item, u32 value,u8 channel){
	const XDMA_REGISTER temp={
		.target=item.target,
		.offset=item.offset,
		.mask=item.mask,
		//remove channel bit so __xdmaWrite does not complain
		.attr=item.attr&~CH};
	if ((item.attr&CH)==0){
		dbg("Writing to a non channel register using xdmaWriteChannel, Target:  %d, channel: %d, offset: %d,mask: %d, config attrb: %d \n",item.target,channel,item.offset,item.mask,item.attr);
		return -1;
	}
	return __xdmaWrite(dev,temp,value,channel);
}

/*! \fn u32 xdmaRead(struct XDMA* dev,const XDMA_REGISTER item)
    \brief Read a register
    if the register has a mask then the value is shifted so the lsb of the value is in the lsb if the returned value. Can return -1 if fail
    \param dev the XDMA struct that is used to access the config bar
    \param item which register we are reading from (can be a subset of a register)
    \returns the value of the register
*/
u32 xdmaRead(struct XDMA* dev,const XDMA_REGISTER item){
	if ((item.attr&CH)!=0){
		dbg("Reading from a channel register using xdmaRead, Target:  %d, channel: %d, offset: %d,mask: %d, config attrb: %d \n",item.target,0,item.offset,item.mask,item.attr);
		return -1;
	}
	return __xdmaRead(dev,item,0);
}

/*! \fn u32 xdmaWrite(struct XDMA* dev,const XDMA_REGISTER item,u32 value)
    \brief Writes to a register
    if the register has a mask then the value is masked and shifted to the correct position in the register It will also read the whole register and write back the non masked values
    \param dev the XDMA struct that is used to access the config bar
    \param item which register we are writing to (can be a subset of a register)
    \param value the value which we are writing to
    \returns 0 if success and -1 if fail
*/
u32 xdmaWrite(struct XDMA* dev,const XDMA_REGISTER item,u32 value){
	if ((item.attr&CH)!=0){
		dbg("Writing to a channel register using xdmaWrite, Target:  %d, channel: %d, offset: %d,mask: %d, config attrb: %d \n",item.target,0,item.offset,item.mask,item.attr);
		return -1;
	}
	return __xdmaWrite(dev,item,value,0);
}

/*! \fn u32 __xdmaReadRaw(void *__iomem mem,const XDMA_REGISTER item)
    \brief Special function that is only used before we know where the config bar is
    \param mem a pointer to the mapped bar
    \param item which register we are writing to (can be a subset of a register)
    \param value the value which we read
*/
u32 __xdmaReadRaw(void *__iomem mem,const XDMA_REGISTER item){
	u32 address=0;
	u32 return_value=0;
	u32 mask=item.mask;
	int i=0;
	//calculate the offset in the config bar
	address=__calcAddress(item,0);
	//read the value from the bar
	return_value= ioread32(mem+address);
//	dbg("__xdmaReadRaw read %d\n",return_value);
	//if we have a mask then we rightshift it until a one in LSB, assume that the mask is continuous
	if (mask){
		return_value=return_value&item.mask;
		for(i=0;i<32;i++){
			if((mask&0x1)==0){
				mask=mask>>1;
				return_value=return_value>>1;
			}
			else{
				break;
			}
		}
	//	dbg("__xdmaReadRaw returns  %d after shift \n",return_value);
		return return_value;

	}
	else{
		return return_value;
	}

}

u32 __xdmaRead32(struct XDMA* dev,u8 target, u8 channel, u8 offset){
		u32 address=0;
		const XDMA_REGISTER temp={
		.target=target,
		.offset=offset,
		.mask=0x0,
		.attr=RW};
		address=__calcAddress(temp,channel);
		return ioread32(dev->bar[dev->config_bar_idx]+address);
}

void __xdmaWrite32(struct XDMA* dev,u8 target, u8 channel, u8 offset,u32 value){
			u32 address=0;
		const XDMA_REGISTER temp={
		.target=target,
		.offset=offset,
		.mask=0x0,
		.attr=RW};
		address=__calcAddress(temp,channel);
		iowrite32(value, dev->bar[dev->config_bar_idx]+address);

}

int check_bypass_bar(XDMA* dev){
	BUG_ON(!dev);
	if(dev->bypass_bar_idx==-1){
		return -1;
	}
	else{
		return 0;
	}
}

u32 read_bypass_bar(XDMA* dev,u32 address){
	if(dev->bypass_bar_idx==-1){
		dbg("doing read_bypass_bar with no bypass bar found\n");
		return 0;
	}
	return ioread32(dev->bar[dev->bypass_bar_idx]+address);
}

void write_bypass_bar(XDMA* dev,u32 address,u32 value){
	if(dev->bypass_bar_idx==-1){
		dbg("doing write_bypass_bar with no bypass bar found\n");
		return;
	}
	iowrite32(value, dev->bar[dev->bypass_bar_idx]+address);
}



int check_user_bar(XDMA* dev){
	BUG_ON(!dev);
	if(dev->user_bar_idx==-1){
		return -1;
	}
	else{
		return 0;
	}
}

u32 read_user_bar(XDMA* dev,u32 address){
	if(dev->user_bar_idx==-1){
		dbg("doing read_user_bar with no user bar found\n");
		return 0;
	}
	return ioread32(dev->bar[dev->user_bar_idx]+address);
}

void write_user_bar(XDMA* dev,u32 address,u32 value){
	if(dev->user_bar_idx==-1){
		dbg("doing write_user_bar with no user bar found\n");
		return;
	}
	iowrite32(value, dev->bar[dev->user_bar_idx]+address);
}


/*! \fn int is_config_bar(XDMA *xdev, int idx)
    \brief Check is a specific bar in the XDMA struct is a config bar
    \param xdev, our XDMA structure
    \param idx which bar to check
    \returns 0 if success and -1 if fail
*/
static int is_config_bar(XDMA *xdev, int idx)
{
	dbg_v("is_config_bar  \n");
	if (!(__xdmaReadRaw(xdev->bar[idx],IRQ_BLOCK_IDENTIFIER)==DMA_SUBSYSTEM_FOR_PCIE_IDENTIFIER)){
		dbg_v("IRQ_BLOCK_IDENTIFIER is not correct in is_config_bar  \n");
		return 0;
	}
	if (!(__xdmaReadRaw(xdev->bar[idx],IRQ_IDENTIFIER)==2)){
		dbg_v("IRQ_IDENTIFIER is not correct in is_config_bar  \n");
		return 0;
	}
	if (!(__xdmaReadRaw(xdev->bar[idx],CONFIG_XDMA_IDENTIFIER)==DMA_SUBSYSTEM_FOR_PCIE_IDENTIFIER)){
		dbg_v("CONFIG_XDMA_IDENTIFIER is not correct in is_config_bar  \n");
		return 0;
	}
	if (!(__xdmaReadRaw(xdev->bar[idx],CONFIG_IDENTIFIER)==3)){
		dbg_v("CONFIG_IDENTIFIER is not correct in is_config_bar  \n");
		return 0;
	}
	return 1;

}

/*! \fn void identify_bars(XDMA *xdev, int *bar_id_list, int num_bars,int config_bar_pos)
    \brief After we have mapped all bars then we try to identify which bar is which
    \param xdev, our XDMA structure
    \param bar_id_list list of bar indexes
    \param config_bar_pos which position the config bar has
*/
static void identify_bars(XDMA *xdev, int *bar_id_list, int num_bars,
			int config_bar_pos)
{
	/*
	 * The following logic identifies which BARs contain what functionality
	 * based on the position of the XDMA config BAR and the number of BARs
	 * detected. The rules are that the user logic and bypass logic BARs
	 * are optional.  When both are present, the XDMA config BAR will be the
	 * 2nd BAR detected (config_bar_pos = 1), with the user logic being
	 * detected first and the bypass being detected last. When one is
	 * omitted, the type of BAR present can be identified by whether the
	 * XDMA config BAR is detected first or last.  When both are omitted,
	 * only the XDMA config BAR is present.  This somewhat convoluted
	 * approach is used instead of relying on BAR numbers in order to work
	 * correctly with both 32-bit and 64-bit BARs.
	 */
	dbg_v("identify_bars  \n");

	BUG_ON(!xdev);
	BUG_ON(!bar_id_list);

	dbg_v("xdev 0x%p, bars %d, config at %d.\n",
		xdev, num_bars, config_bar_pos);

	switch (num_bars) {
	case 1:
		/* Only one BAR present - no extra work necessary */
		break;

	case 2:
		if (config_bar_pos == 0) {
			xdev->bypass_bar_idx = bar_id_list[1];
		} else if (config_bar_pos == 1) {
			xdev->user_bar_idx = bar_id_list[0];
		} else {
			dbg("2, XDMA config BAR unexpected %d.\n",
				config_bar_pos);
		}
		break;

	case 3:
	case 4:
		if ((config_bar_pos == 1) || (config_bar_pos == 2)) {
			/* user bar at bar #0 */
			xdev->user_bar_idx = bar_id_list[0];
			/* bypass bar at the last bar */
			xdev->bypass_bar_idx = bar_id_list[num_bars - 1];
		} else {
			dbg("3/4, XDMA config BAR unexpected %d.\n",
				config_bar_pos);
		}
		break;

	default:
		/* Should not occur - warn user but safe to continue */
		dbg("Unexpected # BARs (%d), XDMA config BAR only.\n",
			num_bars);
		break;

	}
	dbg_v("%d BARs: config %d, user %d, bypass %d.\n",
		num_bars, config_bar_pos, xdev->user_bar_idx,
		xdev->bypass_bar_idx);
}

/*! \fn int map_single_bar(XDMA *xdev, int idx)
    \brief Maps a single bar into our memory space
    \param xdev, our XDMA structure
    \param idx the index of the bar
    \returns 0 if success and -1 if fail
*/
static int map_single_bar(XDMA *xdev, int idx)
{
	resource_size_t bar_start;
	resource_size_t bar_len;
	resource_size_t map_len;

	dbg_v("map_single_bar  \n");
	bar_start = pci_resource_start(xdev->pdev, idx);
	bar_len = pci_resource_len(xdev->pdev, idx);
	map_len = bar_len;

	xdev->bar[idx] = NULL;

	/* do not map BARs with length 0. Note that start MAY be 0! */
	if (!bar_len) {
		//pr_info("BAR #%d is not present - skipping\n", idx);
		return 0;
	}

	/* BAR size exceeds maximum desired mapping? */
	if (bar_len > INT_MAX) {
		dbg("Limit BAR %d mapping from %llu to %d bytes\n", idx,
			(u64)bar_len, INT_MAX);
		map_len = (resource_size_t)INT_MAX;
	}
	/*
	 * map the full device memory or IO region into kernel virtual
	 * address space
	 */
	dbg_v("BAR%d: %llu bytes to be mapped.\n", idx, (u64)map_len);
	xdev->bar[idx] = pci_iomap(xdev->pdev, idx, map_len);

	if (!xdev->bar[idx]) {
		dbg("Could not map BAR %d.\n", idx);
		return -1;
	}

	dbg_v("BAR%d at 0x%llx mapped at 0x%p, length=%llu(/%llu)\n", idx,
		(u64)bar_start, xdev->bar[idx], (u64)map_len, (u64)bar_len);

	return (int)map_len;
}

/*! \fn void unmap_bars(XDMA *xdev)
    \brief unmap the pci device bars
    Should not fail
    \param xdev, our XDMA structure
*/

static void unmap_bars(XDMA *xdev)
{
	int i;
	dbg_v("unmap_bars  \n");
	for (i = 0; i < XDMA_BAR_NUM; i++) {
		/* is this BAR mapped? */
		if (xdev->bar[i]) {
			/* unmap BAR */
			pci_iounmap(xdev->pdev, xdev->bar[i]);
			/* mark as unmapped */
			xdev->bar[i] = NULL;
		}
	}
}



/* map_bars() -- map device regions into kernel virtual address space
 *
 * Map the device memory regions into kernel virtual address space after
 * verifying their sizes respect the minimum sizes needed
 */
/*! \fn  int map_bars(XDMA *xdev)
    \brief Maps the different bars into our memory space
    The number of different bars can be view by checking user_bar_idx,config_bar_idx,bypass_bar_idx in the XDMA struct
    and the pointer to the bars the be accessed through xdev->bar
    \param xdev the XDMA struct that was returned from XDMA_init
    \return 0 if success and -1 if fail
*/
static int map_bars(XDMA *xdev)
{
	int i;
	int bar_id_list[XDMA_BAR_NUM];
	int bar_id_idx = 0;
	int config_bar_pos = 0;

	dbg_v("map_bars  \n");
	/* iterate through all the BARs */
	for (i = 0; i < XDMA_BAR_NUM; i++) {
		int bar_len;

		bar_len = map_single_bar(xdev, i);
		if (bar_len == 0) {
			continue;
		} else if (bar_len < 0) {
			goto fail;
		}

		/* Try to identify BAR as XDMA control BAR */
		if ((bar_len >= XDMA_BAR_SIZE) && (xdev->config_bar_idx < 0)) {

			if (is_config_bar(xdev, i)) {
				xdev->config_bar_idx = i;
				config_bar_pos = bar_id_idx;
				dbg_v("config bar %d, pos %d.\n",xdev->config_bar_idx, config_bar_pos);
			}
		}

		bar_id_list[bar_id_idx] = i;
		bar_id_idx++;
	}

	/* The XDMA config BAR must always be present */
	if (xdev->config_bar_idx < 0) {
		dbg("Failed to detect XDMA config BAR\n");
		goto fail;
	}

	identify_bars(xdev, bar_id_list, bar_id_idx, config_bar_pos);

	/* successfully mapped all required BAR regions */
	return 0;

fail:
	/* unwind; unmap any BARs that we did map */
	unmap_bars(xdev);
	return -1;
}


/*! \fn  int set_dma_mask(XDMA* xdev)
    \brief Check if we can use 64bit or 32bit DMA addressing
    if it returns 0 and bit64_dma_enabled is equal to 1 in the XDMA struct then we can use 64bit
    If it return 0 and bit64_dma_enabled is equal to 0 then we can use 32bit mode
    if it returns -1 then no DMA is suitable
    \param xdev the XDMA struct that was returned from XDMA_init
    \return 0 if success and -1 if fail
*/
 int set_dma_mask(XDMA* xdev)
{
	dbg_v("set_dma_mask  \n");
	BUG_ON(!xdev);

	dbg_v("sizeof(dma_addr_t) == %ld\n", sizeof(dma_addr_t));
	/* 64-bit addressing capability for XDMA? */
	if (!pci_set_dma_mask(xdev->pdev, DMA_BIT_MASK(64))) {
		/* query for DMA transfer */
		/* @see Documentation/DMA-mapping.txt */
		dbg_v("pci_set_dma_mask()\n");
		/* use 64-bit DMA */
		dbg_v("Using a 64-bit DMA mask.\n");
		/* use 64-bit DMA for descriptors */
		pci_set_consistent_dma_mask(xdev->pdev, DMA_BIT_MASK(64));
		/* use 64-bit DMA, 32-bit for consistent */
		xdev->bit64_dma_enabled=1;
	} else if (!pci_set_dma_mask(xdev->pdev, DMA_BIT_MASK(32))) {
		dbg("Could not set 64-bit DMA mask.\n");
		pci_set_consistent_dma_mask(xdev->pdev, DMA_BIT_MASK(32));
		/* use 32-bit DMA */
 	} else {
		dbg("No suitable DMA possible.\n");
		return -1;
	}

	return 0;
}

/*! \fn int probe_h2c_engine(XDMA* xdev,int index)
    \brief Probes the XDMA device for h2c engine with a specific index
    \param xdev the XDMA struct that was returned from XDMA_init
    \param index the index of an engine which we are looking for
    \return 0 if success and -1 if fail
*/
int probe_h2c_engine(XDMA* xdev,u8 index){
	u32 idx=0;
	dbg_v("probe_h2c_engine  \n");
	if(xdmaReadChannel(xdev,H2C_CHANNEL_IDENTIFIER,index)!=DMA_SUBSYSTEM_FOR_PCIE_IDENTIFIER){
		dbg_v("H2C_CHANNEL_IDENTIFIER does not match in probe_h2c_engine.\n");
		return -1;
	}
	if(xdmaReadChannel(xdev,H2C_CHANNEL_TARGET,index)!=0){
		dbg_v("H2C_CHANNEL_TARGET does not match in probe_h2c_engine.\n");
		return -1;
	}
	idx=xdmaReadChannel(xdev,H2C_CHANNEL_CHANNELID,index);
	if(idx!=index){
		dbg_v("H2C_CHANNEL_CHANNELID does not match in probe_h2c_engine.\n");
		return -1;
	}
	xdev->h2c_channels[xdev->h2c_number_channels]=idx;
	xdev->h2c_number_channels=xdev->h2c_number_channels+1;
	return 0;
}

/*! \fn int probe_c2h_engine(XDMA* xdev,int index)
    \brief Probes the XDMA device for c2h engine with a specific index
    \param xdev the XDMA struct that was returned from XDMA_init
    \param index the index of an engine which we are looking for
    \return 0 if success and -1 if fail
*/
int probe_c2h_engine(XDMA* xdev,int index){
	u32 idx=0;
	dbg_v("probe_c2h_engine  \n");
	if(xdmaReadChannel(xdev,C2H_CHANNEL_IDENTIFIER,index)!=DMA_SUBSYSTEM_FOR_PCIE_IDENTIFIER){
		dbg("H2C_CHANNEL_IDENTIFIER does not match in probe_h2c_engine.\n");
		return -1;
	}
	if(xdmaReadChannel(xdev,C2H_CHANNEL_TARGET,index)!=1){
		dbg("H2C_CHANNEL_TARGET does not match in probe_h2c_engine.\n");
		return -1;
	}
	idx=xdmaReadChannel(xdev,H2C_CHANNEL_CHANNELID,index);
	if(idx!=index){
		dbg("H2C_CHANNEL_CHANNELID does not match in probe_h2c_engine.\n");
		return -1;
	}
	xdev->c2h_channels[xdev->c2h_number_channels]=idx;
	xdev->c2h_number_channels=xdev->c2h_number_channels+1;

	return 0;
}

/*! \fn void probe_engines(XDMA* xdev)
    \brief Probes the XDMA device for dma engines
    If it finds anything then it increments either c2h_number_channels or h2c_number_channels in the XDMA struct.
    The index for the engines are stored in c2h_channels and h2c_channels
    \param xdev the XDMA struct that was returned from XDMA_init
*/
void probe_engines(XDMA* xdev){
	int i=0;
	int rv=0;
	dbg_v("probe_engines  \n");
	for(i=0;i<XDMA_CHANNEL_NUM_MAX;i++){
		rv=probe_c2h_engine(xdev,i);
		if (rv){
			break;
		}
	}
	for(i=0;i<XDMA_CHANNEL_NUM_MAX;i++){
		rv=probe_h2c_engine(xdev,i);
		if (rv){
			break;
		}
	}

}

/*! \fn void remove_XDMA(XDMA* xdev)
    \brief Undoes everything that init_XDMA does
    1. Unmaps the bars
    2. Releases the regions
    3. Disables the device

    \param xdev the XDMA struct that was returned from XDMA_init
*/
void remove_XDMA(XDMA* xdev){
	dbg_v("remove_XDMA  \n");
	unmap_bars(xdev);
	if (xdev->got_regions){
		pci_release_regions(xdev->pdev);
	}
	if (!xdev->regions_in_use){
			pci_disable_device(xdev->pdev);
		}
	if(xdev->idx!=-1){
		xdev_list_remove(xdev);
	}
	kfree(xdev);
}

/*! \fn XDMA* init_XDMA(struct pci_dev *pdev)
    \brief Initializes a XDMA struct from the pci_dev
    This does all the basic setup and should be called first in the probe function for the driver
    It creates allocates memory for a XDMA struct and initializes it then
    1. enables the pdi device
    2. enables relaxed ordering(needed?)
    3. Forces MRRS to 513
    4. Enables bus mastering capabilities
    5. Requests th regions
    6. Maps the bars
    7. Locates the config bar
    8. Resets all the DMA enable masks to 0
    9. It probes for the XDMA engines (only checks if they are there)
    If everything is successfull then it should return a non NULL pointer and
    c2h_number_channels or h2c_number_channels in the XDMA struct should be non zero

    \param pdev the pci_dev struct passed to the probe function
    \returns a pointer to a XDMA struct or NULL if something failed
*/
XDMA* init_XDMA(struct pci_dev *pdev){
	XDMA* xdev;
	int rv = 0;
	int idx=0;
	dbg_v("init_XDMA  \n");
	//allocate XDMA struct
	xdev = kzalloc(sizeof(XDMA), GFP_KERNEL);
	if (!xdev) {
		dbg("allocation in init_XDMA failed");
		goto fail;
	}
	xdev->pdev = pdev;
	xdev->config_bar_idx = -1;
	xdev->user_bar_idx = -1;
	xdev->bypass_bar_idx = -1;
	xdev->regions_in_use=0;
	xdev->got_regions=0;
	xdev->bit64_dma_enabled=0;
	xdev->c2h_number_channels=0;
	xdev->h2c_number_channels=0;
	xdev->idx=-1;
	for (idx=0;idx<XDMA_CHANNEL_NUM_MAX;idx++){
		xdev->c2h_channels[idx]=-1;
		xdev->h2c_channels[idx]=-1;
	}
	//init bar pointers
	for(idx=0;idx<XDMA_BAR_NUM;idx++){
		xdev->bar[idx]=NULL;
	}

	//add device to list
	xdev_list_add(xdev);
	if(xdev_find_by_pdev(pdev)!=xdev){
		dbg("Duplicate xdev in list\n");
		goto fail;
	}

	//enable pci device before we do anything with it
	rv = pci_enable_device(pdev);
	if (rv) {
		dbg("pci_enable_device() failed, %d.\n", rv);
		goto fail;
	}

	//enable relaxed ordering
	pcie_capability_set_word(pdev, PCI_EXP_DEVCTL, PCI_EXP_DEVCTL_RELAX_EN);

	/* force MRRS to be 512 */
	rv = pcie_set_readrq(pdev, 512);
	if (rv){
		dbg("device %s, error set PCI_EXP_DEVCTL_READRQ: %d.\n",dev_name(&pdev->dev), rv);
	}

	/* enable bus master capability */
	pci_set_master(pdev);

	rv = pci_request_regions(pdev, DRV_MODULE_NAME);
	/* could not request all regions? */
	if (rv) {
		dbg("pci_request_regions() = %d, device in use?\n", rv);
		/* assume device is in use so do not disable it later */
		xdev->regions_in_use = 1;
		goto fail;
	} else {
		xdev->got_regions = 1;
	}

	//map bars
	rv = map_bars(xdev);
	if (rv){
		goto fail;
	}
	rv=set_dma_mask(xdev);
	if(rv){
		goto fail;
	}

	//zero channel IRQ enable mask
	xdmaWrite(xdev,IRQ_CHANNEL_INT_ENMASK,0);
	//zero user IRQ enable mask
	xdmaWrite(xdev,IRQ_USER_INT_ENMASK,0);

	probe_engines(xdev);
	//we did not find any engines
	if (xdev->c2h_number_channels==0&&xdev->h2c_number_channels==0){
		goto fail;
	}
	return xdev;
	fail:
		remove_XDMA(xdev);
		return NULL;
}
