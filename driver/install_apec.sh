#!/bin/bash

MODULE=apec
echo "Installing rf_apec driver"
if lsmod | grep apec &> /dev/null ; then
	echo "Module apec already loaded"
else
	echo "executing /sbin/insmod apec.ko ${@:1:99}"
	/sbin/insmod apec.ko ${@:1:99}
fi
