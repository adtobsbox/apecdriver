# libXDMA

libXDMA is an abstraction layer to talk to the Xilinx dma bridge ip core

In your probe function you simply have to call init_XDMA

```
static int probe_one(struct pci_dev *pdev, const struct pci_device_id *id)
{
	XDMA* xdev=NULL;
	xdev=init_XDMA(pdev);
	if (xdev==NULL){
		return -EINVAL;
	}

	dev_set_drvdata(&pdev->dev, xdev);
	return 0;


}
```
And in your remove function you simply have to call 
remove_XDMA

```
static void remove_one(struct pci_dev *pdev)
{
	XDMA *xdev = dev_get_drvdata(&pdev->dev);
	remove_XDMA(xdev);

}
```

Afterwards you can read and write to all registers using:
```
u32 xdmaReadChannel(struct XDMA* dev,const XDMA_REGISTER item,u8 channel );
u32 xdmaWriteChannel(struct XDMA* dev,const XDMA_REGISTER item, u32 value,u8 channel);
u32 xdmaRead(struct XDMA* dev,const XDMA_REGISTER item);
u32 xdmaWrite(struct XDMA* dev,const XDMA_REGISTER item,u32 value) ;
```
for example:
```
xdmaReadChannel(xdev,H2C_CHANNEL_IDENTIFIER,0)
```
Will return the channel identifier for the first H2C channel
```
xdmaWrite(xdev,IRQ_CHANNEL_INT_ENMASK,0);
```
Will disable the channel IRQs.
All registers which you can write to are defined in libXDMA.c with descriptions.

The registers are from :
https://www.xilinx.com/support/documentation/ip_documentation/xdma/v4_0/pg195-pcie-dma.pdf