/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the BE-RF-FB section      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "LICENCE".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef APEC_H
#define APEC_H

#include "libXDMA.h"


#define DEVICES_PER_CARD (100)
#define MAX_USERS (512)
#define MAX_ALLOCATION (1048576) //1MB
#define MAX_ALLOCATION_BUFFERS (10)
//The maximum memory that can be allocated for descriptors and writebacks are MAX_ALLOCACTION*MAX_ALLOCATION_BUFFERS

//Used to print bytes in binary
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

struct TRANSFER{
	void* memory;
	dma_addr_t handle;
	u32 dst_addr_lo;	/* destination address (low 32-bit) */
	u32 dst_addr_hi;	/* destination address (high 32-bit) */
	int idx;
	u64 offset;
	struct TRANSFER* next;
	struct TRANSFER* previous;
};
typedef struct TRANSFER TRANSFER;

//used for characted devices to have a link to the APEC struct
struct CDEV_APEC{
	struct cdev c_dev;
	struct APEC* adev;
};
typedef struct CDEV_APEC CDEV_APEC;

struct SETUP_STREAM{
	s64 offset;
	u64 turns;
}__packed;
typedef struct SETUP_STREAM SETUP_STREAM;

struct STREAM_USER_CONTAINER{
	s64 offset;
	u64 turns;
	u32 stream;
	long unsigned idx;
	u32 transfer_idx;
	u64* buffer;
	unsigned long timeout;
	struct APEC* adev;
};
typedef struct STREAM_USER_CONTAINER STREAM_USER_CONTAINER;


//this struct must be 64b aligned to remove false sharing between CPUs
struct ATOMIC_CONTAINER {
atomic_t user_lock;
} __attribute__ ((aligned (64)));
typedef struct ATOMIC_CONTAINER ATOMIC_CONTAINER;

struct APEC{
	//for devices
	char device_head[32];
	char thread_name[32];
	u8 bus_nr;
	u8 dev_nr;
	dev_t first;
	u32 minor;
	u32 major;

	struct class *cl[DEVICES_PER_CARD];
	struct device *dev[DEVICES_PER_CARD];
	CDEV_APEC cdevs[DEVICES_PER_CARD];
	u32 streamMinorNumbers[10];
	u32 directMinorNumbers[10];
	u32 frameMinorNumbers[10];
	u32 contMinorNumbers[10];
	u32 streamStatusMinorNumbers[10];
	u32 streamInterruptMinorNumbers[10];

	int interfaces_created;
	int devices_number;


	XDMA* xdev;
	struct msix_entry entry;
	int msix_irq_line_user[10];
	int msix_irq_line;
	int msix_irq_line_h2c;
	int msix_enabled;
	int msix_setup;
	int msix_setup_user;
	//------------memory for transfers
	struct TRANSFER* transfer;
	struct TRANSFER* current_transfer;
	u32 descriptors;
	//memory for TRANSFER structs
	void* buffer_transfers;

	//lock access the to channel
	spinlock_t lock;
	//tell readers which transfer was the latest
	atomic_t transfer_idx_atomic;
	atomic_t direct_transfer_idx_atomic;
	atomic_t descriptor_counter_atomic;
	atomic_t descriptor_match_errors_atomic;
	atomic_t descriptor_counter_rollover_atomic;
	atomic_t irq_count_atomic;
	atomic_t last_descriptor;
	atomic_t missed_turns;
	u32 last_counter;
	//used to tell the driver to reset the hardware
	atomic_t reset;
	//------------user sync-----------------------
	ATOMIC_CONTAINER users_locks[MAX_USERS] __attribute__ ((aligned (64)));

	//atomic_t users_locks[MAX_USERS];
	long unsigned users_bitmap[MAX_USERS/32+1];
	//unsigned long users_bitmap;
	spinlock_t users_setup_lock;
	//-----------user interrupts------------------
	wait_queue_head_t user_interrupt_queues[10];
	atomic_t virtual_user_interrupt[10];
	struct work_struct after_user_interrupt;
	ATOMIC_CONTAINER users_interrupt_locks[10][MAX_USERS];
	long unsigned users_interrupt_bitmap[10][MAX_USERS/32+1];
	//--------------Thread for polling-----------------------
	struct task_struct *thread_st;
	//buffer for writeback in polling mode
	volatile void* buffer_writeback;
	dma_addr_t dma_handle_writeback;

	//for direct access to user bar
	u32 bus_address;
	u32 bus_data;


};
typedef struct APEC APEC;



struct USER_REGISTER{
	u16 offset;
	u32 mask;
};
typedef struct USER_REGISTER USER_REGISTER;

const USER_REGISTER TRANSFERSIZE_PER_CHANNEL;
const USER_REGISTER CHANNELS;
const USER_REGISTER TRANSFERTOTAL;
const USER_REGISTER CLOCKSETUP;
const USER_REGISTER TIMEOUTMIN;
const USER_REGISTER TIMEOUTMAX;
const USER_REGISTER SIZEPERFRAME;
const USER_REGISTER CPURESET;
const USER_REGISTER POLLING;



#endif
