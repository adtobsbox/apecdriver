/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the BE-RF-FB section      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "LICENCE".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "apec-base.h"

/*! \var  DEVICE_TREE_HEAD[]
    \brief The root of the device tree. This will create the devices in
   /dev/APEC
*/
static const char DEVICE_TREE_HEAD[] = "APEC!";

/*
--transferSizePerChannel must be big enough to contain one frame and must be a
multiple of PAGE_SIZE (4096)
--channels can be anything from 1 to 10 as long as the pcie bandwidth is enough
--timeoutMin is the minimum time that the swap process waits before it swaps the
buffer and initiate a transfer, this is only important if no channels are
connected and it makes sure that it is not swapping all the time
--timeoutMax is the maximum time it waits for all streams to finish a frame,
after this it does a forced swap and the data can be corrupted
*/

/*! \var USER_REGISTER TRANSFERSIZE_PER_CHANNEL
    \brief Used to write the transferSizePerChannel to the correct
   register in the user bar
*/
const USER_REGISTER TRANSFERSIZE_PER_CHANNEL = {.offset = 0x0,
                                                .mask = 0xffffffff};

/*! \var USER_REGISTER CHANNELS
    \brief Used to write the channels to the correct register in
   the user bar
*/
const USER_REGISTER CHANNELS = {.offset = 0x4, .mask = 0xffffffff};

/*! \var USER_REGISTER PADDING
    \brief The padding is calculated from transfertotal, transferOerChannel and
   channels and is set so one transfer is 4096byte aligned
*/
const USER_REGISTER TRANSFERTOTAL = {.offset = 0x8, .mask = 0xffffffff};

/*! \var USER_REGISTER CLOCKSETUP
    \brief Not used yet but will be used to setup the clock dynamically for
   different modes in the future
*/
const USER_REGISTER CLOCKSETUP = {.offset = 0xc, .mask = 0xffffffff};

/*! \var USER_REGISTER WAITUNBREAKABLE
    \brief Used to write the seletectedMode.waitUnbreakable to the correct
   register in the user bar
*/
const USER_REGISTER TIMEOUTMIN = {.offset = 0x10, .mask = 0xffffffff};

/*! \var USER_REGISTER WAITBREAKABLE
    \brief Used to write the seletectedMode.waitBreakable to the correct
   register in the user bar
*/
const USER_REGISTER TIMEOUTMAX = {.offset = 0x14, .mask = 0xffffffff};

/*! \var USER_REGISTER SIZEPERFRAME
    \brief The actual size of the frame, this is used to locate the CRC checksum
*/
const USER_REGISTER SIZEPERFRAME = {.offset = 0x18, .mask = 0xffffffff};

const USER_REGISTER CPURESET = {.offset = 0x1c, .mask = 0xffffffff};

const USER_REGISTER POLLING = {.offset = 0x218, .mask = 0xffffffff};

/*! \var int transfers
    \brief Module argument to tell the module how many transfers to allocate per
   card, default=1
*/
static ulong transfers = (1024ULL);
static ulong t = (1024ULL);
module_param(t, ulong, 0660);
MODULE_PARM_DESC(t,
                 "the number of buffered transfers per card [default=1024] ");

/*! \var int polling
    \brief Module argument to tell the module to use interrupts or polling,
   default=1
*/
static ulong polling = 1; // polling or interrupt
static ulong p = 1; // polling or interrupt

module_param(p, ulong, 0660);
MODULE_PARM_DESC(p,
                 "Tell the module to use interrupts or polling, [default=1]");

/*! \var int transferSizePerChannel
    \brief Module argument to tell the module the maximum size of each transfer,
   default=1
*/
static ulong transferSizePerChannel = 8192;
static ulong ts = 8192;

module_param(ts, ulong, 0660);
MODULE_PARM_DESC(ts,
                 "The maximum size per transfer, [default=8192]");

/*! \var int channels
    \brief Module argument to tell the module how many channels to enable,
   default=1
*/
static ulong channels = 8;
static ulong c = 8;

module_param(c, ulong, 0660);
MODULE_PARM_DESC(c, "The number of enabled channels, [default=8]");

/*! \var int timeoutmin
    \brief Module argument to tell the module the minimum time in 200Mhz clock
   cycles before a new transfer can be initiated , default=0
*/
static ulong timeoutmin = 0;
static ulong ti = 0;
module_param(ti, ulong, 0660);
MODULE_PARM_DESC(ti, "The minimum time in 200Mhz clock cycles before a "
                             "new transfer can be initiated, [default=0]");

/*! \var int timeoutmin
    \brief Module argument to tell the module the minimum time in 200Mhz clock
   cycles before a new transfer can be initiated , default=0
*/
static ulong timeoutmax = 35000;
static ulong ta = 35000;
module_param(ta, ulong, 0660);
MODULE_PARM_DESC(ta,
                 "The maximum time in 200Mhz clock cycles before a new "
                 "transfer is forcefully initiated, [default=35000]");

/*! \var struct pci_device_id pci_ids[]
    \brief The PCIe ids which the module is loaded for
*/
static const struct pci_device_id pci_ids[] = {{PCI_DEVICE(0x10ee, 0x7038)},
                                               {
                                                   0,
                                               }};

/*! \fn  void apec_write_user_bar(APEC* adev,const USER_REGISTER item,u32 value)
    \brief Writes to the user register
    If the USER_REGISTER has a bitmask then it only writes and reads from those
   bit and it does the bitshift for the user, this assumes that the bitmask is
   continous \param adev our APEC struct \param item one USER_REGISTER struct to
   tell it which register to write to, this can contain a bitmask \param value
   the value to write
*/
static void apec_write_user_bar(APEC *adev, const USER_REGISTER item,
                                u32 value) {
  u32 to_write = value;
  u32 mask = item.mask;
  u32 return_value = 0;
  int i = 0;
  BUG_ON(!adev);

  if (mask) {
    // get the register
    return_value = read_user_bar(adev->xdev, item.offset);
    // clear the value
    return_value = return_value & ~mask;
    for (i = 0; i < 32; i++) {
      if ((mask & 0x1) == 0) {
        mask = mask >> 1;
        to_write = to_write << 1;
      } else {
        break;
      }
    }
    to_write = to_write | return_value;
    write_user_bar(adev->xdev, item.offset, to_write);

  } else {
    write_user_bar(adev->xdev, item.offset, value);
  }
}

/*! \fn  u32 apec_read_user_bar(APEC* adev,const USER_REGISTER item)
    \brief reads from the user register
    If the USER_REGISTER has a bitmask then it only writes and reads from those
   bit and it does the bitshift for the user, this assumes that the bitmask is
   continous \param adev our APEC struct \param item one USER_REGISTER struct to
   tell it which register to read from, this can contain a bitmask \returns The
   value from the register
*/
__attribute__((unused)) static u32
apec_read_user_bar(APEC *adev, const USER_REGISTER item) {
  u32 return_value = 0;
  u32 mask = item.mask;
  int i = 0;

  return_value = read_user_bar(adev->xdev, item.offset);
  // if we have a mask then we rightshift it until a one in LSB, assume that the
  // mask is continuous
  if (mask) {
    return_value = return_value & item.mask;
    for (i = 0; i < 32; i++) {
      if ((mask & 0x1) == 0) {
        mask = mask >> 1;
        return_value = return_value >> 1;
      } else {
        break;
      }
    }
    return return_value;

  } else {
    return return_value;
  }
}

static void apec_write_descriptor_to_bypass_bar(APEC *adev, TRANSFER *desc,
                                                u32 idx) {
  BUG_ON(!adev);
  BUG_ON(!adev->xdev);
  if (idx > 510) {
    dbg("max index of descriptor in bypass is 510 \n");
    return;
  }

  write_bypass_bar(adev->xdev, idx * 8, desc->dst_addr_lo);
  write_bypass_bar(adev->xdev, idx * 8 + 4, desc->dst_addr_hi);
}

static u64 apec_bypass_read(APEC *adev, u32 idx) {
  u64 temp = 0;
  BUG_ON(!adev);
  BUG_ON(!adev->xdev);

  if (idx > 511) {
    dbg("max index of descriptor in bypass is 510 \n");
    return -1;
  }
  temp = read_bypass_bar(adev->xdev, (idx * 8) + 4);
  temp = temp << 32;
  temp |= read_bypass_bar(adev->xdev, idx * 8);
  return temp;
}

static u32 apec_read_bypass_offset(APEC *adev) {
  BUG_ON(!adev);
  BUG_ON(!adev->xdev);
  return read_bypass_bar(adev->xdev, 511 * 8) / 8;
}

static void apec_zero_bypass_bar(APEC *adev) {
  u32 i = 0;
  BUG_ON(!adev);
  BUG_ON(!adev->xdev);
  for (i = 0; i < 512; i++) {
    write_bypass_bar(adev->xdev, i * 8, 0);
    write_bypass_bar(adev->xdev, i * 8 + 4, 0);
  }
}

/*! \fn  int msix_capable(struct pci_dev *dev)
    \brief Check if the device is MSI-X capable
    \param dev the pci_dev struct that was returned from the probe function
    \return 1 if success and 0 if fail
*/
static int msix_capable(struct pci_dev *dev) {
  struct pci_bus *bus;

  if (!dev || dev->no_msi)
    return 0;

  for (bus = dev->bus; bus; bus = bus->parent)
    if (bus->bus_flags & PCI_BUS_FLAGS_NO_MSI)
      return 0;

  if (!pci_find_capability(dev, PCI_CAP_ID_MSIX))
    return 0;

  return 1;
}

/*! \fn  void disable_msix(APEC* adev)
    \brief Disables the MSI-X for this device
    \param adev the APEC struct for this device
*/
static void disable_msix(APEC *adev) {
  XDMA *xdev = NULL;
  BUG_ON(!adev);
  if (adev->msix_enabled == 0) {
    dbg("msix not enabled in disable_msix\n");
    return;
  }

  xdev = adev->xdev;
  BUG_ON(!xdev);
  BUG_ON(!(xdev->pdev));
  pci_disable_msix(xdev->pdev);
  adev->msix_enabled = 0;
}

/*! \fn  int enable_msix(APEC* adev)
    \brief Enables the MSI-X for this device
    \param adev the APEC struct for this device
    \returns 0 if success and -i if fail
*/
static int enable_msix(APEC *adev) {
  int rv = 0;
  __attribute__((unused)) int i = 0;
  XDMA *xdev = NULL;

  BUG_ON(!adev);
  xdev = adev->xdev;
  BUG_ON(!xdev);
  BUG_ON(!xdev->pdev);

  if (msix_capable(xdev->pdev)) {
    dbg_v("Enabling MSI-X\n");
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4, 12, 0)
    rv = pci_alloc_irq_vectors(xdev->pdev, 12, 12, PCI_IRQ_MSIX);
#else
    for (i = 0; i < 12; i++) {
      xdev->entry[i].entry = i;
    }
    rv = pci_enable_msix(xdev->pdev, xdev->entry, 12);
#endif
    if (rv < 0) {
      dbg("Couldn't enable MSI-X mode: %d\n", rv);
    }
  } else {
    return -1;
  }
  adev->msix_enabled = 1;

  return rv;
}

/*! \fn  irqreturn_t xdma_channel_irq(int irq, void *dev_id)
    \brief This is the IRQ handler for this device
    \param irq the irq number
    \param dev_id the device id for this device which always is a APEC struct
    \returns 0
*/
static irqreturn_t xdma_channel_irq(int irq, void *dev_id) {
  APEC *adev = NULL;
  u32 idx;
  u32 last_descriptor;
  u32 current_descriptor;
  adev = (APEC *)dev_id;

  xdmaWrite(adev->xdev, IRQ_CHANNEL_INT_ENMASK, 0x0);

  last_descriptor = atomic_read(&adev->last_descriptor);
  idx = __xdmaRead32(adev->xdev, 0x01, 0x00, 0x44);
  // get the last descriptor which we think the FPGA used
  // Get the Current descriptor the FPGA is using, this should be the one we
  // think+1
  current_descriptor = apec_read_bypass_offset(adev);

  if (atomic_read(&adev->reset) == 1) {
    atomic_set(&adev->reset, 0);
    // stop engine
    xdmaWriteChannel(adev->xdev, C2H_CHANNEL_CONTROL_RUN, 0x0, 0);
    // full reset
    apec_write_user_bar(adev, CPURESET, 0xffffffff);
    adev->transfer = adev->current_transfer;
    for (idx = 0; idx < 511; idx++) {
     dma_sync_single_for_device(
         &adev->xdev->pdev->dev, adev->current_transfer->handle,
         (transferSizePerChannel * channels),
         DMA_FROM_DEVICE);
      apec_write_descriptor_to_bypass_bar(adev, adev->current_transfer, idx);
      adev->current_transfer = adev->current_transfer->next;
    }
    apec_write_user_bar(adev, CPURESET, 0x0);
    xdmaWriteChannel(adev->xdev, C2H_CHANNEL_CONTROL_RUN, 0x1, 0);
    last_descriptor = 0;
    xdmaWrite(adev->xdev, IRQ_CHANNEL_INT_ENMASK, 0x2);
    return IRQ_HANDLED;
  }

  // the FPGA is faster than us
  if (current_descriptor > last_descriptor + 16) {
    while (last_descriptor <= current_descriptor - 1) {
     dma_sync_single_for_device(
         &adev->xdev->pdev->dev, adev->transfer->handle,
         (transferSizePerChannel * channels),
         DMA_FROM_DEVICE);
      apec_write_descriptor_to_bypass_bar(adev, adev->transfer,
                                          last_descriptor);
      last_descriptor++;
      adev->transfer = adev->transfer->next;
      dma_sync_single_for_cpu(
          &adev->xdev->pdev->dev, adev->current_transfer->handle,
          (transferSizePerChannel * channels),
          DMA_FROM_DEVICE);
      adev->current_transfer = adev->current_transfer->next;
      atomic_inc(&adev->missed_turns);
    }
    atomic_set(&adev->last_descriptor, last_descriptor);
    atomic_set(&adev->transfer_idx_atomic, adev->current_transfer->idx);
    adev->transfer = adev->transfer->next;
    dma_sync_single_for_cpu(
        &adev->xdev->pdev->dev, adev->current_transfer->handle,
        (transferSizePerChannel * channels),
        DMA_FROM_DEVICE);
    adev->current_transfer = adev->current_transfer->next;
    dbg("skipped 16 turns to catch up\n");
  } else if (last_descriptor == 510) {
    apec_write_descriptor_to_bypass_bar(adev, adev->transfer, last_descriptor);
    atomic_set(&adev->last_descriptor, 0);
    atomic_set(&adev->transfer_idx_atomic, adev->current_transfer->idx);
    adev->transfer = adev->transfer->next;
    dma_sync_single_for_cpu(
        &adev->xdev->pdev->dev, adev->current_transfer->handle,
        (transferSizePerChannel * channels),
        DMA_FROM_DEVICE);
    adev->current_transfer = adev->current_transfer->next;
  } else {
    apec_write_descriptor_to_bypass_bar(adev, adev->transfer, last_descriptor);
    atomic_set(&adev->last_descriptor, last_descriptor + 1);
    atomic_set(&adev->transfer_idx_atomic, adev->current_transfer->idx);
    adev->transfer = adev->transfer->next;
    dma_sync_single_for_cpu(
        &adev->xdev->pdev->dev, adev->current_transfer->handle,
        (transferSizePerChannel * channels),
        DMA_FROM_DEVICE);
    adev->current_transfer = adev->current_transfer->next;
  }

  //---------Notifying readers------------------------------------------

  // update variable so readers can find the latest transfer

  // wake up readers
  for (idx = 0; idx < MAX_USERS; idx++) {
    atomic_set(&adev->users_locks[idx].user_lock, 1);
  }

  //---------------------------------------------------

  // increment our irq counter
  atomic_inc(&adev->irq_count_atomic);

  // spin_unlock(&adev->lock);

  xdmaWrite(adev->xdev, IRQ_CHANNEL_INT_ENMASK, 0x2);
  return IRQ_HANDLED;
}

/*! \fn  irqreturn_t xdma_user_irq(int irq, void *dev_id)
    \brief This is the IRQ handler for this device user interrupt
    \param irq the irq number
    \param dev_id the device id for this device which always is a APEC struct
    \returns 0
*/
static irqreturn_t xdma_user_irq(int irq, void *dev_id) {
  APEC *adev = NULL;
  u32 irq_req;
  u32 irq_pend;
  u32 i;
  u32 j;
  u32 virtual_interrupt;
  u32 reset;
  u32 val;
  adev = (APEC *)dev_id;
  reset = 0;
  irq_req = xdmaRead(adev->xdev, IRQ_USER_INT_REQ);
  irq_pend = xdmaRead(adev->xdev, IRQ_USER_INT_PEND);
  xdmaWrite(adev->xdev, IRQ_USER_INT_ENMASK, 0);
  for (i = 0; i < channels; i++) {
    if ((1 << i & irq_req) && (1 << i & irq_pend)) {
      // 1024 + stream * 128 + static_cast<uint32_t>(vector) * 16 + interrupt
      // vector misc(3) in the firmware, tells us which virtual interrupt was
      // last triggered
      virtual_interrupt = read_user_bar(adev->xdev, 4292 + i * 512);
      for (j = 0; j < MAX_USERS; j++) {
        // if the lock is set to -1 then we set it to 0
        atomic_cmpxchg(&adev->users_interrupt_locks[i][j].user_lock, -1, 0);
      }
      atomic_set(&(adev->virtual_user_interrupt[i]), virtual_interrupt);
      wake_up_interruptible_all(&(adev->user_interrupt_queues[i]));
      // reset interrupt
      reset += 1 << i;
    }
  }
  write_user_bar(adev->xdev, 584, reset);
  if (reset != 0) {
    schedule_work(&adev->after_user_interrupt);
  } else {
    val = 0;
    for (i = 0; i < channels; i++) {
      val += 1 << i;
    }
    xdmaWrite(adev->xdev, IRQ_USER_INT_ENMASK, val);
  }
  return IRQ_HANDLED;
}

/* engine_service_work */
static void after_user_interrupt(struct work_struct *after_user_interrupt) {
  u32 val;
  u32 i;
  u32 j;
  APEC *adev = NULL;
  adev = container_of(after_user_interrupt, APEC, after_user_interrupt);
  val = 0;
  for (i = 0; i < channels; i++) {
    val += 1 << i;
    for (j = 0; j < MAX_USERS; j++) {
      while (atomic_read(&adev->users_interrupt_locks[i][j].user_lock) == 0) {
        wake_up_interruptible_all(&(adev->user_interrupt_queues[i]));
      }
    }
  }
  xdmaWrite(adev->xdev, IRQ_USER_INT_ENMASK, val);
  write_user_bar(adev->xdev, 584, 0);
}

/*! \fn  void irq_msix_channel_teardown(APEC* adev)
    \brief Removes the MSI-X irqs for this device
    \param adev the APEC struct for this device
*/
static void irq_msix_channel_teardown(APEC *adev) {
  if (adev->msix_setup == 0) {
    dbg("MSI-X not setup in irq_msix_channel_teardown\n");
    return;
  }
  xdmaWrite(adev->xdev, IRQ_VECTOR1, 0x00);
  xdmaWrite(adev->xdev, IRQ_VECTOR0, 0x00);
  free_irq(adev->msix_irq_line, adev);
  free_irq(adev->msix_irq_line_h2c, adev);
  adev->msix_setup = 0;
}

/*! \fn  void irq_msix_user_teardown(APEC* adev)
    \brief Removes the MSI-X irqs for this device
    \param adev the APEC struct for this device
*/
static void irq_msix_user_teardown(APEC *adev) {
  int i;
  if (adev->msix_setup_user == 0) {
    dbg("MSI-X not setup in irq_msix_user_teardown\n");
    return;
  }
  xdmaWrite(adev->xdev, IRQ_VECTOR_0, 0x00);
  xdmaWrite(adev->xdev, IRQ_VECTOR_1, 0x00);
  xdmaWrite(adev->xdev, IRQ_VECTOR_2, 0x00);
  xdmaWrite(adev->xdev, IRQ_VECTOR_3, 0x00);
  xdmaWrite(adev->xdev, IRQ_VECTOR_4, 0x00);
  xdmaWrite(adev->xdev, IRQ_VECTOR_5, 0x00);
  xdmaWrite(adev->xdev, IRQ_VECTOR_6, 0x00);
  xdmaWrite(adev->xdev, IRQ_VECTOR_7, 0x00);
  xdmaWrite(adev->xdev, IRQ_VECTOR_8, 0x00);
  xdmaWrite(adev->xdev, IRQ_VECTOR_9, 0x00);
  for (i = 0; i < 10; i++) {
    free_irq(adev->msix_irq_line_user[i], adev);
  }
  adev->msix_setup_user = 0;
}

/*! \fn  int irq_msix_channel_setup(APEC* adev)
    \brief Setup the MSI-X IRQ for C2H channel 0
    \param adev the APEC struct for this device
    \returns 0 if success and a negative value if fail
*/
static int irq_msix_channel_setup(APEC *adev) {

  u32 vector;
  XDMA *xdev = NULL;
  int rv = 0;

  BUG_ON(!adev);
  xdev = adev->xdev;
  BUG_ON(!xdev);
  BUG_ON(!(xdev->pdev));

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4, 12, 0)
  vector = pci_irq_vector(xdev->pdev, 0);
#else
  vector = xdev->entry[0].vector;
#endif
  rv = request_irq(vector, xdma_channel_irq, 0, DRV_MODULE_NAME, adev);
  if (rv) {
    dbg("requesti irq#%d failed %d.\n", vector, rv);
    return rv;
  }
  adev->msix_irq_line_h2c = vector;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4, 12, 0)
  vector = pci_irq_vector(xdev->pdev, 1);
#else
  vector = xdev->entry[1].vector;
#endif
  rv = request_irq(vector, xdma_channel_irq, 0, DRV_MODULE_NAME, adev);
  if (rv) {
    dbg("requesti irq#%d failed %d.\n", vector, rv);
    return rv;
  }
  adev->msix_irq_line = vector;

  // enable irq channel vector 1 which is the C2H, vector0 is H2C
  xdmaWrite(adev->xdev, IRQ_VECTOR1, 0x1);
  xdmaWrite(adev->xdev, IRQ_VECTOR0, 0x0);
  adev->msix_setup = 1;
  return 0;
}

/*! \fn  int irq_msix_user_setup(APEC* adev)
    \brief Setup the MSI-X IRQ for the user interrupts
    \param adev the APEC struct for this device
    \returns 0 if success and a negative value if fail
*/
static int irq_msix_user_setup(APEC *adev) {

  u32 vector;
  XDMA *xdev = NULL;
  int i = 0;
  int rv = 0;

  BUG_ON(!adev);
  xdev = adev->xdev;
  BUG_ON(!xdev);
  BUG_ON(!(xdev->pdev));
  for (i = 2; i < 12; i++) {
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4, 12, 0)
    vector = pci_irq_vector(xdev->pdev, i);
#else
    vector = xdev->entry[i].vector;
#endif
    rv = request_irq(vector, xdma_user_irq, 0, DRV_MODULE_NAME, adev);
    if (rv) {
      dbg("requesti irq#%d failed %d.\n", vector, rv);
      return rv;
    }
    adev->msix_irq_line_user[i - 2] = vector;
  }

  // enable irq channel vector 1 which is the C2H, vector0 is H2C
  xdmaWrite(adev->xdev, IRQ_VECTOR_0, 0x2);
  xdmaWrite(adev->xdev, IRQ_VECTOR_1, 0x3);
  xdmaWrite(adev->xdev, IRQ_VECTOR_2, 0x4);
  xdmaWrite(adev->xdev, IRQ_VECTOR_3, 0x5);
  xdmaWrite(adev->xdev, IRQ_VECTOR_4, 0x6);
  xdmaWrite(adev->xdev, IRQ_VECTOR_5, 0x7);
  xdmaWrite(adev->xdev, IRQ_VECTOR_6, 0x8);
  xdmaWrite(adev->xdev, IRQ_VECTOR_7, 0x9);
  xdmaWrite(adev->xdev, IRQ_VECTOR_8, 0xa);
  xdmaWrite(adev->xdev, IRQ_VECTOR_9, 0xb);
  adev->msix_setup_user = 1;
  return 0;
}

void destroy_descriptor_chain(APEC *adev) {
  u64 deleteCounter = 0;
  TRANSFER *current_trans = NULL;
  BUG_ON(!adev);
  BUG_ON(!(adev->xdev));
  BUG_ON(!(adev->xdev->pdev));

  if (adev->buffer_transfers != NULL) {
    for (deleteCounter = 0; deleteCounter < transfers; deleteCounter++) {
      current_trans = adev->buffer_transfers + deleteCounter * sizeof(TRANSFER);
      if (current_trans->memory != NULL) {
        dma_free_coherent(&adev->xdev->pdev->dev,
                          transferSizePerChannel * channels,
                          current_trans->memory, current_trans->handle);
      }
    }
    vfree(adev->buffer_transfers);
    adev->buffer_transfers = NULL;
  }
}

static int create_descriptor_chain(APEC *adev) {
  dma_addr_t tmp_dma_handle;
  TRANSFER *first_trans = NULL;
  TRANSFER *current_trans = NULL;
  TRANSFER *previous_trans = NULL;
  // u64 number_transfers = 0;
  u64 descriptorCounter = 0;
  u32 i;
  u32 addr = 0;

  adev->buffer_transfers = vmalloc(sizeof(TRANSFER) * transfers);
  if (!adev->buffer_transfers) {
    dbg("Allocation of adev->buffer_transfers failed in "
        "create_descriptor_chain\n");
    return -1;
  }

  adev->buffer_writeback = dma_alloc_coherent(
      &adev->xdev->pdev->dev, 4096, &adev->dma_handle_writeback, GFP_KERNEL);
  if (!adev->buffer_writeback) {
    dbg("Allocation of writeback buffer failed\n");
    vfree(adev->buffer_transfers);
    adev->buffer_transfers = NULL;
    return -1;
  }

  for (descriptorCounter = 0; descriptorCounter < transfers;
       descriptorCounter++) {
    current_trans =
        adev->buffer_transfers + descriptorCounter * sizeof(TRANSFER);
    current_trans->memory = NULL;
    current_trans->idx = descriptorCounter;

    current_trans->memory = dma_alloc_coherent(
        &adev->xdev->pdev->dev, transferSizePerChannel * channels,
        &tmp_dma_handle, GFP_HIGHUSER);
    if (!current_trans->memory) {
      dbg("Allocation of transfer buffer failed for descriptorCounter %llu \n",
          descriptorCounter);
      // TODO free all previous allocated dma buffers
      return -1;
    }

    current_trans->offset =
        transferSizePerChannel * channels * descriptorCounter;
    if (dma_mapping_error(&adev->xdev->pdev->dev, tmp_dma_handle)) {
      dbg("Dma_map_single failed in create_descriptor_chain\n");
      return -1;
    }

    current_trans->handle = tmp_dma_handle;
    current_trans->dst_addr_lo = cpu_to_le32(PCI_DMA_L(current_trans->handle));
    current_trans->dst_addr_hi = cpu_to_le32(PCI_DMA_H(current_trans->handle));
    dma_sync_single_for_device(&adev->xdev->pdev->dev, current_trans->handle,
                               (transferSizePerChannel * channels),
                               DMA_FROM_DEVICE);
    if (first_trans != NULL) {
      previous_trans->next = current_trans;
      current_trans->previous = previous_trans;
    } else {
      first_trans = current_trans;
    }
    previous_trans = current_trans;
  }
  current_trans->next = first_trans;
  first_trans->previous = current_trans;
  adev->current_transfer = first_trans;
  current_trans = first_trans;
  adev->descriptors = transfers;
  for (i = 0; i < 511; i++) {
    apec_write_descriptor_to_bypass_bar(adev, current_trans, i);
    current_trans = current_trans->next;
  }
  // the first transfer to be loaded into the bypassbar from the irq routine
  adev->transfer = current_trans;
  // polling writeback
  addr = cpu_to_le32(PCI_DMA_H(adev->dma_handle_writeback));
  xdmaWriteChannel(adev->xdev, C2H_CHANNEL_POLLMODE_HI_WB_ADDR, addr, 0);
  addr = cpu_to_le32(PCI_DMA_L(adev->dma_handle_writeback));
  xdmaWriteChannel(adev->xdev, C2H_CHANNEL_POLLMODE_LO_WB_ADDR, addr, 0);
  dbg_v("Created descriptor chain for device: bus: %02u device: %02u  with "
        "%llu transfers \n",
        adev->bus_nr, adev->dev_nr, descriptorCounter);
  return 0;
}

static void stop_engine(APEC *adev) {
  XDMA *xdev = NULL;
  BUG_ON(!adev);
  BUG_ON(!(adev->xdev));
  xdev = adev->xdev;
  // stop interrupts
  xdmaWrite(xdev, IRQ_CHANNEL_INT_ENMASK, 0x0);
  xdmaWriteChannel(xdev, C2H_CHANNEL_CONTROL_RUN, 0, 0);
}

static void set_all_register(APEC *adev) {
  XDMA *xdev = NULL;
  u32 val;
  u32 i;
  BUG_ON(!adev);
  BUG_ON(!(adev->xdev));
  xdev = adev->xdev;

  // u32 xdmaWriteChannel(struct XDMA* dev,const XDMA_REGISTER item, u32
  // value,u8 channel)

  // metadata writeback
  xdmaWriteChannel(xdev, C2H_CHANNEL_CONTROL_WRITEBACK, 0, 0);
  // writeback in poll mode
  xdmaWriteChannel(xdev, C2H_CHANNEL_CONTROL_POLLMODE_WB_ENABLE, 0x1, 0);
  // does not apply to this application
  xdmaWriteChannel(xdev, C2H_CHANNEL_CONTROL_NON_INC_MODE, 0, 0);
  // logging on descriptor completion
  xdmaWriteChannel(xdev, C2H_CHANNEL_CONTROL_IE_DESCRIPTOR_COMPLETE, 0x1, 0);
  // interruptmask descriptor completed
  if (!polling) {
    xdmaWriteChannel(xdev, C2H_CHANNEL_IM_DESCRIPTOR_COMPLETED, 0x1, 0);
  }

  // no performance counters
  xdmaWriteChannel(xdev, C2H_CHANNEL_PERFORMANCE_MONITOR_CONTROL_RUN, 0, 0);
  // enable user interrupt for all enabled channels
  val = 0;
  for (i = 0; i < channels; i++) {
    val += 1 << i;
  }
  xdmaWrite(xdev, IRQ_USER_INT_ENMASK, val);
  // We only enable C2H0 and ignore H2
  if (!polling) {
    xdmaWrite(xdev, IRQ_CHANNEL_INT_ENMASK, 0x2);
  }
  // the MSI-X vector is set in irq_msix_channel_setup

  // start the engine
  xdmaWriteChannel(xdev, C2H_CHANNEL_CONTROL_RUN, 0x1, 0);
}

static int status_fpga_open(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  CDEV_APEC *cdev;
  cdev = container_of(inode->i_cdev, CDEV_APEC, c_dev);
  adev = cdev->adev;
  f->private_data = adev;
  dbg_v("status_fpga_open\n");
  return 0;
}

// called when a process closes a stream file
static int status_fpga_close(struct inode *inode, struct file *f) {
  dbg_v("status_fpga_close \n");
  return 0;
}

// called when a process read from a stream file
static ssize_t status_fpga_read(struct file *f, char __user *buf, size_t len,
                                loff_t *off) {
  APEC *adev = NULL;
  int size = 0;
  char *temp;
  u32 to_copy = 0;
  u32 databufferstatus;
  u32 resetstatus;
  u32 moddetrxlosstatus;
  u32 measuredTemp;
  u32 measuredVCCAUX;
  u32 measuredVCCINT;
  u32 measuredVCCBRAM;
  u32 alarm;
  u32 firmware_version;
  u32 idx;

  u32 offset = 0;

  u32 writeback;

  adev = f->private_data;

  temp = (char *)vmalloc(8192);

  databufferstatus = read_user_bar(adev->xdev, 20 * 4);
  resetstatus = read_user_bar(adev->xdev, 22 * 4);
  moddetrxlosstatus = read_user_bar(adev->xdev, 23 * 4);
  // all values are times 100000
  measuredTemp = read_user_bar(adev->xdev, 24 * 4);
  measuredVCCAUX = read_user_bar(adev->xdev, 25 * 4);
  measuredVCCINT = read_user_bar(adev->xdev, 26 * 4);
  measuredVCCBRAM = read_user_bar(adev->xdev, 26 * 4);
  alarm = read_user_bar(adev->xdev, 32 * 4);
  firmware_version = read_user_bar(adev->xdev, 145 * 4);
  idx = adev->xdev->idx;

  writeback = *(u32 *)adev->buffer_writeback;

  offset += snprintf(
      temp + offset, 8192 - offset,
      "descriptor_error : %02u\nread_error : %02u\nidle_stopped : "
      "%02u\ninvalid_length : %02u\nmagic_stopped : %02u\nalign_mismatch : "
      "%02u\ndescriptor_complete : %02u\ndescriptor_stopped : %02u\nbusy : "
      "%02u\ncompleted_descriptor : %02u\ninterrupt_pending : "
      "%02u\ninterrupt_enable_mask : %02u\ninterrupt_request : "
      "%02u\ntransfer_idx : %02u\nmissed_turns : "
      "%02u\ndescriptor_match_error_counter : "
      "%02u\ndescriptor_counter_rollover : %02u\nirq_counter : "
      "%02u\ndescriptor_writeback : %02u\n",
      xdmaReadChannel(adev->xdev, C2H_CHANNEL_STATUS_DESCR_ERROR, 0),
      xdmaReadChannel(adev->xdev, C2H_CHANNEL_STATUS_READ_ERROR, 0),
      xdmaReadChannel(adev->xdev, C2H_CHANNEL_STATUS_IDLE_STOPPED, 0),
      xdmaReadChannel(adev->xdev, C2H_CHANNEL_STATUS_INVALID_LENGTH, 0),
      xdmaReadChannel(adev->xdev, C2H_CHANNEL_STATUS_MAGIC_STOPPED, 0),
      xdmaReadChannel(adev->xdev, C2H_CHANNEL_STATUS_ALIGN_MISMATCH, 0),
      xdmaReadChannel(adev->xdev, C2H_CHANNEL_STATUS_DESCRIPTOR_COMPLETE, 0),
      xdmaReadChannel(adev->xdev, C2H_CHANNEL_STATUS_DESCRIPTOR_STOPPED, 0),
      xdmaReadChannel(adev->xdev, C2H_CHANNEL_STATUS_BUSY, 0),
      xdmaReadChannel(adev->xdev, C2H_CHANNEL_COMPLETED_DESCRIPTOR_COUNT, 0),
      xdmaRead(adev->xdev, IRQ_ENGINE_INT_PEND) & 0x02,
      xdmaRead(adev->xdev, IRQ_CHANNEL_INT_ENMASK) & 0x02,
      xdmaRead(adev->xdev, IRQ_ENGINE_INT_REQ) & 0x02,
      atomic_read(&adev->transfer_idx_atomic), atomic_read(&adev->missed_turns),
      atomic_read(&adev->descriptor_match_errors_atomic),
      atomic_read(&adev->descriptor_counter_rollover_atomic),
      atomic_read(&adev->irq_count_atomic), writeback);

  offset += snprintf(
      temp + offset, 8192 - offset,
      "gt_wizard_reset : %c\ndatabufferwriter_reset : "
      "%c\ndatabufferreader_reset : %c\nxadc-reset : %c\nxdma_busy : "
      "%c\nxdma_descriptor_completed : %c\nxdma_descriptor_stop : "
      "%c\nxdma_descriptor_done_event : %c\nxdma_packet_done_event : "
      "%c\nxdma_irq_event_pending : %c\nxdma_run : "
      "%c\ndatabuffer_not_idle_when_reset : %c\nswap_timeout : "
      "%c\nstart_frame_incorrect_address : %c\nreading_status : %c%c%c\ntemp : "
      "%09u\nvcc_aux : %09u\nvcc_int : %09u\nvcc_bram : %09u\nalarm_temp : "
      "%c\nalarm_vccint : %c\nalarm_vccaux : %c\nalarm_vccbram : "
      "%c\nalarm_vccpint : %c\nalarm_vccpaux : %c\nalarm_vccoddr : "
      "%c\nalarm_or : %c\nfpga_firmware : 20%02u%02u%02u\ncpld_firmware : "
      "20%02u%02u00\ncard_idx : %09u\n",
      (resetstatus & 0x00000400 ? '1' : '0'),
      (resetstatus & 0x00000800 ? '1' : '0'),
      (resetstatus & 0x00001000 ? '1' : '0'),
      (resetstatus & 0x00004000 ? '1' : '0'),
      (databufferstatus & 0x00020000 ? '1' : '0'), // XDMA busy
      (databufferstatus & 0x00040000 ? '1' : '0'), // desc completed
      (databufferstatus & 0x00080000 ? '1' : '0'), // desc stop
      (databufferstatus & 0x00100000 ? '1' : '0'), // desc done
      (databufferstatus & 0x00200000 ? '1' : '0'), // packet done
      (databufferstatus & 0x00400000 ? '1' : '0'), // irq event pending
      (databufferstatus & 0x00800000 ? '1' : '0'), // run
      (databufferstatus & 0x00000400 ? '1'
                                     : '0'), // databuffer-not-idle-when-reset
      (databufferstatus & 0x00000800 ? '1' : '0'), // swap-timeout
      (databufferstatus & 0x00001000 ? '1'
                                     : '0'), // start-frame-incorrect-address
      (databufferstatus & 0x00010000 ? '1' : '0'), // reading status
      (databufferstatus & 0x00008000 ? '1' : '0'), // reading status
      (databufferstatus & 0x00004000 ? '1' : '0'), // reading status
      measuredTemp, measuredVCCAUX, measuredVCCINT, measuredVCCBRAM,
      (alarm & 0x00000001 ? '1' : '0'), (alarm & 0x00000002 ? '1' : '0'),
      (alarm & 0x00000004 ? '1' : '0'), (alarm & 0x00000008 ? '1' : '0'),
      (alarm & 0x00000010 ? '1' : '0'), (alarm & 0x00000020 ? '1' : '0'),
      (alarm & 0x00000040 ? '1' : '0'), (alarm & 0x00000080 ? '1' : '0'),
      firmware_version >> 10 & 0x3f, firmware_version >> 6 & 0xf,
      firmware_version & 0x3f, firmware_version >> 26 & 0x3f,
      firmware_version >> 22 & 0xf, idx);

  // size of output
  size = strlen(temp);
  // we have printed everything
  if (*off >= size) {
    vfree(temp);
    return 0;
  }
  // we can fit everything into the buffer
  if ((size - (*off)) <= len) {
    to_copy = size - (*off);
    if (copy_to_user(buf, temp + (*off), to_copy)) {
      dbg("copy_to_user in status_fpga_read failed\n");
    }
    *off = size;
    vfree(temp);
    return to_copy;
  }
  // we must copy a part
  else {
    if (copy_to_user(buf, temp + (*off), len)) {
      dbg("copy_to_user in status_fpga_read failed\n");
    }
    *off += len;
    vfree(temp);
    return len;
  }
}

// the different operations for the statusExpert file
static struct file_operations status_fpga_fops = {
    .owner = THIS_MODULE,
    .open = status_fpga_open,
    .release = status_fpga_close,
    .read = status_fpga_read,
};

int mmapdrv_mmap(struct file *file, struct vm_area_struct *vma) {
  u64 descriptorCounter = 0;
  TRANSFER *current_trans = NULL;

  unsigned long offset = vma->vm_pgoff << PAGE_SHIFT;
  unsigned long size = vma->vm_end - vma->vm_start;
  APEC *adev = NULL;
  int rv = 0;

  adev = file->private_data;
  if (!adev) {
    dbg("adev is NULL in mmapdrv_mmap\n");
    return -ENXIO;
  }
  if (offset & ~PAGE_MASK) {
    dbg("offset not aligned: %ld\n", offset);
    return -ENXIO;
  }

  BUG_ON(size != transferSizePerChannel * channels * transfers);
  // removed VM_EXEC flag check because on some architecture VM_READ implies
  // VM_EXEC
  if ((vma->vm_flags & VM_WRITE) || (vma->vm_flags & VM_SHARED) ||
      !(vma->vm_flags & VM_READ)) {
    dbg("Only allowed to read\n");
    if ((vma->vm_flags & VM_WRITE)) {
      dbg(" trying to open with VM_WRITE\n");
    }
    if ((vma->vm_flags & VM_SHARED)) {
      dbg(" trying to open with VM_SHARED\n");
    }
    if (!(vma->vm_flags & VM_READ)) {
      dbg(" trying to open without VM_READ\n");
    }
    return (-EINVAL);
  }
  // no swap
  vma->vm_flags |= VM_LOCKED;
  vma->vm_flags &= ~VM_MAYWRITE;
  dbg_v("mapping with vm_flags %lu and page_prot %lu \n",vma->vm_flags,vma->vm_page_prot.pgprot);
  for (descriptorCounter = 0; descriptorCounter < transfers;
       descriptorCounter++) {
    current_trans =
        adev->buffer_transfers + descriptorCounter * sizeof(TRANSFER);
    rv = remap_pfn_range(
        vma,
        vma->vm_start + descriptorCounter * transferSizePerChannel * channels,
        virt_to_phys((void *)current_trans->memory) >> PAGE_SHIFT,
        transferSizePerChannel * channels, vma->vm_page_prot);
    if (rv) {
      dbg("remap page range failed for transfer %llu \n", descriptorCounter);
      return -ENXIO;
    }
  }
  return 0;
}

int mmapdrv_open(struct inode *inode, struct file *file) {
  APEC *adev = NULL;
  CDEV_APEC *cdev;
  cdev = container_of(inode->i_cdev, CDEV_APEC, c_dev);
  adev = cdev->adev;
  file->private_data = adev;
  return (0);
}

/* device close method */
int mmapdrv_release(struct inode *inode, struct file *file) { return (0); }

// file operations for the mmaped buffer
static struct file_operations mmapdrv = {
  owner : THIS_MODULE,
  mmap : mmapdrv_mmap,
  open : mmapdrv_open,
  release : mmapdrv_release,
};

static int buffer_size_open(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  CDEV_APEC *cdev;
  cdev = container_of(inode->i_cdev, CDEV_APEC, c_dev);
  adev = cdev->adev;
  f->private_data = adev;
  dbg_v("buffer_size_open\n");
  return 0;
}

// called when a process closes a stream file
static int buffer_size_close(struct inode *inode, struct file *f) {
  dbg_v("buffer_size_close \n");
  return 0;
}

// called when a process read from a stream file
static ssize_t buffer_size_read(struct file *f, char __user *buf, size_t len,
                                loff_t *off) {
  APEC *adev = NULL;
  int size = 0;
  u64 toReturn = 0;
  char temp[32];
  u32 to_copy = 0;
  adev = f->private_data;

  toReturn = transferSizePerChannel * channels * transfers;

  // our output
  snprintf(temp, sizeof(temp), "%llu\n", toReturn);
  // size of output
  size = strlen(temp);
  // we have printed everything
  if (*off >= size) {
    return 0;
  }
  // we can fit everything into the buffer
  if ((size - (*off)) <= len) {
    to_copy = size - (*off);
    if (copy_to_user(buf, temp + (*off), to_copy)) {
      dbg("copy_to_user in buffer_size_read failed\n");
    }
    *off = size;
    return to_copy;
  }
  // we must copy a part
  else {
    if (copy_to_user(buf, temp + (*off), len)) {
      dbg("copy_to_user in buffer_size_read failed\n");
    }
    *off += len;
    return len;
  }
}

// the different operations for the statusExpert file
static struct file_operations buffer_size_fops = {
    .owner = THIS_MODULE,
    .open = buffer_size_open,
    .release = buffer_size_close,
    .read = buffer_size_read,
};

static int channel_size_open(struct inode *inode, struct file *f) {
  dbg_v("channel_size_open\n");
  return 0;
}

// called when a process closes a stream file
static int channel_size_close(struct inode *inode, struct file *f) {
  dbg_v("channel_size_close \n");
  return 0;
}

// called when a process read from a stream file
static ssize_t channel_size_read(struct file *f, char __user *buf, size_t len,
                                 loff_t *off) {
  int size = 0;
  u32 toReturn = transferSizePerChannel;
  char temp[32];
  u32 to_copy = 0;

  // our output
  snprintf(temp, sizeof(temp), "%u\n", toReturn);
  // size of output
  size = strlen(temp);
  // we have printed everything
  if (*off >= size) {
    return 0;
  }
  // we can fit everything into the buffer
  if ((size - (*off)) <= len) {
    to_copy = size - (*off);
    if (copy_to_user(buf, temp + (*off), to_copy)) {
      dbg("copy_to_user in buffer_size_read failed\n");
    }
    *off = size;
    return to_copy;
  }
  // we must copy a part
  else {
    if (copy_to_user(buf, temp + (*off), len)) {
      dbg("copy_to_user in buffer_size_read failed\n");
    }
    *off += len;
    return len;
  }
}

// the different operations for the statusExpert file
static struct file_operations channel_size_fops = {
    .owner = THIS_MODULE,
    .open = channel_size_open,
    .release = channel_size_close,
    .read = channel_size_read,
};

static int bypass_dump_open(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  CDEV_APEC *cdev;
  cdev = container_of(inode->i_cdev, CDEV_APEC, c_dev);
  adev = cdev->adev;
  f->private_data = adev;
  dbg_v("bypass_dump_open\n");
  return 0;
}

// called when a process closes a stream file
static int bypass_dump_close(struct inode *inode, struct file *f) {
  dbg_v("bypass_dump_close \n");
  return 0;
}

// called when a process read from a stream file
static ssize_t bypass_dump_read(struct file *f, char __user *buf, size_t len,
                                loff_t *off) {
  APEC *adev = NULL;
  char temp[32];
  adev = f->private_data;

  // make sure we have a null string
  strcpy(temp, "");
  // dbg("offset is %llu \n",*off);
  if (*off / 16 <= 510) {
    snprintf(temp, sizeof(temp), "%03lli:%#018llx\n", *off / 16,
             apec_bypass_read(adev, *off / 16));
    if (copy_to_user(buf, temp, 32)) {
      dbg("copy_to_user in bypass_dump_read failed\n");
    }
    *off = *off + 16;
    return strlen(temp);
  } else if (*off / 16 == 511) {
    snprintf(temp, sizeof(temp), "FPG:%08llu\n",
             apec_bypass_read(adev, *off / 16) / 8);
    if (copy_to_user(buf, temp, 16)) {
      dbg("copy_to_user in bypass_dump_read failed\n");
    }
    *off = *off + 16;
    return strlen(temp);
  } else {
    return 0;
  }
}

static int unstuck_open(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  CDEV_APEC *cdev;
  cdev = container_of(inode->i_cdev, CDEV_APEC, c_dev);
  adev = cdev->adev;
  f->private_data = adev;
  dbg_v("unstuck_open\n");
  return 0;
}

// called when a process closes a stream file
static int unstuck_close(struct inode *inode, struct file *f) {
  dbg_v("unstuck_close \n");
  return 0;
}

// called when a process read from a stream file
static ssize_t unstuck_read(struct file *f, char __user *buf, size_t len,
                            loff_t *off) {
  return 0;
}
static ssize_t unstuck_write(struct file *f, const char __user *buf, size_t len,
                             loff_t *off) {
  APEC *adev = NULL;
  u32 idx;
  adev = f->private_data;

  for (idx = 0; idx < MAX_USERS; idx++) {
    atomic_set(&adev->users_locks[idx].user_lock, 1);
  }
  return len;
}

// the different operations for the statusExpert file
static struct file_operations unstuck_fops = {
    .owner = THIS_MODULE,
    .open = unstuck_open,
    .release = unstuck_close,
    .read = unstuck_read,
    .write = unstuck_write,
};

// the different operations for the statusExpert file
static struct file_operations bypass_dump_fops = {
    .owner = THIS_MODULE,
    .open = bypass_dump_open,
    .release = bypass_dump_close,
    .read = bypass_dump_read,
};

static int reset_open(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  CDEV_APEC *cdev;
  cdev = container_of(inode->i_cdev, CDEV_APEC, c_dev);
  adev = cdev->adev;
  f->private_data = adev;
  dbg_v("reset_open\n");
  return 0;
}

// called when a process closes a stream file
static int reset_close(struct inode *inode, struct file *f) {
  dbg_v("reset_close \n");
  return 0;
}

// called when a process read from a stream file
static ssize_t reset_read(struct file *f, char __user *buf, size_t len,
                          loff_t *off) {
  return 0;
}
static ssize_t reset_write(struct file *f, const char __user *buf, size_t len,
                           loff_t *off) {
  APEC *adev = NULL;
  // u32 idx;
  adev = f->private_data;
  atomic_set(&adev->reset, 1);
  return len;
}

// the different operations for the statusExpert file
static struct file_operations reset_fops = {
    .owner = THIS_MODULE,
    .open = reset_open,
    .release = reset_close,
    .read = reset_read,
    .write = reset_write,
};

static int desc_dump_open(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  CDEV_APEC *cdev;
  cdev = container_of(inode->i_cdev, CDEV_APEC, c_dev);
  adev = cdev->adev;
  f->private_data = adev;
  dbg_v("desc_dump_open\n");
  return 0;
}

// called when a process closes a stream file
static int desc_dump_close(struct inode *inode, struct file *f) {
  dbg_v("desc_dump_close \n");
  return 0;
}

// called when a process read from a stream file
static ssize_t desc_dump_read(struct file *f, char __user *buf, size_t len,
                              loff_t *off) {
  APEC *adev = NULL;
  char temp[256];
  TRANSFER *current_trans;
  u64 addr;
  // u32 counter;
  adev = f->private_data;

  // make sure we have a null string
  strcpy(temp, "");
  if (*off / 16 < adev->descriptors) {
    current_trans = adev->buffer_transfers + (*off / 16) * sizeof(TRANSFER);
    addr = current_trans->dst_addr_hi;
    addr = addr << 32;
    addr += current_trans->dst_addr_lo;
    snprintf(temp, sizeof(temp),
             "%03i offset: %#018llx handle: %#018llx addr: %#018llx \n",
             current_trans->idx, current_trans->offset, current_trans->handle,
             addr);
    if (copy_to_user(buf, temp, 256)) {
      dbg("copy_to_user in bypass_dump_read failed\n");
    }
    *off = *off + 16;
    return strlen(temp);
  } else {
    return 0;
  }
}

// the different operations for the statusExpert file
static struct file_operations desc_dump_fops = {
    .owner = THIS_MODULE,
    .open = desc_dump_open,
    .release = desc_dump_close,
    .read = desc_dump_read,
};

static int apecStream_open(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  CDEV_APEC *cdev;
  STREAM_USER_CONTAINER *ucont;
  u32 dev_minor = 0;
  u32 stream;
  unsigned long idx;

  cdev = container_of(inode->i_cdev, CDEV_APEC, c_dev);
  adev = cdev->adev;

  // get our atomic lock
  spin_lock(&adev->users_setup_lock);

  idx = bitmap_find_next_zero_area(adev->users_bitmap, MAX_USERS, 0, 1, 0);
  if (idx >= MAX_USERS) {
    dbg("all slots are taken by users\n");
    spin_unlock(&adev->users_setup_lock);
    return -1;
  }
  bitmap_set(adev->users_bitmap, idx, 1);
  dbg_v("got idx %lu \n", idx);
  spin_unlock(&adev->users_setup_lock);

  // allocate one struct per user
  ucont = (STREAM_USER_CONTAINER *)kzalloc(sizeof(STREAM_USER_CONTAINER),
                                           GFP_KERNEL);
  ucont->buffer = NULL;
  if (!ucont) {
    dbg("could not allocate a stream_user_containers\n");
    return -1;
  }
  ucont->adev = adev;
  // default is latest turn
  ucont->offset = 0;
  ucont->turns = 1;
  ucont->idx = idx;
  ucont->timeout = jiffies + HZ; // resched every second
  f->private_data = ucont;

  ucont->buffer = (u64 *)vmalloc(ucont->turns * sizeof(u64));
  if (!ucont->buffer) {
    dbg("could not allocate a stream_user_container buffer\n");
    return -1;
  }

  // check which stream we are opening
  dev_minor = MINOR(inode->i_rdev);
  for (stream = 0; stream < 10; stream++) {
    if (adev->streamMinorNumbers[stream] == dev_minor) {
      ucont->stream = stream;
      break;
    }
  }

  dbg_v("stream_open\n");
  return 0;
}

// called when a process closes a stream file
static int apecStream_close(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  STREAM_USER_CONTAINER *ucont = NULL;
  ucont = f->private_data;
  adev = ucont->adev;
  if (ucont->buffer != NULL) {
    vfree(ucont->buffer);
    ucont->buffer = NULL;
  }

  dbg_v("stream_close \n");

  spin_lock(&adev->users_setup_lock);
  bitmap_clear(adev->users_bitmap, ucont->idx, 1);
  dbg_v("released idx %lu \n", ucont->idx);
  spin_unlock(&adev->users_setup_lock);

  if (f->private_data != NULL) {
    kfree(f->private_data);
    f->private_data = NULL;
  }
  return 0;
}

// called when a process read from a stream file
static ssize_t apecStream_read(struct file *f, char __user *buf, size_t len,
                               loff_t *off) {
  APEC *adev = NULL;
  STREAM_USER_CONTAINER *ucont = NULL;
  s64 delayCounter = 0;
  u32 i;
  struct TRANSFER *transfer;
  u32 idx;
  ucont = f->private_data;
  adev = ucont->adev;
  if (len < ucont->turns * sizeof(u64)) {
    dbg("buffer is not big enough for offsets\n");
    return 0;
  }
  // positive offset
  i = 0;
  if (ucont->offset >= 0) {
    ucont->timeout = jiffies + HZ; // resched every second
    while (delayCounter <= ucont->offset) {
      delayCounter += 1;
      // wait for interrupt handler to signal us
      atomic_set(&adev->users_locks[ucont->idx].user_lock, 0);
      while (atomic_read(&adev->users_locks[ucont->idx].user_lock) == 0) {
        if (time_after(jiffies, ucont->timeout)) {
          cond_resched();
          ucont->timeout = jiffies + HZ; // resched every second
        }
      }
    }
  }

  idx = atomic_read(&adev->transfer_idx_atomic);
  transfer = adev->buffer_transfers + idx * sizeof(TRANSFER);

  // negative offset
  for (delayCounter = ucont->offset; delayCounter < 0; delayCounter++) {
    transfer = transfer->previous;
  }

  // for every turn
  for (i = 0; i < ucont->turns; i++) {
    *(ucont->buffer + ucont->turns - 1 - i) =
        transfer->offset + transferSizePerChannel * ucont->stream;
    transfer = transfer->previous;
  }
  if (copy_to_user(buf, ucont->buffer, sizeof(u64) * ucont->turns)) {
    dbg("copy_to_user in stream_read failed\n");
    return 0;
  }
  return ucont->turns * sizeof(u64);
}

// called when a process read from a stream file
static ssize_t apecStream_write(struct file *f, const char __user *buf,
                                size_t len, loff_t *off) {
  APEC *adev = NULL;
  STREAM_USER_CONTAINER *ucont = NULL;
  SETUP_STREAM setstream;
  dbg_v("apecStream_write called \n");
  ucont = f->private_data;
  adev = ucont->adev;
  if (len != sizeof(SETUP_STREAM)) {
    dbg("len!=setup_stream \n");
    return 0;
  }
  if (copy_from_user(&setstream, buf, sizeof(SETUP_STREAM))) {
    dbg("copy_from_user in stream_write failed\n");
    return 0;
  }
  if (setstream.turns > transfers / 2) {
    dbg("too big acquistion. Turns: %lld offset: %lld, real buffer size: %lu",
        setstream.turns, setstream.offset, transfers);
    return 0;
  }
  if (abs(setstream.offset) > transfers / 2) {
    dbg("Offset too big. Turns: %lld offset: %lld, real buffer size: %lu",
        setstream.turns, setstream.offset, transfers);
    return 0;
  }

  ucont->offset = setstream.offset;
  ucont->turns = setstream.turns;
  if (ucont->buffer != NULL) {
    vfree(ucont->buffer);
    ucont->buffer = NULL;
  }
  ucont->buffer = (u64 *)vmalloc(ucont->turns * sizeof(u64));

  if (!ucont->buffer) {
    dbg("could not allocate a stream_user_container buffer\n");
    return -1;
  }

  dbg_v("setting offset to %lld and turns to %llu \n", ucont->offset,
        ucont->turns);

  *off += sizeof(SETUP_STREAM);
  return sizeof(SETUP_STREAM);
}

// the different operations for the statusExpert file
static struct file_operations stream_fops = {
    .owner = THIS_MODULE,
    .open = apecStream_open,
    .release = apecStream_close,
    .read = apecStream_read,
    .write = apecStream_write,
};

static int apecCont_open(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  CDEV_APEC *cdev;
  STREAM_USER_CONTAINER *ucont;
  u32 dev_minor = 0;
  u32 stream;
  unsigned long idx;

  cdev = container_of(inode->i_cdev, CDEV_APEC, c_dev);
  adev = cdev->adev;

  // get our atomic lock
  spin_lock(&adev->users_setup_lock);

  idx = bitmap_find_next_zero_area(adev->users_bitmap, MAX_USERS, 0, 1, 0);
  if (idx >= MAX_USERS) {
    dbg("all slots are taken by users\n");
    spin_unlock(&adev->users_setup_lock);
    return -1;
  }
  bitmap_set(adev->users_bitmap, idx, 1);
  dbg_v("got idx %lu \n", idx);
  spin_unlock(&adev->users_setup_lock);

  // allocate one struct per user
  ucont = (STREAM_USER_CONTAINER *)kzalloc(sizeof(STREAM_USER_CONTAINER),
                                           GFP_KERNEL);
  ucont->buffer = NULL;
  if (!ucont) {
    dbg("could not allocate a stream_user_containers\n");
    return -1;
  }
  ucont->adev = adev;
  // default is latest turn
  ucont->offset = 0;
  ucont->turns = 1;
  ucont->idx = idx;
  ucont->timeout = jiffies + HZ; // resched every second
  ucont->transfer_idx = atomic_read(&adev->transfer_idx_atomic);
  f->private_data = ucont;

  ucont->buffer = (u64 *)vmalloc(ucont->turns * sizeof(u64));
  if (!ucont->buffer) {
    dbg("could not allocate a stream_user_container buffer\n");
    return -1;
  }

  // check which stream we are opening
  dev_minor = MINOR(inode->i_rdev);
  for (stream = 0; stream < 10; stream++) {
    if (adev->contMinorNumbers[stream] == dev_minor) {
      ucont->stream = stream;
      break;
    }
  }

  dbg_v("stream_open\n");
  return 0;
}

// called when a process closes a stream file
static int apecCont_close(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  STREAM_USER_CONTAINER *ucont = NULL;
  ucont = f->private_data;
  adev = ucont->adev;
  if (ucont->buffer != NULL) {
    vfree(ucont->buffer);
    ucont->buffer = NULL;
  }

  dbg_v("stream_close \n");

  spin_lock(&adev->users_setup_lock);
  bitmap_clear(adev->users_bitmap, ucont->idx, 1);
  dbg_v("released idx %lu \n", ucont->idx);
  spin_unlock(&adev->users_setup_lock);

  if (f->private_data != NULL) {
    kfree(f->private_data);
    f->private_data = NULL;
  }
  return 0;
}

// called when a process read from a cont file
static ssize_t apecCont_read(struct file *f, char __user *buf, size_t len,
                             loff_t *off) {
  APEC *adev = NULL;
  STREAM_USER_CONTAINER *ucont = NULL;
  s64 delayCounter = 0;
  u32 i;
  struct TRANSFER *transfer;
  u32 idx;
  u32 last_idx;
  u32 copy_size;
  ucont = f->private_data;
  adev = ucont->adev;
  if (len < ucont->turns * sizeof(u64)) {
    dbg("buffer is not big enough for offsets\n");
    return 0;
  }

  last_idx = ucont->transfer_idx;
  // this is the last transfer that we read
  transfer = adev->buffer_transfers + last_idx * sizeof(TRANSFER);

  // jump forward to the next transfer to read
  for (i = 0; i < ucont->turns; i++) {
    transfer = transfer->next;
    delayCounter = transfer->offset + transferSizePerChannel * ucont->stream;
    *(ucont->buffer + i) = delayCounter;
  }
  idx = transfer->idx;
  ucont->transfer_idx = idx;
  copy_size = copy_to_user(buf, ucont->buffer, sizeof(u64) * ucont->turns);
  if (copy_size) {
    dbg("copy_to_user in stream_read failed\n");
    return 0;
  }

  while (true) {
    i = atomic_read(&adev->transfer_idx_atomic);
    // normal case with no wrap around and the transfer_idx is outside the range
    if ((last_idx < idx) && ((i < last_idx) || (i > idx))) {
      break;
    } else if ((last_idx > idx) && ((i < last_idx) && (i > idx))) {
      break;
    }

    // wait until a new frame has arrived
    atomic_set(&adev->users_locks[ucont->idx].user_lock, 0);
    ucont->timeout = jiffies + HZ; // resched every second
    while (atomic_read(&adev->users_locks[ucont->idx].user_lock) == 0) {
      if (time_after(jiffies, ucont->timeout)) {
        cond_resched();
        ucont->timeout = jiffies + HZ; // resched every second
      }
    }
  }

  return ucont->turns * sizeof(u64);
}

// called when a process read from a stream file
static ssize_t apecCont_write(struct file *f, const char __user *buf,
                              size_t len, loff_t *off) {

  APEC *adev = NULL;
  STREAM_USER_CONTAINER *ucont = NULL;
  SETUP_STREAM setstream;
  dbg_v("apecCont_write called \n");
  ucont = f->private_data;
  adev = ucont->adev;
  if (len != sizeof(SETUP_STREAM)) {
    dbg("len!=setup_stream \n");
    return 0;
  }
  if (copy_from_user(&setstream, buf, sizeof(SETUP_STREAM))) {
    dbg("copy_from_user in stream_write failed\n");
    return 0;
  }
  if (setstream.turns > transfers / 2) {
    dbg("too big acquistion. Turns: %lld offset: %lld, real buffer size: %lu",
        setstream.turns, setstream.offset, transfers);
    return 0;
  }
  if (abs(setstream.offset) > transfers / 2) {
    dbg("Offset too big. Turns: %lld offset: %lld, real buffer size: %lu",
        setstream.turns, setstream.offset, transfers);
    return 0;
  }

  ucont->offset = setstream.offset;
  ucont->turns = setstream.turns;
  ucont->transfer_idx = atomic_read(&adev->transfer_idx_atomic);
  if (ucont->buffer != NULL) {
    vfree(ucont->buffer);
    ucont->buffer = NULL;
  }
  ucont->buffer = (u64 *)vmalloc(ucont->turns * sizeof(u64));

  if (!ucont->buffer) {
    dbg("could not allocate a stream_user_container buffer\n");
    return -1;
  }

  dbg_v("setting offset to %lld and turns to %llu \n", ucont->offset,
        ucont->turns);

  *off += sizeof(SETUP_STREAM);
  return sizeof(SETUP_STREAM);
}

// the different operations for the statusExpert file
static struct file_operations cont_fops = {
    .owner = THIS_MODULE,
    .open = apecCont_open,
    .release = apecCont_close,
    .read = apecCont_read,
    .write = apecCont_write,
};

static int apecDirect_open(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  CDEV_APEC *cdev;
  STREAM_USER_CONTAINER *ucont;
  u32 dev_minor = 0;
  u32 stream;

  cdev = container_of(inode->i_cdev, CDEV_APEC, c_dev);
  adev = cdev->adev;

  // allocate one struct per user
  ucont = (STREAM_USER_CONTAINER *)kzalloc(sizeof(STREAM_USER_CONTAINER),
                                           GFP_KERNEL);
  ucont->buffer = NULL;
  if (!ucont) {
    dbg("could not allocate a stream_user_containers\n");
    return -1;
  }
  ucont->adev = adev;
  // default is latest turn
  ucont->offset = 0;
  ucont->turns = 1;
  f->private_data = ucont;

  ucont->buffer = (u64 *)vmalloc(ucont->turns * sizeof(u64));
  if (!ucont->buffer) {
    dbg("could not allocate a stream_user_container buffer\n");
    return -1;
  }

  // check which stream we are opening
  dev_minor = MINOR(inode->i_rdev);
  for (stream = 0; stream < 10; stream++) {
    if (adev->directMinorNumbers[stream] == dev_minor) {
      ucont->stream = stream;
      break;
    }
  }

  dbg_v("direct_open\n");
  return 0;
}

// called when a process closes a stream file
static int apecDirect_close(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  STREAM_USER_CONTAINER *ucont = NULL;
  ucont = f->private_data;
  adev = ucont->adev;
  if (ucont->buffer != NULL) {
    vfree(ucont->buffer);
    ucont->buffer = NULL;
  }

  dbg_v("direct_close \n");

  if (f->private_data != NULL) {
    kfree(f->private_data);
    f->private_data = NULL;
  }
  return 0;
}

// called when a process read from a stream file
static ssize_t apecDirect_read(struct file *f, char __user *buf, size_t len,
                               loff_t *off) {
  APEC *adev = NULL;
  STREAM_USER_CONTAINER *ucont = NULL;
  s64 delayCounter = 0;
  u32 i;
  struct TRANSFER *transfer;
  u32 idx;
  u32 writeback;
  ucont = f->private_data;
  adev = ucont->adev;
  if (len < ucont->turns * sizeof(u64)) {
    dbg("buffer is not big enough for offsets\n");
    return 0;
  }
  // read our latest transfer
  idx = atomic_read(&adev->direct_transfer_idx_atomic);
  transfer = adev->buffer_transfers + idx * sizeof(TRANSFER);

  writeback = *(u32 *)adev->buffer_writeback;

  // positive offset
  if (ucont->offset > 0) {
    for (delayCounter = ucont->offset; delayCounter >= 0; delayCounter--) {
      transfer = transfer->next;
    }
  }
  // negative offset
  else if (ucont->offset < 0) {
    for (delayCounter = ucont->offset; delayCounter < 0; delayCounter++) {
      transfer = transfer->previous;
    }
  }

  // for every turn
  for (i = 0; i < ucont->turns; i++) {
    *(ucont->buffer + ucont->turns - 1 - i) =
        transfer->offset + transferSizePerChannel * ucont->stream;
    transfer = transfer->previous;
  }

  // positive offset so we need to wait until the data is available
  delayCounter = 0;
  if (ucont->offset > 0) {
    ucont->timeout = jiffies + HZ; // resched every second
    while (delayCounter < ucont->offset) {
      delayCounter += 1;
      // wait for interrupt handler to signal us
      atomic_set(&adev->users_locks[ucont->idx].user_lock, 0);
      while (atomic_read(&adev->users_locks[ucont->idx].user_lock) == 0) {
        if (time_after(jiffies, ucont->timeout)) {
          cond_resched();
          ucont->timeout = jiffies + HZ; // resched every second
        }
      }
    }
  }

  if (copy_to_user(buf, ucont->buffer, sizeof(u64) * ucont->turns)) {
    dbg("copy_to_user in stream_read failed\n");
    return 0;
  }
  return ucont->turns * sizeof(u64);
}

// called when a process read from a stream file
static ssize_t apecDirect_write(struct file *f, const char __user *buf,
                                size_t len, loff_t *off) {

  APEC *adev = NULL;
  STREAM_USER_CONTAINER *ucont = NULL;
  SETUP_STREAM setstream;
  dbg_v("apecDirect_write called \n");
  ucont = f->private_data;
  adev = ucont->adev;
  if (len != sizeof(SETUP_STREAM)) {
    dbg("len!=setup_stream \n");
    return 0;
  }
  if (copy_from_user(&setstream, buf, sizeof(SETUP_STREAM))) {
    dbg("copy_from_user in stream_write failed\n");
    return 0;
  }
  if (setstream.turns > transfers / 2) {
    dbg("too big acquistion. Turns: %lld offset: %lld, real buffer size: %lu",
        setstream.turns, setstream.offset, transfers);
    return 0;
  }
  if (abs(setstream.offset) > transfers / 2) {
    dbg("Offset too big. Turns: %lld offset: %lld, real buffer size: %lu",
        setstream.turns, setstream.offset, transfers);
    return 0;
  }

  ucont->offset = setstream.offset;
  ucont->turns = setstream.turns;
  if (ucont->buffer != NULL) {
    vfree(ucont->buffer);
    ucont->buffer = NULL;
  }
  ucont->buffer = (u64 *)vmalloc(ucont->turns * sizeof(u64));

  if (!ucont->buffer) {
    dbg("could not allocate a stream_user_container buffer\n");
    return -1;
  }

  dbg_v("setting offset to %lld and turns to %llu \n", ucont->offset,
        ucont->turns);

  *off += sizeof(SETUP_STREAM);
  return sizeof(SETUP_STREAM);
}

// the different operations for the statusExpert file
static struct file_operations direct_fops = {
    .owner = THIS_MODULE,
    .open = apecDirect_open,
    .release = apecDirect_close,
    .read = apecDirect_read,
    .write = apecDirect_write,
};

static int frame_open(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  CDEV_APEC *cdev;
  STREAM_USER_CONTAINER *ucont;
  u32 dev_minor = 0;
  u32 stream;

  cdev = container_of(inode->i_cdev, CDEV_APEC, c_dev);
  adev = cdev->adev;

  // allocate one struct per user
  ucont = (STREAM_USER_CONTAINER *)kzalloc(sizeof(STREAM_USER_CONTAINER),
                                           GFP_KERNEL);
  ucont->buffer = NULL;
  if (!ucont) {
    dbg("could not allocate a stream_user_containers\n");
    return -1;
  }
  ucont->adev = adev;
  // default is latest turn
  ucont->offset = 0;
  ucont->turns = 1;
  ucont->idx = 0;
  ucont->timeout = jiffies + HZ; // resched every second
  f->private_data = ucont;
  ucont->buffer = NULL;

  // check which stream we are opening
  dev_minor = MINOR(inode->i_rdev);
  for (stream = 0; stream < 10; stream++) {
    if (adev->frameMinorNumbers[stream] == dev_minor) {
      ucont->stream = stream;
      break;
    }
  }

  dbg_v("frame_open\n");
  return 0;
}

// called when a process closes a stream file
static int frame_close(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  STREAM_USER_CONTAINER *ucont = NULL;
  ucont = f->private_data;
  adev = ucont->adev;

  if (f->private_data != NULL) {
    kfree(f->private_data);
    f->private_data = NULL;
  }
  return 0;
}

// called when a process read from a stream file
static ssize_t frame_read(struct file *f, char __user *buf, size_t len,
                          loff_t *off) {
  APEC *adev = NULL;
  STREAM_USER_CONTAINER *ucont = NULL;
  u32 tagbits;
  int size = 0;
  u32 to_copy = 0;
  char temp[32];

  ucont = f->private_data;
  adev = ucont->adev;
  tagbits = read_user_bar(adev->xdev, (135 + ucont->stream) * 4);

  // our output
  snprintf(temp, sizeof(temp), "%u\n", tagbits);
  // size of output
  size = strlen(temp);
  // we have printed everything
  if (*off >= size) {
    return 0;
  }
  // we can fit everything into the buffer
  if ((size - (*off)) <= len) {
    to_copy = size - (*off);
    if (copy_to_user(buf, temp + (*off), to_copy)) {
      dbg("copy_to_user in frame_read failed\n");
    }
    *off = size;
    return to_copy;
  }
  // we must copy a part
  else {
    if (copy_to_user(buf, temp + (*off), len)) {
      dbg("copy_to_user in frame_read failed\n");
    }
    *off += len;
    return len;
  }
}

// called when a process read from a stream file
static ssize_t frame_write(struct file *f, const char __user *buf, size_t len,
                           loff_t *off) {
  APEC *adev = NULL;
  STREAM_USER_CONTAINER *ucont = NULL;
  u32 tagbits;
  int ret;
  dbg_v("frame_write open \n");
  ucont = f->private_data;
  adev = ucont->adev;

  ret = kstrtouint_from_user(buf, len, 10, &tagbits);
  *off += len;
  if (!ret) {
    write_user_bar(adev->xdev, (135 + ucont->stream) * 4, tagbits);
    dbg_v("wrote tagbit %u to register %u \n", tagbits,
          (135 + ucont->stream) * 4);
  } else {
    dbg("frame_write failed \n");
  }

  return len;
}

// the different operations for the statusExpert file
static struct file_operations frame_fops = {
    .owner = THIS_MODULE,
    .open = frame_open,
    .release = frame_close,
    .read = frame_read,
    .write = frame_write,
};

static int stream_status_open(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  CDEV_APEC *cdev;
  STREAM_USER_CONTAINER *ucont;
  u32 dev_minor = 0;
  u32 stream;

  cdev = container_of(inode->i_cdev, CDEV_APEC, c_dev);
  adev = cdev->adev;

  // allocate one struct per user
  ucont = (STREAM_USER_CONTAINER *)kzalloc(sizeof(STREAM_USER_CONTAINER),
                                           GFP_KERNEL);
  ucont->buffer = NULL;
  if (!ucont) {
    dbg("could not allocate a stream_user_containers\n");
    return -1;
  }
  ucont->adev = adev;
  // default is latest turn
  ucont->offset = 0;
  ucont->turns = 1;
  ucont->idx = 0;
  ucont->timeout = jiffies + HZ; // resched every second
  f->private_data = ucont;
  ucont->buffer = NULL;

  // check which stream we are opening
  dev_minor = MINOR(inode->i_rdev);
  for (stream = 0; stream < 10; stream++) {
    if (adev->streamStatusMinorNumbers[stream] == dev_minor) {
      ucont->stream = stream;
      break;
    }
  }

  dbg_v("stream_status_open\n");
  return 0;
}

// called when a process closes a stream file
static int stream_status_close(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  STREAM_USER_CONTAINER *ucont = NULL;
  ucont = f->private_data;
  adev = ucont->adev;

  if (f->private_data != NULL) {
    kfree(f->private_data);
    f->private_data = NULL;
  }
  return 0;
}

// called when a process read from a stream file
static ssize_t stream_status_read(struct file *f, char __user *buf, size_t len,
                                  loff_t *off) {
  APEC *adev = NULL;
  STREAM_USER_CONTAINER *ucont = NULL;
  u32 stream;
  int size = 0;
  char *temp;
  u32 to_copy = 0;
  u32 offset = 0;
  u32 sfpoffset = 0;

  u32 streamstatus;
  u32 gtwizardstatus;
  u32 gtresetdone;
  u32 databufferstatus;
  u32 bypasscontrollerstatus;
  u32 resetstatus;
  u32 moddetrxlosstatus;
  u32 sfpTemp;
  u32 sfpVCC;
  u32 sfpPower;
  u32 sfpalarm0;
  u32 sfpalarm1;
  u32 sfpalarm2;
  u32 sfpalarm3;
  u32 gt_mmcm_lock_out;
  u32 gt_reset_done;
  u32 gt_FSM_reset_done;
  u32 gt_cpllfbclklost_out;
  u32 cplllock_out;

  ucont = f->private_data;
  adev = ucont->adev;
  stream = ucont->stream;
  temp = (char *)vmalloc(1024);
  sfpoffset = 33 + stream * 10;

  // general reads
  gtwizardstatus = read_user_bar(adev->xdev, 18 * 4);
  gtresetdone = read_user_bar(adev->xdev, 19 * 4);
  databufferstatus = read_user_bar(adev->xdev, 20 * 4);
  bypasscontrollerstatus = read_user_bar(adev->xdev, 21 * 4);
  resetstatus = read_user_bar(adev->xdev, 22 * 4);
  moddetrxlosstatus = read_user_bar(adev->xdev, 23 * 4);

  // specific reads
  streamstatus = read_user_bar(adev->xdev, (8 + stream) * 4);
  sfpTemp = read_user_bar(adev->xdev, (sfpoffset)*4) << 8;
  sfpTemp |= (read_user_bar(adev->xdev, (sfpoffset + 1) * 4) & 0xff);
  sfpVCC = read_user_bar(adev->xdev, (sfpoffset + 2) * 4) << 8;
  sfpVCC |= (read_user_bar(adev->xdev, (sfpoffset + 3) * 4) & 0xff);
  sfpPower = read_user_bar(adev->xdev, (sfpoffset + 4) * 4) << 8;
  sfpPower |= (read_user_bar(adev->xdev, (sfpoffset + 5) * 4) & 0xff);
  sfpalarm0 = read_user_bar(adev->xdev, (sfpoffset + 6) * 4);
  sfpalarm1 = read_user_bar(adev->xdev, (sfpoffset + 7) * 4);
  sfpalarm2 = read_user_bar(adev->xdev, (sfpoffset + 8) * 4);
  sfpalarm3 = read_user_bar(adev->xdev, (sfpoffset + 9) * 4);

  switch (stream) {
  case 0:
    gt_mmcm_lock_out = gtwizardstatus & 0x00000008;
    gt_reset_done = gtresetdone & 0x00000008;
    gt_FSM_reset_done = gtresetdone & 0x00002000;
    gt_cpllfbclklost_out = gtwizardstatus & 0x00010000;
    cplllock_out = gtwizardstatus & 0x00020000;
    break;
  case 1:
    gt_mmcm_lock_out = gtwizardstatus & 0x00000002;
    gt_reset_done = gtresetdone & 0x00000002;
    gt_FSM_reset_done = gtresetdone & 0x00000800;
    gt_cpllfbclklost_out = gtwizardstatus & 0x00001000;
    cplllock_out = gtwizardstatus & 0x00002000;
    break;
  case 2:
    gt_mmcm_lock_out = gtwizardstatus & 0x00000004;
    gt_reset_done = gtresetdone & 0x00000004;
    gt_FSM_reset_done = gtresetdone & 0x00001000;
    gt_cpllfbclklost_out = gtwizardstatus & 0x00004000;
    cplllock_out = gtwizardstatus & 0x00008000;
    break;
  case 3:
    gt_mmcm_lock_out = gtwizardstatus & 0x00000010;
    gt_reset_done = gtresetdone & 0x00000010;
    gt_FSM_reset_done = gtresetdone & 0x00004000;
    gt_cpllfbclklost_out = gtwizardstatus & 0x00040000;
    cplllock_out = gtwizardstatus & 0x00080000;
    break;
  case 4:
    gt_mmcm_lock_out = gtwizardstatus & 0x00000020;
    gt_reset_done = gtresetdone & 0x00000020;
    gt_FSM_reset_done = gtresetdone & 0x00008000;
    gt_cpllfbclklost_out = gtwizardstatus & 0x00100000;
    cplllock_out = gtwizardstatus & 0x00200000;
    break;
  case 5:
    gt_mmcm_lock_out = gtwizardstatus & 0x00000001;
    gt_reset_done = gtresetdone & 0x00000001;
    gt_FSM_reset_done = gtresetdone & 0x00000400;
    gt_cpllfbclklost_out = gtwizardstatus & 0x00000400;
    cplllock_out = gtwizardstatus & 0x00000800;
    break;
  case 6:
    gt_mmcm_lock_out = gtwizardstatus & 0x00000040;
    gt_reset_done = gtresetdone & 0x00000040;
    gt_FSM_reset_done = gtresetdone & 0x00010000;
    gt_cpllfbclklost_out = gtwizardstatus & 0x00400000;
    cplllock_out = gtwizardstatus & 0x00800000;
    break;
  case 7:
    gt_mmcm_lock_out = gtwizardstatus & 0x00000200;
    gt_reset_done = gtresetdone & 0x00000200;
    gt_FSM_reset_done = gtresetdone & 0x00080000;
    gt_cpllfbclklost_out = gtwizardstatus & 0x10000000;
    cplllock_out = gtwizardstatus & 0x20000000;
    break;
  case 8:
    gt_mmcm_lock_out = gtwizardstatus & 0x00000080;
    gt_reset_done = gtresetdone & 0x00000080;
    gt_FSM_reset_done = gtresetdone & 0x00020000;
    gt_cpllfbclklost_out = gtwizardstatus & 0x01000000;
    cplllock_out = gtwizardstatus & 0x02000000;
    break;
  case 9:
    gt_mmcm_lock_out = gtwizardstatus & 0x00000100;
    gt_reset_done = gtresetdone & 0x00000100;
    gt_FSM_reset_done = gtresetdone & 0x00040000;
    gt_cpllfbclklost_out = gtwizardstatus & 0x04000000;
    cplllock_out = gtwizardstatus & 0x08000000;
    break;

  default:
    gt_mmcm_lock_out = 0;
    gt_reset_done = 0;
    gt_FSM_reset_done = 0;
    gt_cpllfbclklost_out = 1;
    cplllock_out = 0;
    break;
  }

  offset += snprintf(
      temp + offset, 1024 - offset,
      "three_commas_found : %c\nheader_found : %c\ndisperr : "
      "%c%c%c%c%c%c%c%c\nnotintable : %c%c%c%c%c%c%c%c\nrx_buf_status : "
      "%c%c%c\nbytesisaligned : %c\ngt_mmcm_lock_out : %c\ngt_reset_done : "
      "%c\ngt_fsm_reset_done : %c\nstream_running : %c\npipeline_reset : "
      "%c\nrx_los : %c\nmoddet : %c\ngt_cpllfbclklost_out : %c\ncplllock_out : "
      "%c\nsfp_temp : %u\nsfp_vcc : %u\nsfp_power : %u\nalarm_temp_high : "
      "%c\nalarm_temp_low : %c\nalarm_vcc_high : %c\nalarm_vcc_low : "
      "%c\nalarm_rx_power_high : %c\nalarm_rx_power_low : "
      "%c\nwarning_temp_high : %c\nwarning_temp_low : %c\nwarning_vcc_high : "
      "%c\nwarning_vcc_low : %c\nwarning_rx_power_high : "
      "%c\nwarning_rx_power_low : %c\n",
      (streamstatus & 0x01 ? '1' : '0'),       // three commas
      (streamstatus & 0x02 ? '1' : '0'),       // header-found
      BYTE_TO_BINARY(streamstatus >> 2),       // disper
      BYTE_TO_BINARY(streamstatus >> 10),      // notintable
      (streamstatus & 0x00100000 ? '1' : '0'), // rx-bufstat
      (streamstatus & 0x00080000 ? '1' : '0'), // rx-bufstat
      (streamstatus & 0x00040000 ? '1' : '0'), // rx-bufstat
      (streamstatus & 0x00200000 ? '1' : '0'), // byteisaligned
      (gt_mmcm_lock_out ? '1' : '0'),          // gt_mmcm_lock_out
      (gt_reset_done ? '1' : '0'),             // gt_reset_done
      (gt_FSM_reset_done ? '1' : '0'),         // gt_FSM_reset_done
      (databufferstatus & (0x00000001 << stream) ? '1' : '0'), // stream_running
      (resetstatus & (0x00000001 << stream) ? '1' : '0'),      // pipeline_reset
      (moddetrxlosstatus & (0x00000001 << stream) ? '1' : '0'), // rx_los
      (moddetrxlosstatus & (0x00000400 << stream) ? '1' : '0'), // moddet
      (gt_cpllfbclklost_out ? '1' : '0'), // gt_cpllfbclklost_out
      (cplllock_out ? '1' : '0'),         // cplllock_out
      sfpTemp, sfpVCC, sfpPower, (sfpalarm0 & 0x00000080 ? '1' : '0'),
      (sfpalarm0 & 0x00000040 ? '1' : '0'),
      (sfpalarm0 & 0x00000020 ? '1' : '0'),
      (sfpalarm0 & 0x00000010 ? '1' : '0'),
      (sfpalarm1 & 0x00000080 ? '1' : '0'),
      (sfpalarm1 & 0x00000040 ? '1' : '0'),
      (sfpalarm2 & 0x00000080 ? '1' : '0'),
      (sfpalarm2 & 0x00000040 ? '1' : '0'),
      (sfpalarm2 & 0x00000020 ? '1' : '0'),
      (sfpalarm2 & 0x00000010 ? '1' : '0'),
      (sfpalarm3 & 0x00000080 ? '1' : '0'),
      (sfpalarm3 & 0x00000040 ? '1' : '0'));
  // size of output
  size = strlen(temp);
  // we have printed everything
  if (*off >= size) {
    vfree(temp);
    return 0;
  }
  // we can fit everything into the buffer
  if ((size - (*off)) <= len) {
    to_copy = size - (*off);
    if (copy_to_user(buf, temp + (*off), to_copy)) {
      dbg("copy_to_user in status_fpga_read failed\n");
    }
    *off = size;
    vfree(temp);
    return to_copy;
  }
  // we must copy a part
  else {
    if (copy_to_user(buf, temp + (*off), len)) {
      dbg("copy_to_user in status_fpga_read failed\n");
    }
    *off += len;
    vfree(temp);
    return len;
  }
}

// the different operations for the statusExpert file
static struct file_operations stream_status_fops = {
    .owner = THIS_MODULE,
    .open = stream_status_open,
    .release = stream_status_close,
    .read = stream_status_read,
};

static int stream_interrupt_open(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  CDEV_APEC *cdev;
  STREAM_USER_CONTAINER *ucont;
  u32 dev_minor = 0;
  u32 stream;
  u32 idx;

  cdev = container_of(inode->i_cdev, CDEV_APEC, c_dev);
  adev = cdev->adev;

  // allocate one struct per user
  ucont = (STREAM_USER_CONTAINER *)kzalloc(sizeof(STREAM_USER_CONTAINER),
                                           GFP_KERNEL);
  ucont->buffer = NULL;
  if (!ucont) {
    dbg("could not allocate a stream_user_containers\n");
    return -1;
  }
  ucont->adev = adev;
  // default is latest turn
  ucont->offset = 0;
  ucont->turns = 1;
  ucont->idx = 0;
  ucont->timeout = jiffies + HZ; // resched every second
  f->private_data = ucont;
  ucont->buffer = NULL;

  // check which stream we are opening
  dev_minor = MINOR(inode->i_rdev);
  for (stream = 0; stream < 10; stream++) {
    if (adev->streamInterruptMinorNumbers[stream] == dev_minor) {
      ucont->stream = stream;
      break;
    }
  }
  spin_lock(&adev->users_setup_lock);
  idx = bitmap_find_next_zero_area(adev->users_interrupt_bitmap[ucont->stream],
                                   MAX_USERS, 0, 1, 0);
  if (idx >= MAX_USERS) {
    dbg("all slots are taken by users in interrupt open\n");
    spin_unlock(&adev->users_setup_lock);
    return -1;
  }
  bitmap_set(adev->users_interrupt_bitmap[ucont->stream], idx, 1);
  dbg_v("got idx %u \n", idx);
  spin_unlock(&adev->users_setup_lock);
  ucont->idx = idx;
  dbg_v("stream_interrupt_open\n");
  return 0;
}

// called when a process closes a stream file
static int stream_interrupt_close(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  STREAM_USER_CONTAINER *ucont = NULL;
  ucont = f->private_data;
  adev = ucont->adev;
  atomic_set(&adev->users_interrupt_locks[ucont->stream][ucont->idx].user_lock,
             -2);
  spin_lock(&adev->users_setup_lock);
  bitmap_clear(adev->users_interrupt_bitmap[ucont->stream], ucont->idx, 1);
  dbg_v("released idx %lu \n", ucont->idx);
  spin_unlock(&adev->users_setup_lock);

  if (f->private_data != NULL) {
    kfree(f->private_data);
    f->private_data = NULL;
  }
  return 0;
}

// called when a process read from a stream interrupt file
static ssize_t stream_interrupt_read(struct file *f, char __user *buf,
                                     size_t len, loff_t *off) {
  APEC *adev = NULL;
  STREAM_USER_CONTAINER *ucont = NULL;
  u32 stream;
  int res;
  int size = 0;
  int toReturn = 0;
  char temp[32];
  u32 to_copy = 0;
  // we already wrote the interrupt but it reads until end of file
  if (*off != 0) {
    return 0;
  }

  ucont = f->private_data;
  adev = ucont->adev;
  stream = ucont->stream;
  atomic_set(&adev->users_interrupt_locks[ucont->stream][ucont->idx].user_lock,
             -1);
  res = wait_event_interruptible_timeout(
      (adev->user_interrupt_queues[stream]),
      atomic_read(
          &adev->users_interrupt_locks[ucont->stream][ucont->idx].user_lock) ==
          0,
      5 * HZ);
  atomic_set(&adev->users_interrupt_locks[ucont->stream][ucont->idx].user_lock,
             1);
  // we were woken up by the irq and not the timeout
  if (res > 1) {
    toReturn = (int)atomic_read(&(adev->virtual_user_interrupt[stream]));
  } else {
    toReturn = -1;
  }

  // our output
  snprintf(temp, sizeof(temp), "%i\n", toReturn);
  // size of output
  size = strlen(temp);
  if (len < size) {
    dbg("buffer is not big enough for interrupt\n");
    return 0;
  }

  // we have printed everything
  if (*off >= size) {
    return 0;
  }
  // we can fit everything into the buffer
  if ((size - (*off)) <= len) {
    to_copy = size - (*off);
    if (copy_to_user(buf, temp + (*off), to_copy)) {
      dbg("copy_to_user in stream_interrupt_read failed\n");
    }
    *off = size;
    return to_copy;
  }
  // we must copy a part
  else {
    if (copy_to_user(buf, temp + (*off), len)) {
      dbg("copy_to_user in stream_interrupt_read failed\n");
    }
    *off += len;
    return len;
  }
}

// the different operations for the stream interrupt file
static struct file_operations stream_interrupt_fops = {
    .owner = THIS_MODULE,
    .open = stream_interrupt_open,
    .release = stream_interrupt_close,
    .read = stream_interrupt_read,
};

static int bus_address_open(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  CDEV_APEC *cdev;
  cdev = container_of(inode->i_cdev, CDEV_APEC, c_dev);
  adev = cdev->adev;
  f->private_data = adev;
  return 0;
}

// called when a process closes a stream file
static int bus_address_close(struct inode *inode, struct file *f) { return 0; }

// called when a process read from a stream file
static ssize_t bus_address_read(struct file *f, char __user *buf, size_t len,
                                loff_t *off) {
  APEC *adev = NULL;
  u32 address;
  int size = 0;
  u32 to_copy = 0;
  char temp[32];

  adev = f->private_data;
  address = adev->bus_address;

  // our output
  snprintf(temp, sizeof(temp), "%u\n", address);
  // size of output
  size = strlen(temp);
  // we have printed everything
  if (*off >= size) {
    return 0;
  }
  // we can fit everything into the buffer
  if ((size - (*off)) <= len) {
    to_copy = size - (*off);
    if (copy_to_user(buf, temp + (*off), to_copy)) {
      dbg("copy_to_user in frame_read failed\n");
    }
    *off = size;
    return to_copy;
  }
  // we must copy a part
  else {
    if (copy_to_user(buf, temp + (*off), len)) {
      dbg("copy_to_user in frame_read failed\n");
    }
    *off += len;
    return len;
  }
}
static ssize_t bus_address_write(struct file *f, const char __user *buf,
                                 size_t len, loff_t *off) {
  APEC *adev = NULL;
  u32 address;
  int ret;
  adev = f->private_data;

  ret = kstrtouint_from_user(buf, len, 10, &address);
  *off += len;
  if (!ret) {
    adev->bus_address = address;
  } else {
    dbg("bus_address_write failed \n");
  }
  return len;
}

// the different operations for the statusExpert file
static struct file_operations bus_address_fops = {
    .owner = THIS_MODULE,
    .open = bus_address_open,
    .release = bus_address_close,
    .read = bus_address_read,
    .write = bus_address_write,
};

static int bus_data_open(struct inode *inode, struct file *f) {
  APEC *adev = NULL;
  CDEV_APEC *cdev;
  cdev = container_of(inode->i_cdev, CDEV_APEC, c_dev);
  adev = cdev->adev;
  f->private_data = adev;
  return 0;
}

// called when a process closes a stream file
static int bus_data_close(struct inode *inode, struct file *f) { return 0; }

// called when a process read from a stream file
static ssize_t bus_data_read(struct file *f, char __user *buf, size_t len,
                             loff_t *off) {
  APEC *adev = NULL;
  u32 address;
  u32 data;
  int size = 0;
  u32 to_copy = 0;
  char temp[32];

  adev = f->private_data;
  address = adev->bus_address;
  data = read_user_bar(adev->xdev, address * 4);

  // our output
  snprintf(temp, sizeof(temp), "%u\n", data);
  // size of output
  size = strlen(temp);
  // we have printed everything
  if (*off >= size) {
    return 0;
  }
  // we can fit everything into the buffer
  if ((size - (*off)) <= len) {
    to_copy = size - (*off);
    if (copy_to_user(buf, temp + (*off), to_copy)) {
      dbg("copy_to_user in frame_read failed\n");
    }
    *off = size;
    return to_copy;
  }
  // we must copy a part
  else {
    if (copy_to_user(buf, temp + (*off), len)) {
      dbg("copy_to_user in frame_read failed\n");
    }
    *off += len;
    return len;
  }
}
static ssize_t bus_data_write(struct file *f, const char __user *buf,
                              size_t len, loff_t *off) {
  APEC *adev = NULL;
  u32 address;
  u32 data;
  int ret;

  adev = f->private_data;
  address = adev->bus_address;

  ret = kstrtouint_from_user(buf, len, 10, &data);
  *off += len;
  if (!ret) {
    write_user_bar(adev->xdev, address * 4, data);
  } else {
    dbg("bus_data_write failed \n");
  }
  return len;
}

// the different operations for the statusExpert file
static struct file_operations bus_data_fops = {
    .owner = THIS_MODULE,
    .open = bus_data_open,
    .release = bus_data_close,
    .read = bus_data_read,
    .write = bus_data_write,
};

static void remove_device_interface(APEC *adev) {
  int i = 0;
  BUG_ON(!adev);
  if (adev->interfaces_created) {
    for (i = adev->devices_number; i > 0; i--) {
      cdev_del(&adev->cdevs[i].c_dev);
      device_destroy(adev->cl[i], MKDEV(adev->major, adev->minor + i));
      class_destroy(adev->cl[i]);
    }
    unregister_chrdev_region(adev->first, DEVICES_PER_CARD);
  }
  adev->interfaces_created = 0;
}

static int create_device_interface(APEC *adev) {
  int ret = 0;
  char temp[128];
  char streamnumber[16];
  int counter = 0;
  int i = 0;
  // Allocate devices for this device
  if ((ret = alloc_chrdev_region(&adev->first, 0, DEVICES_PER_CARD,
                                 DRV_MODULE_NAME)) < 0) {
    dbg("failed to allocate a range of char devices \n");
    goto err_alloc;
  }
  adev->minor = MINOR(adev->first);
  adev->major = MAJOR(adev->first);

  //-----------------------------fpga status-----------------------
  counter += 1;

  snprintf(temp, sizeof(temp), "apec%02u%02ustatus", adev->bus_nr,
           adev->dev_nr);
  if (IS_ERR(adev->cl[counter] = class_create(THIS_MODULE, temp))) {
    dbg("failed to create a class \n");
    counter -= 1;
    goto fail;
  }

  strcpy(temp, adev->device_head);
  strcat(temp, "status");
  if (IS_ERR(adev->dev[counter] = device_create(
                 adev->cl[counter], NULL,
                 MKDEV(adev->major, adev->minor + counter), NULL, temp))) {
    dbg("failed to create a device \n");
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  cdev_init(&adev->cdevs[counter].c_dev, &status_fpga_fops);
  if ((ret = cdev_add(&adev->cdevs[counter].c_dev,
                      MKDEV(adev->major, adev->minor + counter), 1)) < 0) {
    dbg("failed to add a device \n");
    device_destroy(adev->cl[counter],
                   MKDEV(adev->major, adev->minor + counter));
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  //-----------------------------buffer status-----------------------
  counter += 1;
  snprintf(temp, sizeof(temp), "apec%02u%02ubuffer", adev->bus_nr,
           adev->dev_nr);
  if (IS_ERR(adev->cl[counter] = class_create(THIS_MODULE, temp))) {
    dbg("failed to create a class \n");
    counter -= 1;
    goto fail;
  }
  strcpy(temp, adev->device_head);
  strcat(temp, "buffer");
  if (IS_ERR(adev->dev[counter] = device_create(
                 adev->cl[counter], NULL,
                 MKDEV(adev->major, adev->minor + counter), NULL, temp))) {
    dbg("failed to create a device \n");
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  cdev_init(&adev->cdevs[counter].c_dev, &mmapdrv);
  if ((ret = cdev_add(&adev->cdevs[counter].c_dev,
                      MKDEV(adev->major, adev->minor + counter), 1)) < 0) {
    dbg("failed to add a device \n");
    device_destroy(adev->cl[counter],
                   MKDEV(adev->major, adev->minor + counter));
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  //-----------------------------unstuck operation-----------------------
  counter += 1;
  snprintf(temp, sizeof(temp), "apec%02u%02uunstuck", adev->bus_nr,
           adev->dev_nr);
  if (IS_ERR(adev->cl[counter] = class_create(THIS_MODULE, temp))) {
    dbg("failed to create a class \n");
    counter -= 1;
    goto fail;
  }
  strcpy(temp, adev->device_head);
  strcat(temp, "unstuck");
  if (IS_ERR(adev->dev[counter] = device_create(
                 adev->cl[counter], NULL,
                 MKDEV(adev->major, adev->minor + counter), NULL, temp))) {
    dbg("failed to create a device \n");
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  cdev_init(&adev->cdevs[counter].c_dev, &unstuck_fops);
  if ((ret = cdev_add(&adev->cdevs[counter].c_dev,
                      MKDEV(adev->major, adev->minor + counter), 1)) < 0) {
    dbg("failed to add a device \n");
    device_destroy(adev->cl[counter],
                   MKDEV(adev->major, adev->minor + counter));
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }
  //-----------------------------reset operation-----------------------
  counter += 1;
  snprintf(temp, sizeof(temp), "apec%02u%02ureset", adev->bus_nr, adev->dev_nr);
  if (IS_ERR(adev->cl[counter] = class_create(THIS_MODULE, temp))) {
    dbg("failed to create a class \n");
    counter -= 1;
    goto fail;
  }
  strcpy(temp, adev->device_head);
  strcat(temp, "reset");
  if (IS_ERR(adev->dev[counter] = device_create(
                 adev->cl[counter], NULL,
                 MKDEV(adev->major, adev->minor + counter), NULL, temp))) {
    dbg("failed to create a device \n");
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  cdev_init(&adev->cdevs[counter].c_dev, &reset_fops);
  if ((ret = cdev_add(&adev->cdevs[counter].c_dev,
                      MKDEV(adev->major, adev->minor + counter), 1)) < 0) {
    dbg("failed to add a device \n");
    device_destroy(adev->cl[counter],
                   MKDEV(adev->major, adev->minor + counter));
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  //---------Buffer size--------------------------------------------
  counter += 1;
  snprintf(temp, sizeof(temp), "apec%02u%02ubufferSize", adev->bus_nr,
           adev->dev_nr);
  if (IS_ERR(adev->cl[counter] = class_create(THIS_MODULE, temp))) {
    dbg("failed to create a class \n");
    counter -= 1;
    goto fail;
  }

  strcpy(temp, adev->device_head);
  strcat(temp, "bufferSize");
  if (IS_ERR(adev->dev[counter] = device_create(
                 adev->cl[counter], NULL,
                 MKDEV(adev->major, adev->minor + counter), NULL, temp))) {
    dbg("failed to create a device \n");
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  cdev_init(&adev->cdevs[counter].c_dev, &buffer_size_fops);
  if ((ret = cdev_add(&adev->cdevs[counter].c_dev,
                      MKDEV(adev->major, adev->minor + counter), 1)) < 0) {
    dbg("failed to add a device \n");
    device_destroy(adev->cl[counter],
                   MKDEV(adev->major, adev->minor + counter));
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  //---------Channel size--------------------------------------------
  counter += 1;
  snprintf(temp, sizeof(temp), "apec%02u%02uchannelSize", adev->bus_nr,
           adev->dev_nr);
  if (IS_ERR(adev->cl[counter] = class_create(THIS_MODULE, temp))) {
    dbg("failed to create a class \n");
    counter -= 1;
    goto fail;
  }

  strcpy(temp, adev->device_head);
  strcat(temp, "channelSize");
  if (IS_ERR(adev->dev[counter] = device_create(
                 adev->cl[counter], NULL,
                 MKDEV(adev->major, adev->minor + counter), NULL, temp))) {
    dbg("failed to create a device \n");
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  cdev_init(&adev->cdevs[counter].c_dev, &channel_size_fops);
  if ((ret = cdev_add(&adev->cdevs[counter].c_dev,
                      MKDEV(adev->major, adev->minor + counter), 1)) < 0) {
    dbg("failed to add a device \n");
    device_destroy(adev->cl[counter],
                   MKDEV(adev->major, adev->minor + counter));
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  //----------------dumpbypass--------------------------------------------------
  counter += 1;
  snprintf(temp, sizeof(temp), "apec%02u%02udumpBypass", adev->bus_nr,
           adev->dev_nr);
  if (IS_ERR(adev->cl[counter] = class_create(THIS_MODULE, temp))) {
    dbg("failed to create a class \n");
    counter -= 1;
    goto fail;
  }

  strcpy(temp, adev->device_head);
  strcat(temp, "dumpBypass");
  if (IS_ERR(adev->dev[counter] = device_create(
                 adev->cl[counter], NULL,
                 MKDEV(adev->major, adev->minor + counter), NULL, temp))) {
    dbg("failed to create a device \n");
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  cdev_init(&adev->cdevs[counter].c_dev, &bypass_dump_fops);
  if ((ret = cdev_add(&adev->cdevs[counter].c_dev,
                      MKDEV(adev->major, adev->minor + counter), 1)) < 0) {
    dbg("failed to add a device \n");
    device_destroy(adev->cl[counter],
                   MKDEV(adev->major, adev->minor + counter));
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  //----------------dumpDesc--------------------------------------------------
  counter += 1;
  snprintf(temp, sizeof(temp), "apec%02u%02udumpDesc", adev->bus_nr,
           adev->dev_nr);
  if (IS_ERR(adev->cl[counter] = class_create(THIS_MODULE, temp))) {
    dbg("failed to create a class \n");
    counter -= 1;
    goto fail;
  }

  strcpy(temp, adev->device_head);
  strcat(temp, "dumpDesc");
  if (IS_ERR(adev->dev[counter] = device_create(
                 adev->cl[counter], NULL,
                 MKDEV(adev->major, adev->minor + counter), NULL, temp))) {
    dbg("failed to create a device \n");
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  cdev_init(&adev->cdevs[counter].c_dev, &desc_dump_fops);
  if ((ret = cdev_add(&adev->cdevs[counter].c_dev,
                      MKDEV(adev->major, adev->minor + counter), 1)) < 0) {
    dbg("failed to add a device \n");
    device_destroy(adev->cl[counter],
                   MKDEV(adev->major, adev->minor + counter));
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  //----------------per
  // channel--------------------------------------------------
  for (i = 0; i < channels; i++) {
    //----------------streams--------------------------------------------------
    counter += 1;
    snprintf(temp, sizeof(temp), "apec%02u%02ustream%d", adev->bus_nr,
             adev->dev_nr, i);
    if (IS_ERR(adev->cl[counter] = class_create(THIS_MODULE, temp))) {
      dbg("failed to create a class \n");
      counter -= 1;
      goto fail;
    }
    snprintf(streamnumber, sizeof(streamnumber), "%d", i);
    strcpy(temp, adev->device_head);
    strcat(temp, "stream");
    strcat(temp, streamnumber);
    strcat(temp, "!stream");
    if (IS_ERR(adev->dev[counter] = device_create(
                   adev->cl[counter], NULL,
                   MKDEV(adev->major, adev->minor + counter), NULL, temp))) {
      dbg("failed to create a device \n");
      class_destroy(adev->cl[counter]);
      counter -= 1;
      goto fail;
    }
    adev->streamMinorNumbers[i] = adev->minor + counter;

    cdev_init(&adev->cdevs[counter].c_dev, &stream_fops);
    if ((ret = cdev_add(&adev->cdevs[counter].c_dev,
                        MKDEV(adev->major, adev->minor + counter), 1)) < 0) {
      dbg("failed to add a device \n");
      device_destroy(adev->cl[counter],
                     MKDEV(adev->major, adev->minor + counter));
      class_destroy(adev->cl[counter]);
      counter -= 1;
      goto fail;
    }

    //----------------cont--------------------------------------------------
    counter += 1;
    snprintf(temp, sizeof(temp), "apec%02u%02ucont%d", adev->bus_nr,
             adev->dev_nr, i);
    if (IS_ERR(adev->cl[counter] = class_create(THIS_MODULE, temp))) {
      dbg("failed to create a class \n");
      counter -= 1;
      goto fail;
    }
    snprintf(streamnumber, sizeof(streamnumber), "%d", i);
    strcpy(temp, adev->device_head);
    strcat(temp, "stream");
    strcat(temp, streamnumber);
    strcat(temp, "!cont");
    if (IS_ERR(adev->dev[counter] = device_create(
                   adev->cl[counter], NULL,
                   MKDEV(adev->major, adev->minor + counter), NULL, temp))) {
      dbg("failed to create a device \n");
      class_destroy(adev->cl[counter]);
      counter -= 1;
      goto fail;
    }
    adev->contMinorNumbers[i] = adev->minor + counter;

    cdev_init(&adev->cdevs[counter].c_dev, &cont_fops);
    if ((ret = cdev_add(&adev->cdevs[counter].c_dev,
                        MKDEV(adev->major, adev->minor + counter), 1)) < 0) {
      dbg("failed to add a device \n");
      device_destroy(adev->cl[counter],
                     MKDEV(adev->major, adev->minor + counter));
      class_destroy(adev->cl[counter]);
      counter -= 1;
      goto fail;
    }

    //----------------direct--------------------------------------------------
    counter += 1;
    snprintf(temp, sizeof(temp), "apec%02u%02udirect%d", adev->bus_nr,
             adev->dev_nr, i);
    if (IS_ERR(adev->cl[counter] = class_create(THIS_MODULE, temp))) {
      dbg("failed to create a class \n");
      counter -= 1;
      goto fail;
    }
    snprintf(streamnumber, sizeof(streamnumber), "%d", i);
    strcpy(temp, adev->device_head);
    strcat(temp, "stream");
    strcat(temp, streamnumber);
    strcat(temp, "!direct");
    if (IS_ERR(adev->dev[counter] = device_create(
                   adev->cl[counter], NULL,
                   MKDEV(adev->major, adev->minor + counter), NULL, temp))) {
      dbg("failed to create a device \n");
      class_destroy(adev->cl[counter]);
      counter -= 1;
      goto fail;
    }
    adev->directMinorNumbers[i] = adev->minor + counter;

    cdev_init(&adev->cdevs[counter].c_dev, &direct_fops);
    if ((ret = cdev_add(&adev->cdevs[counter].c_dev,
                        MKDEV(adev->major, adev->minor + counter), 1)) < 0) {
      dbg("failed to add a device \n");
      device_destroy(adev->cl[counter],
                     MKDEV(adev->major, adev->minor + counter));
      class_destroy(adev->cl[counter]);
      counter -= 1;
      goto fail;
    }
    //----------------frames--------------------------------------------------
    counter += 1;
    snprintf(temp, sizeof(temp), "apec%02u%02uframe%d", adev->bus_nr,
             adev->dev_nr, i);
    if (IS_ERR(adev->cl[counter] = class_create(THIS_MODULE, temp))) {
      dbg("failed to create a class \n");
      counter -= 1;
      goto fail;
    }
    snprintf(streamnumber, sizeof(streamnumber), "%d", i);
    strcpy(temp, adev->device_head);
    strcat(temp, "stream");
    strcat(temp, streamnumber);
    strcat(temp, "!frame");
    if (IS_ERR(adev->dev[counter] = device_create(
                   adev->cl[counter], NULL,
                   MKDEV(adev->major, adev->minor + counter), NULL, temp))) {
      dbg("failed to create a device \n");
      class_destroy(adev->cl[counter]);
      counter -= 1;
      goto fail;
    }
    adev->frameMinorNumbers[i] = adev->minor + counter;

    cdev_init(&adev->cdevs[counter].c_dev, &frame_fops);
    if ((ret = cdev_add(&adev->cdevs[counter].c_dev,
                        MKDEV(adev->major, adev->minor + counter), 1)) < 0) {
      dbg("failed to add a device \n");
      device_destroy(adev->cl[counter],
                     MKDEV(adev->major, adev->minor + counter));
      class_destroy(adev->cl[counter]);
      counter -= 1;
      goto fail;
    }

    //----------------stream
    // status--------------------------------------------------
    counter += 1;
    snprintf(temp, sizeof(temp), "apec%02u%02ustreamstatus%d", adev->bus_nr,
             adev->dev_nr, i);
    if (IS_ERR(adev->cl[counter] = class_create(THIS_MODULE, temp))) {
      dbg("failed to create a class \n");
      counter -= 1;
      goto fail;
    }
    snprintf(streamnumber, sizeof(streamnumber), "%d", i);
    strcpy(temp, adev->device_head);
    strcat(temp, "stream");
    strcat(temp, streamnumber);
    strcat(temp, "!status");
    if (IS_ERR(adev->dev[counter] = device_create(
                   adev->cl[counter], NULL,
                   MKDEV(adev->major, adev->minor + counter), NULL, temp))) {
      dbg("failed to create a device \n");
      class_destroy(adev->cl[counter]);
      counter -= 1;
      goto fail;
    }
    adev->streamStatusMinorNumbers[i] = adev->minor + counter;

    cdev_init(&adev->cdevs[counter].c_dev, &stream_status_fops);
    if ((ret = cdev_add(&adev->cdevs[counter].c_dev,
                        MKDEV(adev->major, adev->minor + counter), 1)) < 0) {
      dbg("failed to add a device \n");
      device_destroy(adev->cl[counter],
                     MKDEV(adev->major, adev->minor + counter));
      class_destroy(adev->cl[counter]);
      counter -= 1;
      goto fail;
    }

    //----------------stream
    // interrupt--------------------------------------------------
    counter += 1;
    snprintf(temp, sizeof(temp), "apec%02u%02ustreaminterrupt%d", adev->bus_nr,
             adev->dev_nr, i);
    if (IS_ERR(adev->cl[counter] = class_create(THIS_MODULE, temp))) {
      dbg("failed to create a class \n");
      counter -= 1;
      goto fail;
    }
    snprintf(streamnumber, sizeof(streamnumber), "%d", i);
    strcpy(temp, adev->device_head);
    strcat(temp, "stream");
    strcat(temp, streamnumber);
    strcat(temp, "!interrupt");
    if (IS_ERR(adev->dev[counter] = device_create(
                   adev->cl[counter], NULL,
                   MKDEV(adev->major, adev->minor + counter), NULL, temp))) {
      dbg("failed to create a device \n");
      class_destroy(adev->cl[counter]);
      counter -= 1;
      goto fail;
    }
    adev->streamInterruptMinorNumbers[i] = adev->minor + counter;

    cdev_init(&adev->cdevs[counter].c_dev, &stream_interrupt_fops);
    if ((ret = cdev_add(&adev->cdevs[counter].c_dev,
                        MKDEV(adev->major, adev->minor + counter), 1)) < 0) {
      dbg("failed to add a device \n");
      device_destroy(adev->cl[counter],
                     MKDEV(adev->major, adev->minor + counter));
      class_destroy(adev->cl[counter]);
      counter -= 1;
      goto fail;
    }
  }

  //----------------------bus
  // address--------------------------------------------
  counter += 1;
  snprintf(temp, sizeof(temp), "apec%02u%02ubusaddress", adev->bus_nr,
           adev->dev_nr);
  if (IS_ERR(adev->cl[counter] = class_create(THIS_MODULE, temp))) {
    dbg("failed to create a class \n");
    counter -= 1;
    goto fail;
  }

  strcpy(temp, adev->device_head);
  strcat(temp, "bus!busaddress");
  if (IS_ERR(adev->dev[counter] = device_create(
                 adev->cl[counter], NULL,
                 MKDEV(adev->major, adev->minor + counter), NULL, temp))) {
    dbg("failed to create a device \n");
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  cdev_init(&adev->cdevs[counter].c_dev, &bus_address_fops);
  if ((ret = cdev_add(&adev->cdevs[counter].c_dev,
                      MKDEV(adev->major, adev->minor + counter), 1)) < 0) {
    dbg("failed to add a device \n");
    device_destroy(adev->cl[counter],
                   MKDEV(adev->major, adev->minor + counter));
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  //----------------------bus data--------------------------------------------
  counter += 1;
  snprintf(temp, sizeof(temp), "apec%02u%02ubusdata", adev->bus_nr,
           adev->dev_nr);
  if (IS_ERR(adev->cl[counter] = class_create(THIS_MODULE, temp))) {
    dbg("failed to create a class \n");
    counter -= 1;
    goto fail;
  }

  strcpy(temp, adev->device_head);
  strcat(temp, "bus!busdata");
  if (IS_ERR(adev->dev[counter] = device_create(
                 adev->cl[counter], NULL,
                 MKDEV(adev->major, adev->minor + counter), NULL, temp))) {
    dbg("failed to create a device \n");
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  cdev_init(&adev->cdevs[counter].c_dev, &bus_data_fops);
  if ((ret = cdev_add(&adev->cdevs[counter].c_dev,
                      MKDEV(adev->major, adev->minor + counter), 1)) < 0) {
    dbg("failed to add a device \n");
    device_destroy(adev->cl[counter],
                   MKDEV(adev->major, adev->minor + counter));
    class_destroy(adev->cl[counter]);
    counter -= 1;
    goto fail;
  }

  //-------------------------------------------------------------------------
  adev->devices_number = counter;
  adev->interfaces_created = 1;
  return 0;
fail:
  for (i = counter; i > 0; i--) {
    cdev_del(&adev->cdevs[i].c_dev);
    device_destroy(adev->cl[i], MKDEV(adev->major, adev->minor + i));
    class_destroy(adev->cl[i]);
  }
  unregister_chrdev_region(adev->first, DEVICES_PER_CARD);
err_alloc:
  return -1;
}

static int polling_fn(void *device) {
  u32 last_descriptor = 0;
  u32 idx = 0;
  int i;
  APEC *adev = (APEC *)device;
  u32 schedule_counter = 0;
  unsigned long timeout;
  u32 restart = 0;
  u32 last_descriptor_counter_writeback = 0;
  u32 catchup = 1;
  volatile u32 *descriptor_counter_writeback = (u32 *)adev->buffer_writeback;
  dbg("APEC Thread running\n");

  while (!kthread_should_stop()) {
    schedule_counter++;
    // does the same as apec_read_bypass_offset but removes 2 function calls
    // busy until the the FPGA has another descriptor index than the driver do
    timeout = jiffies + HZ;
    while (last_descriptor_counter_writeback ==
           *(descriptor_counter_writeback)) {
      // make sure the compiler does not optimize this loop
      if (time_after(jiffies, timeout) || atomic_read(&adev->reset) == 1) {
        if (atomic_read(&adev->reset) == 1) {
          dbg("Resetting APEC");
          printk(KERN_INFO "Resetting APEC.\n");
          atomic_set(&adev->reset, 0);
        } else {
          dbg("polling stuck");
        }
        schedule();
        // stop engine
        xdmaWriteChannel(adev->xdev, C2H_CHANNEL_CONTROL_RUN, 0x0, 0);
        // full reset
        apec_write_user_bar(adev, CPURESET, 0xffffffff);
        adev->transfer = adev->current_transfer;
        for (i = 0; i < 511; i++) {
          dma_sync_single_for_device(
              &adev->xdev->pdev->dev, adev->current_transfer->handle,
              (transferSizePerChannel * channels),
              DMA_FROM_DEVICE);
          apec_write_descriptor_to_bypass_bar(adev, adev->current_transfer, i);

          adev->current_transfer = adev->current_transfer->next;
        }
        apec_write_user_bar(adev, CPURESET, 0x0);
        xdmaWriteChannel(adev->xdev, C2H_CHANNEL_CONTROL_RUN, 0x1, 0);
        last_descriptor = 0;
        restart = 1;
        break;
      }
    }

    if (last_descriptor_counter_writeback < *(descriptor_counter_writeback)) {
      catchup =
          (*(descriptor_counter_writeback)-last_descriptor_counter_writeback) /
          channels;
    } else {
      catchup = 1;
    }

    last_descriptor_counter_writeback = *(descriptor_counter_writeback);
    if (!restart) {

      // replace all the used descriptors
      u32 desc_counter = 0;
      while (apec_bypass_read(adev, last_descriptor) == 0) {
        if (desc_counter > 0) {
          atomic_inc(&adev->missed_turns);
        }
        dma_sync_single_for_device(
            &adev->xdev->pdev->dev, adev->transfer->handle,
            (transferSizePerChannel * channels),
            DMA_FROM_DEVICE);
        apec_write_descriptor_to_bypass_bar(adev, adev->transfer,
                                            last_descriptor);
        if (last_descriptor == 510) {
          last_descriptor = 0;
        } else {
          last_descriptor++;
        }
        adev->transfer = adev->transfer->next;
        desc_counter++;
      }
      for (; catchup > 0; catchup--) {
        // TODO could (most likely happen that transfer and current_transfer is
        // not sychronized, they should have an offset of 512)
        atomic_set(&adev->transfer_idx_atomic, adev->current_transfer->idx);
        i = adev->transfer->idx - 512;
        if (i < 0) {
          i += transfers;
        }
        atomic_set(&adev->direct_transfer_idx_atomic, (u32)i);
        dma_sync_single_for_cpu(
            &adev->xdev->pdev->dev, adev->current_transfer->handle,
            (transferSizePerChannel * channels),
            DMA_FROM_DEVICE);
        adev->current_transfer = adev->current_transfer->next;
        // wake up readers
        for (idx = 0; idx < MAX_USERS; idx++) {
          atomic_set(&adev->users_locks[idx].user_lock, 1);
        }
        if (schedule_counter > 11245) {
          schedule_counter = 0;
          cond_resched();
        }
      }
    }
    restart = 0;
  }
  dbg("APEC Thread Stopping\n");
  do_exit(0);
  return 0;
}

/*! \fn void remove_one(struct pci_dev *pdev)
    \brief called when a device is removed it should clean everything up
    \param dev The pci_dev struct
*/
static void remove_one(struct pci_dev *pdev) {
  int ret;
  APEC *adev = dev_get_drvdata(&pdev->dev);
  XDMA *xdev = adev->xdev;
  dev_set_drvdata(&pdev->dev, NULL);
  if (polling) {
    ret = kthread_stop(adev->thread_st);
    if (ret) {
      dbg("polling thread did not exit correctly\n");
    }
  }
  dbg_v("remove_device_interface\n");
  remove_device_interface(adev);
  dbg_v("stop_engine\n");
  stop_engine(adev);
  dbg_v("irq_msix_channel_teardown\n");
  irq_msix_channel_teardown(adev);
  dbg_v("irq_msix_user_teardown\n");
  irq_msix_user_teardown(adev);
  dbg_v("disable_msix\n");
  disable_msix(adev);
  dbg("destroy_descriptor_chain\n");
  destroy_descriptor_chain(adev);
  kfree(adev);
  dbg_v("remove_XDMA\n");
  remove_XDMA(xdev);
}

/*! \fn int probe_one(struct pci_dev *pdev, const struct pci_device_id *id)
    \brief called when a device is detected that might use our driver, probes
   the device Can definitely fail, indicated by returing -EINVAL \param dev The
   pci_dev struct \param id our pci_device_id struct
*/
static int probe_one(struct pci_dev *pdev, const struct pci_device_id *id) {

  XDMA *xdev = NULL;
  APEC *adev = NULL;
  int rv = 0;
  int i = 0;
#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 0, 0)
  const struct sched_param polling_thread_sched = {.sched_priority = 50};
#endif

  //  Get the XDMA struct for communicating with the card
  xdev = init_XDMA(pdev);
  if (xdev == NULL) {
    dbg("Received a NULL pointer from init_XDMA in probe_one()\n");
    return -EINVAL;
  }
  if (xdev->bit64_dma_enabled != 1) {
    dbg("device does not support 64 bit addressing\n");
    remove_XDMA(xdev);
    return -EINVAL;
  }

  // We must have one C2H engine
  if (xdev->c2h_number_channels <= 0) {
    dbg("device has no C2H engine\n");
    remove_XDMA(xdev);
    return -EINVAL;
  }

  // allocate our own structure
  adev = kzalloc(sizeof(APEC), GFP_KERNEL);
  if (adev == NULL) {
    dbg("Could not allocate the APEC struct\n");
    remove_XDMA(xdev);
    return -EINVAL;
  }
  adev->bus_nr = pdev->bus->number;
  adev->dev_nr = PCI_SLOT(pdev->devfn);
  dbg_v("Device: bus: %02u device: %02u \n", adev->bus_nr, adev->dev_nr);
  // the root of all the device files in /sys for this device should be
  // /sys/APEC/0100/....
  snprintf(adev->device_head, sizeof(adev->device_head), "%s%02x%02x!",
           DEVICE_TREE_HEAD, adev->bus_nr, adev->dev_nr);

  snprintf(adev->thread_name, sizeof(adev->thread_name),
           "APECPollingThread%02x%02x", adev->bus_nr, adev->dev_nr);

  // setup relationship
  adev->xdev = xdev;
  adev->msix_setup = 0;
  adev->msix_setup_user = 0;
  adev->msix_enabled = 0;
  adev->transfer = 0;
  adev->last_counter = 0;

  adev->interfaces_created = 0;
  adev->devices_number = 0;
  // initialize all cdev so they point back to the created APEC struct
  for (i = 0; i < DEVICES_PER_CARD; i++) {
    adev->cdevs[i].adev = adev;
  }
  spin_lock_init(&adev->lock);
  dbg_v("Address alignement %u , len granularity %u, address bits %u\n ",
        xdmaReadChannel(xdev, C2H_CHANNEL_ADDR_ALIGNMENT, 0),
        xdmaReadChannel(xdev, C2H_CHANNEL_LEN_GRANULARITY, 0),
        xdmaReadChannel(xdev, C2H_CHANNEL_ADDRESS_BITS, 0));
  atomic_set(&adev->transfer_idx_atomic, 0);
  atomic_set(&adev->direct_transfer_idx_atomic, 0);
  atomic_set(&adev->descriptor_match_errors_atomic, 0);
  atomic_set(&adev->descriptor_counter_atomic, 0);
  atomic_set(&adev->descriptor_counter_rollover_atomic, 0);
  atomic_set(&adev->irq_count_atomic, 0);
  atomic_set(&adev->last_descriptor, 0);
  atomic_set(&adev->missed_turns, 0);

  atomic_set(&adev->reset, 0);

  for (i = 0; i < MAX_USERS; i++) {
    atomic_set(&adev->users_locks[i].user_lock, 0);
  }
  spin_lock_init(&adev->users_setup_lock);
  bitmap_zero(adev->users_bitmap, MAX_USERS);

  // for user interrupts
  for (i = 0; i < 10; i++) {
    init_waitqueue_head(&(adev->user_interrupt_queues[i]));
    atomic_set(&(adev->virtual_user_interrupt[i]), -1);
    bitmap_zero(adev->users_interrupt_bitmap[i], MAX_USERS);
    for (rv = 0; rv < MAX_USERS; rv++) {
      atomic_set(&adev->users_interrupt_locks[i][rv].user_lock, -2);
    }
  }
  INIT_WORK(&adev->after_user_interrupt, after_user_interrupt);

  // the pci_dev struct holds a pointer to our APEC structure which we can use
  // to get the XDMA struct
  dev_set_drvdata(&pdev->dev, adev);

  //--------------From now on we can call remove_one if something fails
  // check that the user bar is there
  rv = check_user_bar(adev->xdev);

  if (rv) {
    dbg("No user bar detected\n");
    remove_one(pdev);
    return -EINVAL;
  }

  rv = check_bypass_bar(adev->xdev);

  if (rv) {
    dbg("No bypass bar detected\n");
    remove_one(pdev);
    return -EINVAL;
  }
  apec_zero_bypass_bar(adev);

  // the FPGA send data 32bytes per clockcycle
  apec_write_user_bar(adev, TRANSFERSIZE_PER_CHANNEL,
                      transferSizePerChannel / 32);
  apec_write_user_bar(adev, CHANNELS, channels);
  // the size of one transfer
  apec_write_user_bar(adev, TRANSFERTOTAL,
                      (transferSizePerChannel * channels) / 32);
  // to setup the clock, not used now
  apec_write_user_bar(adev, CLOCKSETUP, 1);
  apec_write_user_bar(adev, TIMEOUTMIN, timeoutmin);
  apec_write_user_bar(adev, TIMEOUTMAX, timeoutmax);
  apec_write_user_bar(adev, SIZEPERFRAME, 895);
  // reset bypasscontroller and databufferreader
  apec_write_user_bar(adev, CPURESET, 0xffffffff);
  // reset interrupts
  write_user_bar(adev->xdev, 584, 1023);

  if (polling == 1) {
    apec_write_user_bar(adev, POLLING, 0xffffffff);
  } else {
    apec_write_user_bar(adev, POLLING, 0x0);
  }

  rv = create_descriptor_chain(adev);
  if (rv) {
    dbg("Creation of descriptor chain failed\n");
    remove_one(pdev);
    return -EINVAL;
  }

  // enable MSI-X
  rv = enable_msix(adev);
  if (rv < 0) {
    dbg("enable msix failed \n");
    remove_one(pdev);
    return -EINVAL;
  }
  rv = irq_msix_channel_setup(adev);
  if (rv) {
    dbg("irq_msix_channel_setup failed \n");
    remove_one(pdev);
    return -EINVAL;
  }
  rv = irq_msix_user_setup(adev);
  if (rv) {
    dbg("irq_msix_channel_setup failed \n");
    remove_one(pdev);
    return -EINVAL;
  }

  // reset control register
  __xdmaWrite32(adev->xdev, 0x1, 0, 0x04, 0);
  // clear status register
  __xdmaWrite32(adev->xdev, 0x1, 0, 0x40, 0xfffffffe);

  rv = create_device_interface(adev);
  if (rv < 0) {
    dbg("creation of interfaces failed\n");
    remove_one(pdev);
    return -EINVAL;
  }

  // start the beast
  apec_write_user_bar(adev, CPURESET, 0x0);
  write_user_bar(adev->xdev, 584, 0);
  if (polling) {
    // create task
    adev->thread_st = kthread_run(polling_fn, adev, adev->thread_name);
    if (IS_ERR(adev->thread_st)) {
      dbg("Polling thread was not created correctly\n");
      remove_one(pdev);
      return -EINVAL;
    }
#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 0, 0)
    rv = sched_setscheduler(adev->thread_st, SCHED_RR, &polling_thread_sched);
    if (rv) {
      dbg("could not set scheduling params for the polling thread\n");
    }
#endif
  }
  set_all_register(adev);
  return 0;
}

/*! \fn pci_ers_result_t apec_error_detected(struct pci_dev
   *pdev,pci_channel_state_t state) \brief called when an error occurs with the
   PCIE \param dev The pci_dev struct \param state the current state of the PCIe
   device \return the action that needs to be taken for the different
*/
static pci_ers_result_t apec_error_detected(struct pci_dev *pdev,
                                            pci_channel_state_t state) {

  switch (state) {
  case pci_channel_io_normal:
    return PCI_ERS_RESULT_CAN_RECOVER;
  case pci_channel_io_frozen:
    pci_disable_device(pdev);
    return PCI_ERS_RESULT_NEED_RESET;
  case pci_channel_io_perm_failure:
    return PCI_ERS_RESULT_DISCONNECT;
  }
  return PCI_ERS_RESULT_NEED_RESET;
}

/*! \fn pci_ers_result_t apec_slot_reset(struct pci_dev *pdev)
    \brief called when the device is reset
    \param dev The pci_dev struct
    \returns if we can recover or not
*/
static pci_ers_result_t apec_slot_reset(struct pci_dev *pdev) {

  if (pci_enable_device_mem(pdev)) {
    return PCI_ERS_RESULT_DISCONNECT;
  }

  pci_set_master(pdev);
  pci_restore_state(pdev);
  pci_save_state(pdev);

  return PCI_ERS_RESULT_RECOVERED;
}

/*! \fn void apec_error_resume(struct pci_dev *pdev)
    \brief called when the device can resume
    \param dev The pci_dev struct
*/
static void apec_error_resume(struct pci_dev *pdev) {
// pci_cleanup_aer_uncorrect_error_status(pdev);
#if KERNEL_VERSION(5, 7, 0) <= LINUX_VERSION_CODE
  pci_aer_clear_nonfatal_status(pdev);
#else
  pci_cleanup_aer_uncorrect_error_status(pdev);
#endif
}

static const struct pci_error_handlers apec_err_handler = {
    .error_detected = apec_error_detected,
    .slot_reset = apec_slot_reset,
    .resume = apec_error_resume,
};

static struct pci_driver apec_driver = {
    .name = DRV_MODULE_NAME,
    .id_table = pci_ids,
    .probe = probe_one,
    .remove = remove_one,
    .err_handler = &apec_err_handler,
};

static int __init apec_init(void) {
  int ret = 0;
  // int i = 0;
  printk(KERN_INFO "Loading APEC.\n");
  transfers=t;
  polling=p;
  transferSizePerChannel=ts;
  channels=c;
  timeoutmin=ti;
  timeoutmax=ta;

  printk(
      KERN_INFO
      "APEC arguments: transfers:%lu  polling=%lu transferSizePerChannel=%lu "
      "channels=%lu timeoutmin=%lu timeoutmax=%lu \n ",
      transfers, polling, transferSizePerChannel, channels, timeoutmin,
      timeoutmax);

  if (transfers <= 0) {
    dbg("Transfers is 0 in apec_init\n");
    return -EIO;
  }

  if ((ret = pci_register_driver(&apec_driver)) < 0) {
    dbg("failed to register pci driver \n");
    return ret;
  }

  printk(KERN_INFO "Loading APEC complete.\n");
  return 0;
}
static void __exit apec_exit(void) {
  printk(KERN_INFO "Unloading APEC.\n");
  pci_unregister_driver(&apec_driver);
}

module_init(apec_init);
module_exit(apec_exit);
MODULE_AUTHOR("Martin Soderen");
MODULE_LICENSE("GPL v2");
MODULE_DEVICE_TABLE(pci, pci_ids);
