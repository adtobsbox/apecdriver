REPO_PARENT ?= $(shell pwd)
# include the build environment
include $(REPO_PARENT)/common.mk

DIRS = \
    driver \
    lib \

.PHONY: all clean cleanall install $(DIRS)

all clean cleanall install: $(DIRS)
clean: TARGET = clean
cleanall: TARGET = cleanall
install: TARGET = install

$(DIRS):
	$(MAKE) -C $@ $(TARGET)

test: lib
