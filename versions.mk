#define product's name
PRODUCT_NAME=rf_apec
CPU = L867
KVER = 3.10.0-957.1.3.rt56.913.el7.x86_64
ACC = lab
DEPLOY_PATH = /

# common version
VER_MAJOR=1
VER_MINOR=0
VER_PATCH=2

# version of the header file(s)
HEADER_MAJOR=$(VER_MAJOR)
HEADER_MINOR=$(VER_MINOR)
HEADER_PATCH=$(VER_PATCH)

# version of the library
LIB_MAJOR=$(VER_MAJOR)
LIB_MINOR=$(VER_MINOR)
LIB_PATCH=$(VER_PATCH)

# version of the programs
PROG_MAJOR=$(VER_MAJOR)
PROG_MINOR=$(VER_MINOR)
PROG_PATCH=$(VER_PATCH)

# define versions
VER_PREV=
VER_CURRENT=$(VER_MAJOR).$(VER_MINOR)
VER_NEXT=
