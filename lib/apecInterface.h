#ifndef HEADER_APECINTERFACE
#define HEADER_APECINTERFACE

#include <sys/mman.h>
#include <sys/stat.h> 
#include <string>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include  <stdexcept>
#include <unistd.h>
#include <fcntl.h>
#include <boost/filesystem.hpp>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include <memory>

///@{ 
namespace apecInterface{

/** To have same type definitions as in the driver */
typedef int64_t s64;
/** To have same type definitions as in the driver */
typedef uint64_t u64;

	    /** \brief Struct used for setting up the driver
      */
struct __attribute__((__packed__))  setup_stream{
	s64 offset;
	u64 turns;
};

/** \brief b[3] b[2] b[1] b[0] -> b[1] b[0] b[3] b[2]
*/
uint32_t bit16swap(const uint32_t& in);


/**
 * \class APECInterface
 *
 * \brief Library to abstract away the communication with th APECDriver
 *
 * This library allows applications to setup and read data from the APECDriver
 * There are different ways of reading data such as direct(no syncronization), stream(synchronization but the user is responsible for reading data fast enough),
 * cont(the driver keeps tracks of the last read data and returns new data when enough is available).
 * you can also choose to enable or disable the mmaping of the buffer. This is useful if you have one thread that is triggered and it only needs to read the offsets in the buffer
 * and then you can have another thread that actually copies the data from the buffer  
 *
 * \note Attempts at zen rarely work.
 *
 * \author  $Author: Martin Soderen $
 *
 * \version $Revision: 1.0 $
 *
 * \date $Date: 2020/08/07 10:30:00 $
 *
 * Contact: martin.soderen@cern.ch
 *
 * Created on: May 01 13:37:00 2019
 *
 */
template<class T>
class APECInterface{
public:
	    /** \brief Constructs a APECInterface instance.
      * \param device A string that tells the library which card to read from, for example "af00" will interface with /dev/APEC/af00
      * \param stream Which stream to read data from, for example 0 will read data from /dev/APEC/af00/stream0
      * \param direct If the synchronization should be enabled, if this is disabled then readData will be none-blocking and postive offsets will be ignored 
      * \param doMmap If we should mmap the buffer, if this is disabled then readData will throw an error but readOffsets will function as normal
      * \param continuous This makes the driver keep track of where the last read was done and offsets are ignored, for example if you read 512 turns and then do another read it will return the next 512 turns. HOWEVER if you don't read fast enough then this will wait until the driver has done a full rotation in the circular buffer
      * 
      */
	APECInterface(const std::string& device,const unsigned& stream,const bool& direct=false,const bool& doMmap=true,const bool& continuous=false):_device(device),_stream(stream),_devicePath("/dev/APEC/"+device+"/"),_bufferSizePath(_devicePath+"bufferSize"),_bufferPath(_devicePath+"buffer"),_doMmap(doMmap){

		boost::filesystem::path temp(_bufferSizePath);
		if (!boost::filesystem::exists(temp)){
			std::ostringstream tempMsg;
			tempMsg<<"BufferSize file: "<<_bufferSizePath<<" does not exists"<<std::endl;
			throw std::runtime_error(tempMsg.str());
		} 

		temp=boost::filesystem::path(_bufferPath);
		if (!boost::filesystem::exists(temp)){
			std::ostringstream tempMsg;
			tempMsg<<"Buffer file: "<<_bufferPath<<" does not exists"<<std::endl;
			throw std::runtime_error(tempMsg.str());
		} 
		{
			std::ostringstream streamPath;
			if(continuous){
				streamPath<<_devicePath<<"stream"<<_stream<<"/cont";
			}

			else if (direct){
				streamPath<<_devicePath<<"stream"<<_stream<<"/direct";
			}
			else{
				streamPath<<_devicePath<<"stream"<<_stream<<"/stream";
			}
			_streamPath=streamPath.str();
		}

		temp=boost::filesystem::path(_streamPath);
		if (!boost::filesystem::exists(temp)){
			std::ostringstream tempMsg;
			tempMsg<<"Stream file: "<<_streamPath<<" does not exists"<<std::endl;
			throw std::runtime_error(tempMsg.str());
		} 

		_bufferSize=getBufferSize();
		if(_bufferSize==0){
			throw std::runtime_error("buffersize of 0 found, this could be becuase you do not have the privileges to open this file");
		}
		if(_doMmap){
	   		_bufferFileDescriptor = open(_bufferPath.c_str(), O_RDONLY);
	   		if(_bufferFileDescriptor<0){
	   			throw std::runtime_error("opening of buffer file failed");
	   		}
	   		_memory = static_cast<int8_t*>(mmap(NULL, _bufferSize, PROT_READ, MAP_PRIVATE, _bufferFileDescriptor, 0));
	   		if (_memory==MAP_FAILED){
	   			std::ostringstream tempMsg;
	   			tempMsg<<"Mapping of "<<_bufferPath<<" failed with buffersize: "<<_bufferSize;
	   			throw std::runtime_error(tempMsg.str());
	   		}
			close(_bufferFileDescriptor);
   		}
   		_fdStream = open(_streamPath.c_str(),O_RDWR);
   		if(_fdStream<0){
   			std::ostringstream tempMsg;
   			tempMsg<<"Could not open "<<_streamPath;
   			throw std::runtime_error(tempMsg.str());
   		}
   		_offsetBuffer=static_cast<u64*>(malloc(_turns*sizeof(u64)));

   		for(unsigned i =0;i<_turns;i++){
   			returnContainer.push_back( std::make_shared<T>());
   		}

	}
		    /** \brief Destructor
      */
	~APECInterface(){
		free(_offsetBuffer);
		for(unsigned i =0;i<_turns;i++){
   			returnContainer[i].reset();
   		}
   		returnContainer.clear();
		if(_doMmap){		
			munmap(_memory, _bufferSize);
		}
		close(_fdStream);
	}
	    /** \brief Setup the offset in the buffer and the number of frames to receive from the driver
      * \param offset  X[0] is the latest frame received and readData and readOffsets will return X[offset] as the first frame, so a negative offset is in the past and vice versa
      * \param turns the number of turns to return
      * Example: turns = 8 and offset = -4<br>
      * current buffer:<br>
      * Oldest,																		Newest<br>
      * X[-12],X[-11],X[-10],X[-9],X[-8],X[-7],X[-6],X[-5],X[-4],X[-3],X[-2],X[-1],X[0]<br>
	  * Will return:<br>
	  * end of array, 							Beginning of array<br>
	  * X[-11],X[-10],X[-9],X[-8],X[-7],X[-6],X[-5],X[-4]<br>
      */

	void setup(const s64& offset, const u64& turns){
		struct setup_stream setup_struct;
		setup_struct.offset=offset;
		setup_struct.turns=turns;
		if(write(_fdStream, &setup_struct, sizeof(struct setup_stream))!=sizeof(struct setup_stream)){		
			std::ostringstream tempMsg;
   			tempMsg<<"Could not setup the stream with offset: "<<offset<<" turns: "<<turns<<" "<<_streamPath;
   			throw std::runtime_error(tempMsg.str());
		}
		free(_offsetBuffer);
		for(unsigned i =0;i<_turns;i++){
   			returnContainer[i].reset();
   		}
   		returnContainer.clear();
		_turns=turns;
		_offset=offset;
		_offsetBuffer=static_cast<u64*>(malloc(_turns*sizeof(u64)));
		 for(unsigned i =0;i<_turns;i++){
   			returnContainer.push_back( std::make_shared<T>());
   		}

	}
	    /** \brief Reads the offsets from the driver sets the pointer for all the T template types which are returned
	    * \returns Returns the data in form of a vector of template T which must implement the function void set_ptr(const void* ptr)
      */

	std::vector<std::shared_ptr<T>> readData(){
		if(!_doMmap){
			throw std::runtime_error("Can't read data if doMmap was not selected");
		}
		std::vector<std::shared_ptr<T>> toReturn;
		int rv=read(_fdStream,static_cast<void*>(_offsetBuffer),static_cast<int> (_turns*sizeof(u64)));
		if(rv!= static_cast<int>(_turns*sizeof(u64))){
			return toReturn;
		}
	//	std::cout<<"offset: "<<*_offsetBuffer<<std::endl;
		for(u64 i=0;i<_turns;i++){
			returnContainer[i]->set_ptr(reinterpret_cast<const void*>(reinterpret_cast<const int16_t*>(_memory+*(_offsetBuffer+i))));
			//toReturn.push_back(T(reinterpret_cast<const void*>(reinterpret_cast<const int16_t*>(_memory+*(_offsetBuffer+i)))));
		}
		return returnContainer;

	}
		/** \brief Reads the offsets from the driver and returns them in form of a vector
	    * \returns Vector of offsets in the circular buffer to the frames
      */
	std::vector<u64> readOffsets(){
		std::vector<u64> toReturn;
		int rv=read(_fdStream,static_cast<void*>(_offsetBuffer),static_cast<int> (_turns*sizeof(u64)));
		if(rv!= static_cast<int>(_turns*sizeof(u64))){
			return toReturn;
		}
		for(u64 i=0;i<_turns;i++){
			toReturn.push_back(*(_offsetBuffer+i));
		}
		return toReturn;

	}
			/** \brief Returns a template T in the circular buffer from a offset previously read by the driver
			* \param offset A offset previously read by the driver
	    * \returns A template T type which points to the frame defined by the offset
      */
	std::shared_ptr<T> readDataFromOffset(u64 offset){
		if(!_doMmap){
			throw std::runtime_error("Can't read data if doMmap was not selected");
		}
		if (returnContainer.size()>0){
			returnContainer[0]->set_ptr(reinterpret_cast<const void*>(reinterpret_cast<const int16_t*>(_memory+offset)));
			return returnContainer[0];
		}
		std::shared_ptr<T> toReturn=std::make_shared<T>();
		toReturn->set_ptr(reinterpret_cast<const void*>(reinterpret_cast<const int16_t*>(_memory+offset)));
		return toReturn;
	}
			/** \brief Returns the offset setup by the user
	    * \returns The offset setup by the user
      */
	s64 getOffset(){
		return _offset;
	}
		/** \brief Returns the number of turns setup by the user
	    * \returns The number of turns setup by the user
      */
	u64 getTurns(){
		return _turns;
	}
protected:
private:
	long long unsigned getBufferSize(){
		std::ifstream infile(_bufferSizePath);
		long long unsigned result=0;
		if (infile.is_open()&&infile>>result){
			return result;
		}
		return 0;

}
	const std::string _device; ///< which device we are using
	std::vector<std::shared_ptr<T>> returnContainer;///< A temporary buffer of allocated T types which are used by the user to read the data
	const unsigned _stream; ///< the stream we are reading from
	const std::string _devicePath; ///< the path to the device
	const std::string _bufferSizePath; ///< the file to read the size of the buffer
	const std::string _bufferPath; ///< the file to mmap for reading the data
	s64 _offset=0;	///< current offset
	u64 _turns=1;	///< current number of turns
	 std::string _streamPath; ///< driver device used for reading the offsets
	long long unsigned _bufferSize; ///< the read buffersize
	int _bufferFileDescriptor; ///< file descriptor for the mmaped buffer
	int8_t* _memory;	///< pointer to mmaped buffer
	u64* _offsetBuffer;	///< temporary buffer used to read the offsets from the driver
	int _fdStream; ///< file descriptor for the driver device to read the offsets
	bool _doMmap; ///< if we have mmaped the buffer, used for safety checks
APECInterface(const APECInterface&):_stream(0){

};

};

class ADT{
friend class APECInterface<ADT>;
public:
	const void* getPointer(){
		return _ptr;
	}
	uint32_t getHeader(){
		const uint32_t* ptr=reinterpret_cast<const uint32_t*>(_ptr);
		return bit16swap(*(ptr+0));
	};
	uint32_t getSourceID(){
		const uint32_t* ptr=reinterpret_cast<const uint32_t*>(_ptr);
		return bit16swap(*(ptr+1));
	};
	uint32_t getBlockSize(){
		const uint32_t* ptr=reinterpret_cast<const uint32_t*>(_ptr);
		return bit16swap(*(ptr+2));
	};
	uint32_t getTurnCounter(){
		const uint32_t* ptr=reinterpret_cast<const uint32_t*>(_ptr);
		return bit16swap(*(ptr+3));
	};
	uint32_t getTagBits(){
		const uint32_t* ptr=reinterpret_cast<const uint32_t*>(_ptr);
		return bit16swap(*(ptr+4));
	};
	uint32_t getCycleNumber(){
		const uint32_t* ptr=reinterpret_cast<const uint32_t*>(_ptr);
		return bit16swap(*(ptr+5));
	};
	uint16_t getDataWordSize(){
		const uint16_t* ptr=reinterpret_cast<const uint16_t*>(_ptr);
		return *(ptr+12);
	};
	
	uint16_t getDataPerBunch(){
	 	const uint16_t* ptr=reinterpret_cast<const uint16_t*>(_ptr);
		return *(ptr+13);
	};
	uint32_t getReserved(){
		const uint32_t* ptr=reinterpret_cast<const uint32_t*>(_ptr);
		return bit16swap(*(ptr+7));
	};

	const int16_t* getData(){
		const int16_t* ptr=reinterpret_cast<const int16_t*>(_ptr);
		return (ptr+16);
	};

	uint32_t getCRC32(){
		const uint32_t* ptr=reinterpret_cast<const uint32_t*>(_ptr);
		return bit16swap(*(ptr+1790));
	};

	uint32_t getSize(){
		return 36+7128;
	}

	bool APECTagOK(){
		const uint32_t* ptr=reinterpret_cast<const uint32_t*>(_ptr);
		return *(ptr+1792)==1128616001;
	}

	bool CRCOK(){
		const uint8_t* ptr=reinterpret_cast<const uint8_t*>(_ptr);
		ptr+=7172;
		return *ptr==0xff;
	}

	bool notInTableOK(){
		const uint8_t* ptr=reinterpret_cast<const uint8_t*>(_ptr);
		ptr+=7173;
		return *ptr==0x00;
	}
	bool disparityOK(){
		const uint8_t* ptr=reinterpret_cast<const uint8_t*>(_ptr);
		ptr+=7174;
		return *ptr==0x00;
	}
	
	
	
		/*!
	 * \fn RBOXLoggingServer(const RBOXLoggingServer& )
	 * \brief Deleted copy constructor.
	 */
		ADT(const ADT& obj ) {
			_ptr=obj._ptr;

		};

	/*!
	 * \fn RBOXLoggingServer operator=(const RBOXLoggingServer& server)
	 * \brief Deleted copy assignment.
	 */
	ADT operator=(const ADT& obj) {
		return obj;
	}
	~ADT(){};
	ADT(){};
protected:
	void set_ptr(const void* ptr){
		_ptr=ptr;
	}
private:
//ADT(const void* ptr):_ptr(ptr){};
const void* _ptr=NULL;

};



void test();
}

#endif
///@}
