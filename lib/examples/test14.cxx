#include "apecInterface.h"
#include <ios>
#include <cstdlib>
#include <cstring>
#include <stdlib.h>

using namespace apecInterface;

int main(int argc, char *argv[]){
    std::vector <std::string> sources;
    for (int i = 1; i < argc; ++i) {
            sources.push_back(argv[i]); // Add all but the last argument to the vector.
     }
     if(sources.size()<3){
        throw std::runtime_error ("Usage: test1 card stream offset, example: test1 1b00 0 -4096");
     }

	APECInterface<ADT>* test= new APECInterface<ADT>(sources[0],std::stoi(sources[1]) , true, true);
	s64 offset= std::stoll(sources[2]);
    test->setup(0,1);

 APECInterface<ADT>* test1= new APECInterface<ADT>(sources[0],std::stoi(sources[1]), true, true);
 test1->setup(offset,4096);

  auto data1= test1->readData();
 auto data= test->readData();
 std::cout<<"new test"<<std::endl;
std::cout<<data[0]->getTurnCounter()<<std::endl;
std::cout<<data1[4095]->getTurnCounter()<<std::endl;


	delete test;
}
