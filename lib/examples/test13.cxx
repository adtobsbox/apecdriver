#include "apecInterface.h"
#include <ios>
#include <cstdlib>
#include <cstring>
#include <stdlib.h>

using namespace apecInterface; 

int main(int argc, char *argv[]){
    std::vector <std::string> sources;
    for (int i = 1; i < argc; ++i) {
            sources.push_back(argv[i]); // Add all but the last argument to the vector.
     }
     if(sources.size()<2){
        throw std::runtime_error ("Usage: test1 card stream, example: test1 1b00 0 ");
     }

	APECInterface<ADT>* test= new APECInterface<ADT>(sources[0],std::stoi(sources[1]),false, true);
	test->setup(0,1);
	while(true){
		auto data= test->readData();
		for (auto it : data){
			const int16_t* bunchData=it->getData();
			for (std::size_t i=0;i<3564;i++){
				if(*(bunchData+i)!=0){
					std::cout<<"bunch "<<i<<" has none zero data: "<<*(bunchData+i)<<std::endl;
				}
			}
		}
		

	
}
}
