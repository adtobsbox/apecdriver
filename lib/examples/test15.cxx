#include "apecInterface.h"
#include <ios>
#include <cstdlib>
#include <cstring>
#include <stdlib.h>

using namespace apecInterface; 

int main(int argc, char *argv[]){
    std::vector <std::string> sources;
    for (int i = 1; i < argc; ++i) {
            sources.push_back(argv[i]); // Add all but the last argument to the vector.
     }
     if(sources.size()<2){
        throw std::runtime_error ("Usage: test1 card stream, example: test1 1b00 0 ");
     }

	APECInterface<ADT>* test= new APECInterface<ADT>(sources[0],std::stoi(sources[1]),true,true);
	test->setup(0,1);
	//uint32_t tagbits=0;
//	while(true){
		auto data= test->readData();
		if (data[0]->getTagBits()!=0){
			std::cout<<"tagbits: "<<data[0]->getTagBits()<<std::endl;
			std::cout<<"turnCounter: "<<data[0]->getTurnCounter()<<std::endl;
		}

	
//}
}
