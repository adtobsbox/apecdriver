#include <sys/mman.h>
#include <sys/stat.h> 
#include <string>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include  <stdexcept>
#include <unistd.h>
#include <fcntl.h>
#include <iomanip>
#include <cstdlib>




int main(){
	std::string _bufferPath="/dev/APEC/0100/buffer";
	std::ifstream infile("/dev/APEC/0100/bufferSize");
		long long unsigned result=0;
		if (!(infile.is_open()&&infile>>result)){
			return -1;
		}
	std::cout<<"Buffersize: "<<result<<std::endl;	

	 int _bufferFileDescriptor = open(_bufferPath.c_str(), O_RDONLY);
   	 void* _memory = mmap(NULL, result, PROT_READ, MAP_PRIVATE, _bufferFileDescriptor, 0);
   	  		if (_memory==MAP_FAILED){
   			std::ostringstream tempMsg;
   			tempMsg<<"Mapping of "<<_bufferPath<<" failed";
   			throw std::runtime_error(tempMsg.str());
   		}
   	 std::ofstream myFile;
   	 myFile.open ("/home/msoderen/data2.bin", std::ios::out | std::ios::binary);
   	 myFile.write(static_cast<const char*>(_memory), result);
   	 myFile.close();

}
