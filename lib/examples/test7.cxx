#include "apecInterface.h"
#include <ios>
#include <chrono>

using namespace apecInterface; 
int main(int argc, char *argv[]){
    std::vector <std::string> sources;
    for (int i = 1; i < argc; ++i) {
            sources.push_back(argv[i]); // Add all but the last argument to the vector.
     }
     if(sources.size()<2){
        throw std::runtime_error ("Usage: test1 card stream, example: test1 1b00 0 ");
     }

	APECInterface<ADT>* test= new APECInterface<ADT>(sources[0],std::stoi(sources[1]),false,true,true);
	
	while(true){
		auto start = std::chrono::high_resolution_clock::now();
		test->setup(0,65536);
		auto data= test->readOffsets();
		data= test->readOffsets();
		data= test->readOffsets();
		std::size_t duration=std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now()-start).count();
		std::cout<<duration<<std::endl;
		// std::cout<<"header: "<<data[0]->getHeader()<<std::endl;
		// std::cout<<"sourceid: "<<data[0]->getSourceID()<<std::endl;
		// std::cout<<"blocksize: "<<data[0]->getBlockSize()<<std::endl;
		// std::cout<<"turncounter: "<<data[0]->getTurnCounter()<<std::endl;	
		// std::cout<<"tagbits: "<<data[0]->getTagBits()<<std::endl;
		// std::cout<<"cyclenumber: "<<data[0]->getCycleNumber()<<std::endl;
		// std::cout<<"datawordsize: "<<data[0]->getDataWordSize()<<std::endl;
		// std::cout<<"dataperbunch: "<<data[0]->getDataPerBunch()<<std::endl;
		// std::cout<<"reserved: "<<data[0]->getReserved()<<std::endl;
		// std::cout<<"bunch 0: "<<*(data[0]->getData())<<std::endl;
		// std::cout<<"bunch 1: "<<*(data[0]->getData()+1)<<std::endl;
		// std::cout<<"bunch 3563: "<<*(data[0]->getData()+3563)<<std::endl;

	
}
}
