#include "apecInterface.h"
#include <ios>
#include <string>
#include <vector>
#include <stdexcept>
#include <fstream>
#include <chrono> 
#include <ctime> 
#include<atomic>
#include <thread>

std::atomic<std::uint64_t> frameCounter;

using namespace apecInterface; 

void watchdog(){
	uint64_t ownFrameCounter=frameCounter;
	while (true){
		std::this_thread::sleep_for(std::chrono::seconds(1));
		if (ownFrameCounter==frameCounter){
			auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()); 
			std::cout <<"Reader is stuck "<< ctime(&timenow) << std::endl; 
			return;
		}
		ownFrameCounter=frameCounter;
	}

}


int main(int argc, char *argv[]){
	frameCounter.store(0);
	uint32_t streamCounter=0;
	uint32_t fpgaCounter=0;
	//int stream=0;
	std::ofstream outfile;
	//std::streambuf * buf;
	bool error=false;
	bool dumpBuffer=true;
	bool firstFrame =true;

	std::vector <std::string> sources;
 	for (int i = 1; i < argc; ++i) {
            sources.push_back(argv[i]); // Add all but the last argument to the vector.
     }
     
     if(sources.size()<2){
     	throw std::runtime_error ("Not enough arguements given");
     }

     //stream=std::stoi(sources[1]);
     //APECInterface	interface= new APECInterface<ADT>("0100",stream);
     APECInterface<ADT>* interface=new APECInterface<ADT>(sources[0],std::stoi(sources[1]));
	 interface->setup(0,4096);
	 outfile.open("APECTEST"+sources[0]+"-"+sources[1]+".txt", std::ios_base::app);

	// int16_t toCompare[128]={0};
	 bool dataError=false;
	 std::thread first (watchdog);
	 bool errorLast=false;
	 uint64_t errorCounter=0;
	 while(true){
	 	error=false;
	 	dataError=false;
	 	if (errorCounter>1000000){
	 		std::cout<<"Maximum errorCounter reached"<<std::endl;
	 		return 0;
	 	}
	 	auto data= interface->readData();
	 	frameCounter++;
	 	for(auto it :data){
		 	streamCounter++;
			fpgaCounter=it->getTurnCounter();
			if(fpgaCounter!=streamCounter ){
				error=true;
			}
			if (it->getHeader()!=1331852081){
				error=true;
			}

			if (it->getSourceID()!=286327044 and it->getSourceID()!=13107 and it->getSourceID()!=286327043 and it->getSourceID()!=17476){
				error=true;
			}

			if(it->getBlockSize()!=7128){
				error=true;
			}
			if(it->getTagBits()!=4096){
				error=true;
			}
		//	if(it->getCycleNumber()!=2147483648){
		//		error=true;
		//	}
			if(it->getDataWordSize()!=0){
				error=true;
			}
			if(it->getDataPerBunch()!=0){
				error=true;
			}
			if(it->getReserved()){
				error=true;
			}

			if (!it->CRCOK()){
			//	error=true;
			}
			if(!it->APECTagOK()){
			//	error=true;
			}

			if(!it->notInTableOK()){
				error=true;
			}
			if(!it->disparityOK()){
				error=true;
			}
			/*const int16_t* data=it->getData();
			data+=127;
			for (int i=0;i<14;i++){
				if(memcmp(data,toCompare,128)){
					error=true;
					dataError=true;
				}
				data+=256;
			}
			*/
			if (errorLast){
				errorLast=false;
				streamCounter=fpgaCounter;
				continue;
			}

			if(error){
				errorLast=true;
				errorCounter++;
				outfile<<"---------------------------"<<std::endl;
				auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()); 
				outfile << ctime(&timenow) << std::endl; 
				outfile<<"Header: "<<it->getHeader()<<std::endl;
				outfile<<"SourceID: "<<it->getSourceID()<<std::endl;
				outfile<<"BlockSize: "<<it->getBlockSize()<<std::endl;
				outfile<<"turnCounter: "<<it->getTurnCounter()<<std::endl;
				outfile<<"CPUCounter: "<<streamCounter<<std::endl;
				outfile<<"Tagbits: "<<it->getTagBits()<<std::endl;
				outfile<<"CycleNumber: "<<it->getCycleNumber()<<std::endl;
				outfile<<"dataWordSize: "<<it->getDataWordSize()<<std::endl;
				outfile<<"dataPerBunch: "<<it->getDataPerBunch()<<std::endl;
				outfile<<"reserved: "<<it->getReserved()<<std::endl;
				if (it->CRCOK()){
					outfile<<"CRC: OK"<<std::endl;
				}
				else{
					outfile<<"CRC: not OK"<<std::endl;
				}
				if (it->APECTagOK()){
					outfile<<"APEC tag: OK"<<std::endl;
				}
				else{
					outfile<<"APEC tag: not OK"<<std::endl;
				}
				if (it->notInTableOK()){
					outfile<<"notintable: OK"<<std::endl;
				}
				else{
					outfile<<"notintable: not OK"<<std::endl;
				}
				if (it->disparityOK()){
					outfile<<"disparity: OK"<<std::endl;
				}
				else{
					outfile<<"disparity: not OK"<<std::endl;
				}
				if (!dataError){
					outfile<<"dataError: OK"<<std::endl;
				}
				else{
					outfile<<"dataError: not OK"<<std::endl;
				}
			if(dumpBuffer and !firstFrame){
				std::ofstream myFile;
				std::ostringstream temp;
				temp<<"dumpbuffer-"<<"-"<<sources[1]<<streamCounter<<".txt";
         		myFile.open (temp.str(), std::ios::out | std::ios::binary);
         		//data starts from stream0 from last turn, this can crash if descriptor 0 is used, whatever
         		myFile.write(static_cast<const char*>(it->getPointer())-std::stoi(sources[1])*8192 -81920, 81920*2);
         		myFile.close();
			}
			firstFrame=false;


			}
			streamCounter=fpgaCounter;
		
		}		

	}
}
