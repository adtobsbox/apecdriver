#include "apecInterface.h"
#include <ios>
#include <cstdlib>
#include <cstring>
#include <stdlib.h>

using namespace apecInterface; 

int main(int argc, char *argv[]){
    std::vector <std::string> sources;
    for (int i = 1; i < argc; ++i) {
            sources.push_back(argv[i]); // Add all but the last argument to the vector.
     }
     if(sources.size()<2){
        throw std::runtime_error ("Usage: test1 card stream, example: test1 1b00 0 ");
     }

	APECInterface<ADT>* test= new APECInterface<ADT>(sources[0],std::stoi(sources[1]));
	test->setup(0,65536);
	uint32_t streamCounter=0;
	uint32_t fpgaCounter=0;
	while(true){
        auto data= test->readData();
        bool first=true;
        for(auto it :data){
            if(first){
                streamCounter=it->getTurnCounter();
                first=false;
                continue;
            }
            streamCounter++;
            fpgaCounter=it->getTurnCounter();
            if(fpgaCounter!=streamCounter ){
                std::cout<<"Counters does not match, fpga: "<<fpgaCounter<<" CPU: "<<streamCounter <<" diff: "<<streamCounter-fpgaCounter<<std::endl;
                streamCounter=fpgaCounter;
            }
        }
    }
}
