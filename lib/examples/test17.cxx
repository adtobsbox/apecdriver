#include "apecInterface.h"
#include <ios>
#include <cstdlib>
#include <cstring>
#include <stdlib.h>
#include <chrono>

using namespace apecInterface; 

int main(int argc, char *argv[]){
    std::vector <std::string> sources;
    for (int i = 1; i < argc; ++i) {
            sources.push_back(argv[i]); // Add all but the last argument to the vector.
     }
     if(sources.size()<2){
        throw std::runtime_error ("Usage: test1 card stream, example: test1 1b00 0 ");
     }

	APECInterface<ADT>* test= new APECInterface<ADT>(sources[0],std::stoi(sources[1]),false,true,true);
	test->setup(0,128);
	uint64_t last_second_counter = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	uint64_t last_120second_counter = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	uint64_t frame_counter=0;
	uint64_t bunched_frame_counter=0;
	uint32_t last_turncounter=0;
	uint32_t turncounter_error=0;
	while(true){
		auto data= test->readData();
		for (auto it : data){
			frame_counter++;
			if( it->getTagBits() !=0){
				bunched_frame_counter++;
				uint32_t new_turncounter=it->getTurnCounter();
				if(new_turncounter!=(last_turncounter+1)){
					turncounter_error++;
					std::cout<<"new: "<<new_turncounter<<" should be: "<<(last_turncounter+1)<<std::endl;
				}
				last_turncounter=new_turncounter;
			}
		}
		uint64_t now=std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
		if(now-last_second_counter>1000){
			std::cout<<"frames: "<< static_cast<float>(frame_counter)/ static_cast<float>((now-last_120second_counter)/120000.0)<<std::endl;
			std::cout<<"frame counter: "<<frame_counter<<std::endl;
			std::cout<<"bunched frames: "<< static_cast<float>(bunched_frame_counter)/ ( static_cast<float>(now-last_120second_counter)/120000.0)<<std::endl;
			std::cout<<"bunched frame counter: "<<bunched_frame_counter<<std::endl;
			std::cout<<"turncounter_error: "<<turncounter_error<<std::endl;
			last_second_counter=now;
		}
		if(now-last_120second_counter>120000){
			std::cout<<"frames(120s): "<<frame_counter/120<<std::endl;
			std::cout<<"bunched frames(120s): "<<bunched_frame_counter /120<<std::endl;
			std::cout<<"turncounter_error: "<<turncounter_error<<std::endl;
			last_second_counter=now;
			last_120second_counter=now;
			frame_counter=0;
			bunched_frame_counter=0;
			turncounter_error=0;
		}

	
}
}
