#include <cstdint>
#include <iostream>
#include <fstream>
#include <exception>
#include <stdexcept>

int main(){
	while (true){
    std::string interruptFileName = "/dev/APEC/af00/stream0/interrupt";
    std::ifstream interruptFile(interruptFileName.c_str());
    if (!interruptFile.is_open()) {
      throw std::runtime_error("Could not open interrupt file");
    }
    int32_t data;
    while (interruptFile >> data) {
    	std::cout<<"data: "<<data<<std::endl;
    }
}

}