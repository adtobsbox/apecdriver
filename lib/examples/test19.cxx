#include "apecInterface.h"
#include <chrono>
#include <ios>

using namespace apecInterface;
int main(int argc, char *argv[]) {
  std::vector<std::string> sources;
  for (int i = 1; i < argc; ++i) {
    sources.push_back(argv[i]); // Add all but the last argument to the vector.
  }
  if (sources.size() < 2) {
    throw std::runtime_error(
        "Usage: test1 card stream, example: test1 1b00 0 ");
  }

  APECInterface<ADT> *test = new APECInterface<ADT>(
      sources[0], std::stoi(sources[1]), false, true, true);
  test->setup(0, 1);
  std::size_t timeCounter = 0;
  std::size_t transfers = 0;
  const std::size_t transfersToCount = 100000;

  uint32_t header;
  uint32_t sourceID;
  uint32_t blockSize;
  uint32_t turnCounter;
  uint32_t tagBits;
  uint32_t cycleNumber;
  uint16_t dataWordSize;
  uint16_t dataPerBunch;
  uint32_t reserved;
  int16_t *dataContainer = reinterpret_cast<int16_t *>(malloc(sizeof(int16_t) * 3564));
  uint32_t crc32;

  while (true) {

    auto data = test->readData();
auto start = std::chrono::high_resolution_clock::now();
    for (auto it : data) {
      header = it->getHeader();
      sourceID=it->getSourceID();
      blockSize=it->getBlockSize();
      turnCounter=it->getTurnCounter();
      tagBits=it->getTagBits();
      cycleNumber=it->getCycleNumber();
      dataWordSize=it->getDataWordSize();
      dataPerBunch=it->getDataPerBunch();
      reserved=it->getReserved();
      memcpy(dataContainer,it->getData(),3564*sizeof(int16_t));
      crc32=it->getCRC32();
    }
    std::size_t duration =
        std::chrono::duration_cast<std::chrono::nanoseconds>(
            std::chrono::high_resolution_clock::now() - start)
            .count();
    timeCounter+=duration;
    transfers++;
    if(transfers>=transfersToCount){
     break;
    }

  }
  std::cout<<static_cast<double>(timeCounter)/static_cast<double>(transfers)<<"ns"<<std::endl;
}
