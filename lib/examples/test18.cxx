#include "apecInterface.h"
#include <ios>
#include <cstdlib>
#include <cstring>
#include <stdlib.h>
#include <chrono>
#include <thread>
using namespace apecInterface; 

int main(int argc, char *argv[]){
    std::vector <std::string> sources;
    for (int i = 1; i < argc; ++i) {
            sources.push_back(argv[i]); // Add all but the last argument to the vector.
     }
     if(sources.size()<2){
        throw std::runtime_error ("Usage: test1 card stream, example: test1 1b00 0 ");
     }

	APECInterface<ADT>* test= new APECInterface<ADT>(sources[0],std::stoi(sources[1]),false,true,true);
	test->setup(0,128);
	while(true){
		auto offsets= test->readOffsets();
            std::string card = "/dev/APEC/" + sources[0] + "/status";
            std::ifstream infile(card.c_str() );
            if (!infile.is_open())
            {
            	std::cout<<"Could not open FPGA status file"<<std::endl;
                return 0;
            }
            std::string property;
            std::string comma;
            std::size_t data;
            while (infile >> property >> comma >> data)
            {
            	if (property=="transfer_idx"){
            		int64_t readOffset=offsets.back()/8192;
            		int64_t hardwareOffset=data;
            		if(readOffset<hardwareOffset ){
            			std::cout<<hardwareOffset-readOffset<<std::endl;
            		}
            		else{
            			std::cout<<4194304- (hardwareOffset-readOffset)<<std::endl;	
            		}
            	}
                
            }
            std::this_thread::sleep_for(std::chrono::microseconds(1500) );
	
}
}
