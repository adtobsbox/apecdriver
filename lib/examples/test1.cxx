#include "apecInterface.h"
#include <ios>
#include <cstdlib>
#include <cstring>
#include <stdlib.h>

using namespace apecInterface; 

int main(int argc, char *argv[]){
    std::vector <std::string> sources;
    for (int i = 1; i < argc; ++i) {
            sources.push_back(argv[i]); // Add all but the last argument to the vector.
     }
     if(sources.size()<2){
        throw std::runtime_error ("Usage: test1 card stream, example: test1 1b00 0 ");
     }

	APECInterface<ADT>* test= new APECInterface<ADT>(sources[0],std::stoi(sources[1]));
	test->setup(0,1);
	uint32_t streamCounter=0;
	uint32_t fpgaCounter=0;
	const void* previous_pointer=NULL;
	while(true){
        auto data= test->readData();
 
        for(auto it :data){
            //streamCounter++;
            fpgaCounter=it->getTurnCounter();
            if(fpgaCounter==0 ){
                std::cout<<"turncounter is zero with ptr:"<<it->getPointer()<<" previous pointer: "<<previous_pointer<<std::endl;
		const int32_t* rawData=reinterpret_cast<const int32_t*>(it->getPointer());
		for (std::size_t i=0;i<32;i++){
			std::cout<<*(rawData+i)<<std::endl;	
		}
            }
	previous_pointer=it->getPointer();
        }
    }
}
