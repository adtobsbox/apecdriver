```
from apecInterface import *
# first argument is the card name listed in /dev/APEC, second argument is the channel for that card so this open /dev/APEC/0100/stream0
test=APECINTERFACE("0100",0)
# first argument is the offset in the buffer and the second is the number of turns to acquire
# positive offset is in the future so this call would be blocking until that many new turns have been acquired
# negative offset is in the past and it returns instantaneous
test.setup(0,1)

while True:
    # data is an array of turn-by-turn data 
    data=toADT(test.readData())
    print(data[0].getTurnCounter())
}
```