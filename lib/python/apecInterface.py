import os
import numpy as np
import mmap
baseFolder="/dev/APEC/"

class ADTData:
	def __init__(self,data):
		self._data=data

	def __convert(self,data):
		temp=data.tostring()
		temp=temp[2:]+temp[0:2]
		return np.fromstring(temp, dtype='uint32')

	def getHeader(self):
			return self.__convert(self._data[0:2])

	def getSourceID(self):
			return self.__convert(self._data[2:4])

	def getBlockSize(self):
			return self.__convert(self._data[4:6])

	def getTurnCounter(self):
			return self.__convert(self._data[6:8])

	def getTagBits(self):
			return self.__convert(self._data[8:10])

	def getCycleNumber(self):
			return self.__convert(self._data[10:12])

	def getDataWordSize(self):
			return self._data[12]

	def getDataPerBunch(self):
			return self._data[13]

	def getReserved(self):
			return self.__convert(self._data[14:16])

	def getData(self):
			return self._data[16:3580]


class toADT(ADTData):
	def __init__(self,data):
		ADTData.__init__(self,data[0])
		self._turns=[]
		for element in data:
			self._turns.append(ADTData(element))
	def __getitem__(self,key):
		return self._turns[key]





class APECINTERFACE:
	def __init__(self,device,stream,direct=False,cont=False):
		#check parameters
		if type(device)!=str:
			raise RuntimeError("Device must be a string");
		if not os.path.isdir(baseFolder+device):
			raise RuntimeError("Folder "+baseFolder+device+" does not exists")
		if type(stream)!=int:
			raise RuntimeError("stream must be of type int")
		if stream<0 or stream>9:
			raise RuntimeError("Stream must be in the range of 0-9")

		self._device=device
		self._stream=stream
		self._turns=1
		self._deviceFolder=baseFolder+self._device+"/"

		#check bufferSize
		bufferSizeFile=self._deviceFolder+"bufferSize"
		#os.path.isfile does not seem to work
		if not "bufferSize" in os.listdir(self._deviceFolder):
			raise RuntimeError(bufferSizeFile+" does not exists")
		with open(bufferSizeFile) as f:
			self._bufferSize=int(f.read())
		if self._bufferSize<=0:
			raise RuntimeError("buffersize is less or equal to zero")

		#check stream file
		streamPath=""
		if direct:
			streamPath=self._deviceFolder+"stream"+str(self._stream)+"/direct"
		else:
			streamPath=self._deviceFolder+"stream"+str(self._stream)+"/stream"
		if cont:
			streamPath=self._deviceFolder+"stream"+str(self._stream)+"/cont"
		if not "stream"+str(self._stream) in os.listdir(self._deviceFolder):
			raise RuntimeError(streamPath+" does not exists")
		self._streamFile=open(streamPath,'r+b')

		#check bufferFile
		bufferPath=self._deviceFolder+"buffer"
		if not "buffer" in os.listdir(self._deviceFolder):
			raise RuntimeError(bufferPath+" does not exists")

		#open buffer
		self._buffer_mmap_file=open(bufferPath,"rb")
		self._buffer_mmap=mmap.mmap(self._buffer_mmap_file.fileno(),self._bufferSize,prot=mmap.PROT_READ)
		self._buffer = np.frombuffer(self._buffer_mmap, dtype='int16', count=self._bufferSize/2)

	def setup(self,offset,turns):
		if type(offset)!=int or type(turns)!=int:
			raise RuntimeError("offset and turns must be of type int")
		if turns<1:
			raise RuntimeError("turns must be greater than 0")
#either this is correct
#		x=np.array([np.int64(offset),np.uint64(turns)])
#		x=x.tostring()
#or the following 3 lines are correcty
		x=np.array([np.uint64(turns)])
		y=np.array([np.int64(offset)])
		x=y.tostring()+x.tostring()

		self._streamFile.write(x)
		self._streamFile.flush()
		self._turns=turns
	def readOffsets(self):
		temp=self._streamFile.read(self._turns*8)
		return np.frombuffer(temp,dtype='uint64')

	def readData(self):
		toReturn=[]
		temp=self._streamFile.read(self._turns*8)
		offsets=np.frombuffer(temp,dtype='uint64')/2
		for element in offsets:
			toReturn.append(self._buffer[element:])
		return toReturn
