```
#include "apecInterface.h"
using namespace apecInterface;
int main(){
    //read stream0 from card 0100
    APECInterface test=APECInterface<ADT>("0100",0);
    //read one turn with 0 offset
    test.setup(0,1);
    while(true){
        auto data= test.readData(); //blocking until new data is available
        for(auto it :data){
            std::cout<<it->getTurnCounter()<<std::endl;
        }
    }
}
```